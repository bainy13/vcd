﻿<#
    TO DO LIST
    ===========
    0. Fix Remove-vCloudOrg handling of break statements
    1. Organisation Users may be created in AD (along with appropriate OU structure etc)
    2. Collect user credentials once, then reuse for Connect-CIRest and other Connect- function
    3. Re-write FW and NAT functions to support work on Advanced Edges
    4. Write Role assignment function to set rights on Org Roles (adv edge rights)
    7. Get module file to auto-check for new versions
    8. Get module file to auto-update on new-version detected

#>

function show-header()
{
    Write-Host @'
                                           
 ██████ █████   ████   ████   ████ ██████  
 ██  ██ ██  ██ ██  ██ ██  ██ ██      ██    
 ██████ █████  ██  ██ ██████ ██      ██    
 ██     ██  ██ ██  ██ ██  ██ ██      ██    
 ██     ██  ██  ████  ██  ██  ████   ██    
                                           
         delivering  business  agility     
                                           
'@ -ForegroundColor White -BackgroundColor Red
}

function Import-vCDcommonVars()
{
    <#
        This function is used to hold variables common to all parts of the module
            - doing this prevents duplication inside each script
            - but care must be taken -
            - changes in here affect ALL functions that use these variables
    #>
    # Specify mail server to use for functions that use email and the author to contact with any issues/queries.
    $global:mailServer = "SLOMCSSMTP001.mds.lan"
    $global:emailAuthor = "gb-phc@proact.co.uk"

    # Set the VI and CI Server details for Production Environment
    $global:viServer = "phctenvc01.eu.mds.lan"
    $global:ciServer = "vcloud.proact.eu"

    # Set the VI and CI Server details for Lab Environment
    $global:viLabsServer = "devphctenvc01.phcmgmt.lan"
    $global:ciLabsServer = "devphcvcd01.phcmgmt.lan"

    # Windows Domain name of the lab - if this script is executed on a PS Host that's domain joined
    $global:labDomainName = "PHCMGMT"

    # Set Cloud Provider vCD Org / Catalogue / vDC
    $global:providerOrgName = "Proact"
    $global:pubCatName = "Public Images"

    # Set Base OU for AD Users Creation
    $global:ADbaseOU = "OU=Portal Users,DC=phcuser,DC=lan"
    $global:labADbaseOU = "OU=Portal Users,DC=phcmgmt,DC=lan"

    # As a safeguard, detect if the machine's domain is the lab domain name, if so, then force -labs ON
    if ($env:USERDOMAIN -eq $labDomainName)
    {
        Write-Host "LAB USE DETECTED" -ForegroundColor DarkMagenta -BackgroundColor White
        if (!$PSBoundParameters.ContainsKey('labs'))
        {
            Write-Warning "YOU SHOULD BE USING THE LABS SWITCH"
            confirm-continue
            if ($break)
            {
                break
            }
        }
    }

    # Check version of this module
    # write-Host "$(((get-module vcd).version).Major).$(((get-module vcd).version).Minor)"
    <#if ((get-module vcd).version -ne "1")
    {
        Write-Host "A newer version of this module is available - update for the latest features / support"
    }
    #>

}

function connect-vi
{
    # Initialise PowerCLI Environment
    try
    {
        "Checking for PowerCLI Support..."
        get-powercliversion | Out-Null
    }

    catch
    {
        Write-host "PowerCLI Support is Required" -ForegroundColor Yellow
        Import-Module VMware.VimAutomation.Core
        Import-Module VMware.VimAutomation.Vds
        Import-Module VMware.VimAutomation.Cloud
        Import-Module VMware.VimAutomation.PCloud
        Import-Module VMware.VimAutomation.Cis.Core
        Import-Module VMware.VimAutomation.Storage
        Import-Module VMware.VimAutomation.HorizonView
        Import-Module VMware.VimAutomation.HA
        Import-Module VMware.VimAutomation.vROps
        Import-Module VMware.VumAutomation
        Import-Module VMware.DeployAutomation
        Import-Module VMware.ImageBuilder
        Import-Module VMware.VimAutomation.License

        try
        {
            get-powercliversion | Out-Null
        }

        catch
        {
            write-warning "Sorry, couldn't auto-load the extension"
            break
        }
    }
    write-host "PowerCLI Environment Present" -ForegroundColor Green

    # Handle the connection to the CI server
    if(!$DefaultVIServers)
    {
        Write-Host "Connecting to VI Server..."
        connect-viserver -server $viserver
    }

    if ($DefaultVIServers.name -ne $viserver -or @($DefaultVIServers).count -gt 1)
    {
        Write-Warning "Existing connection to VI Server exists - disconnecting for safety"
        disconnect-viserver * -confirm:$false
        Write-Host "Connecting to correct VI Server..."
        connect-viserver -server $viserver
    }
}

function disconnect-vi
{
    foreach ($vi in $defaultVIservers)
    {
        if ($vi.name -eq $viserver)
            {
                Write-Host "Disconnecting VI Server..."
                disconnect-viserver * -confirm:$false
            }
    }
}

function connect-ci
{
    # Initialise PowerCLI Environment
    try
    {
        "Checking for PowerCLI Support..."
        get-powercliversion | Out-Null
    }

    catch
    {
        Write-host "PowerCLI Support is Required" -ForegroundColor Yellow
        Import-Module VMware.VimAutomation.Core
        Import-Module VMware.VimAutomation.Vds
        Import-Module VMware.VimAutomation.Cloud
        Import-Module VMware.VimAutomation.PCloud
        Import-Module VMware.VimAutomation.Cis.Core
        Import-Module VMware.VimAutomation.Storage
        Import-Module VMware.VimAutomation.HorizonView
        Import-Module VMware.VimAutomation.HA
        Import-Module VMware.VimAutomation.vROps
        Import-Module VMware.VumAutomation
        Import-Module VMware.DeployAutomation
        Import-Module VMware.ImageBuilder
        Import-Module VMware.VimAutomation.License

        try
        {
            get-powercliversion | Out-Null
        }

        catch
        {
            write-warning "Sorry, couldn't auto-load the extension"
            break
        }
    }
    write-host "PowerCLI Environment Present" -ForegroundColor Green

    # Handle the connection to the CI server
    if(!$DefaultCIServers)
    {
        Write-Host "Connecting to CI Server..."
        connect-ciserver -server $ciserver
    }

    if ($DefaultCIServers.name -ne $ciserver -or @($DefaultCIServers).count -gt 1)
    {
        Write-Warning "Existing connection to CI Server exists - disconnecting for safety"
        disconnect-ciserver * -confirm:$false
        Write-Host "Connecting to correct CI Server..."
        connect-ciserver -server $ciserver
    }
}

function disconnect-ci
{
    foreach ($ci in $defaultCIservers)
    {
        if ($ci.name -eq $ciserver)
            {
                Write-Host "Disconnecting CI Server..."
                disconnect-ciserver * -confirm:$false
            }
    }
}

function connect-ciREST()
{
    <#
    .SYNOPSIS
        version 1.0 - Written by Dave Hocking 2017.
        Function makes a connection to vCD using supplied credentials and stores the auth header for future use
        Tested on vCD 8.10 but written to support 8.20 (not tested at time of writing)

    .EXAMPLE
        connect-ciREST -server "vcloud.proact.eu" -version 8.10
         - Connects to the specified server, using version 8.10 API calls
         - As no Org is specified, assumption is that a SYSTEM admin account will be supplied

    .EXAMPLE
        connect-ciREST -server "vcloud.proact.eu" -orgName GB-DJH
         - Connects to the specified server, as no version was specified, it will default to using version 8.10 API calls
         - As the GB-DJH Org is specified, assumption is that an admin account from that Organisation will be supplied

    #>

    # Allow advanced parameter functions
    [CmdletBinding()]

    # Define the Parameters to pass the to function
    Param(
    [Parameter(Mandatory=$true)]
    [string]$server,

    [Parameter(Mandatory=$false)]
    [validateSet("5.5","5.6","8.0","8.10","8.20")]
    [string]$version="8.20",

    [Parameter(Mandatory=$false)]
    [string]$orgName="system"
    )

    # Check for the supplied version of vCloud Director, then use correct API version
    $apiver = switch ($version)
    {
         "8.20" {"27.0"}
         "8.10" {"20.1"}
         "8.0" {"9.0"}
         "5.6" {"5.6"}
         "5.5" {"5.5"}
    }

    # Get login details, even if the user has a saved creds store, you'll need this for REST API access later
    if (!$creds)
    {
        $script:creds = Get-Credential -Message "Enter vCloud Admin-Level Credentials"
    }
    $username = $creds.UserName
    $password = $creds.GetNetworkCredential().password

    # Build the Auth string
    $auth = $username + '@' + $orgName + ':' + $password

    # Encode basic authorization for the header
    $Encoded = [System.Text.Encoding]::UTF8.GetBytes($auth)
    $EncodedPassword = [System.Convert]::ToBase64String($Encoded)

    # Define a standard header
    $ciHeaders = @{"Accept"="application/*+xml;version=$apiver";"Authorization"="Basic $($EncodedPassword)"}

    # Create full address of resource to be retrieved
    $script:baseurl = "https://$($server)/api"
    $resource = '/versions'
    $url = $baseurl + $resource

    # Given the API version, get the login URL
    $versions = Invoke-RestMethod -Uri $url -Headers $ciHeaders -Method GET
    ForEach ($ver in $versions.SupportedVersions.VersionInfo)
    {
        if ($ver.Version -eq $apiver)
        {
            $loginUrl = $ver.LoginUrl
        }
    }

    # Log in and capture the response header, add the x-vcloud-authorization value for future use
    $response = Invoke-WebRequest -Uri $loginUrl -Headers $ciHeaders -Method Post
    $global:ciHeaders = @{"Accept"="application/*+xml;version=$apiver";"x-vcloud-authorization"="$($response.Headers.'x-vcloud-authorization')"}
    if ($response.Headers.'x-vcloud-authorization' -eq $null)
    {
        Write-Warning "Couldn't login successfully"
        break
    }
    else
    {
        Write-Host "Successfully logged into vCloud via API" -ForegroundColor Green
    }
    ""
}

function disconnect-ciREST()
{
    # Logout
    $sessionURL = $baseurl + '/session'
    $logout = Invoke-webrequest -Uri $sessionurl -Headers $ciHeaders -Method DELETE
    if ($logout.StatusCode -ne 204)
    {
        Write-Warning "Error in logging out - Script variable `"`$ciHeaders`" still contains auth key."
    }
    else
    {
        ""
        Write-Host "Successfully logged out" -ForegroundColor Green
        ""
    }
}

function show-arrayMenu()
{
    <#
    .SYNOPSIS
        version 1.0 - Written by Dave Hocking 2016.
        Function takes an array of objects (such as from "ls" or "get-vm")...
        Presents them as a menu with a numerical reference to choose from...
        Then returns a global-scoped variable called $object that contains the choice

    .EXAMPLE
        show-ArrayMenu (get-vm)
         - Uses PowerCLI to get a list of VMs from connected servers, then displays a list for the user to choose from

    #>

    # Allow advanced parameter functions
    [CmdletBinding()]

    # Define the Parameters to pass the to function
    Param(
    [Parameter(Mandatory=$true)]
    $objects
    )

    # Reset the break variable
    $global:break = $false

    # Define the do-while loop that cycles around until a valid choice is made
    do {
        $loop = $false
        $numObjects = $objects.count

        # Starts for-loop to draw menu based on the number of items in the array
        for($i=1; $i -le $numObjects;$i++)
        {
            Write-Host "(" -NoNewline -ForegroundColor Gray
            Write-Host "$i" -NoNewline -ForegroundColor Green
            Write-Host ") " -NoNewline -ForegroundColor Gray
            Write-Host $objects.get($i-1)
        }

        # Give the user an option for (x) to leave this menu (return NO $object)
        Write-Host "(" -NoNewline -ForegroundColor Gray
        Write-Host "x" -NoNewline -ForegroundColor Red
        Write-Host ") " -NoNewline -ForegroundColor Gray
        Write-Host eXit this menu

        # Prompts user to choose an option, then verifies it's validity
        ""
        $choice = Read-Host "Enter your selection (1-n) or (x)"
        if ($choice -eq "x" -or $choice -eq "X")
        {
            # Set the break variable
            $global:break = $true
        }

        else
        {
            # if the choice wasn't to quit, decide if it's in bounds
            [int32]$selection = $choice

            if ($selection -gt $numObjects -or $selection -lt 1)
            {
                # Send the user around the loop again, until they choose a valid option
                Write-Warning "Not a valid choice, please try again."
                ""
                $loop = $true
            }
        }
    }
    while ($loop -eq $true)

    # Error checking
    if (!$selection)
    {
        Write-Warning "Something went wrong, causing no selection to be detected."
        # Set the break variable
        $global:break = $true
    }
    else
    {
        # Output the object of the users' choice
        $global:object = $objects.get($selection-1)
        Write-Host "Selected " -NoNewline
        Write-Host "$($object)" -ForegroundColor Green
    }
}

function confirm-continue()
{
    <#
        .SYNOPSIS
        Manages user-input for a simple confirm to continue prompt
        .DESCRIPTION
        This function draws a simple "yes / no" dialogue box for users to confirm
        The function exits and creates script-scoped variables for $continue or $break
        Use of this function should include the ability to detect the presence of the $break variable
        and instructions to break from whatever processing was taking place.

        i.e.

            confirm-continue
            if ($break)
            {
                break
            }

        .NOTES
        Author:  Dave Hocking
        .EXAMPLE
        PS> confirm-continue
    #>

    do
    {
        $script:break = $false
        $loop = $false
        $continue = Read-Host "Do you wish to continue y/n ?"

        # Uses the switch statement to check for the input
        switch ($continue)
        {
            y {$loop = $false; $script:continue}
            n {Write-host "Exiting on command." -ForegroundColor Yellow; $script:break = $true}
            default {"You must choose an option, y or n" ; "" ; $loop = $true}
        }
    }
    while ($loop -eq $true)
}

function done()
{
    Write-Host "Done." -ForegroundColor Green
    ""
}

function ipcalc()
{
    <#
    .SYNOPSIS
    ipcalc calculates the IP subnet information based
    upon the entered IP address and subnet.
    .DESCRIPTION
    ipcalc calculates the IP subnet information based
    upon the entered IP address and subnet. It can accept
    both CIDR and dotted decimal formats.
    .NOTE
    By: Jason Wasser
    Created Date: 10/29/2013
    Modified Date: 8/25/2016

    Changelog
     * added object output in addition to text output
     * added binary output as an option
    .PARAMETER IPAddress
    Enter the IP address by itself or with CIDR notation.
    .PARAMETER Netmask
    Enter the subnet mask information in dotted decimal
    form.
    .PARAMETER IncludeTextOutput
    Include a text output of the subnet information in ipcalc.pl similar format.
    .PARAMETER IncludeBinaryOutput
    Include the binary format of the subnet information.
    .EXAMPLE
    ipcalc.ps1 -IPAddress 10.100.100.1 -NetMask 255.255.255.0

    Address   : 10.100.100.1
    Netmask   : 255.255.255.0
    Wildcard  : 0.0.0.255
    Network   : 10.100.100.0/24
    Broadcast : 10.100.100.255
    HostMin   : 10.100.100.1
    HostMax   : 10.100.100.254
    Hosts/Net : 254

    .EXAMPLE
    ipcalc.ps1 10.10.100.5/24

    Address   : 10.10.100.5
    Netmask   : 255.255.255.0
    Wildcard  : 0.0.0.255
    Network   : 10.10.100.0/24
    Broadcast : 10.10.100.255
    HostMin   : 10.10.100.1
    HostMax   : 10.10.100.254
    Hosts/Net : 254

    .EXAMPLE
    ipcalc.ps1 192.168.0.1/24 -IncludeBinaryOutput

    Address         : 192.168.0.1
    Netmask         : 255.255.255.0
    Wildcard        : 0.0.0.255
    Network         : 192.168.0.0/24
    Broadcast       : 192.168.0.255
    HostMin         : 192.168.0.1
    HostMax         : 192.168.0.254
    Hosts/Net       : 254
    AddressBinary   : 11000000101010000000000000000001
    NetmaskBinary   : 11111111111111111111111100000000
    WildcardBinary  : 00000000000000000000000011111111
    NetworkBinary   : 11000000101010000000000000000000
    HostMinBinary   : 11000000101010000000000000000001
    HostMaxBinary   : 11000000101010000000000011111110
    BroadcastBinary : 11000000101010000000000011111111

    .LINK
    https://gallery.technet.microsoft.com/scriptcenter/ipcalc-PowerShell-Script-01b7bd23
    #>
    [CmdletBinding()]
    param (
        [Parameter(Mandatory=$True,Position=1)]
        [string]$IPAddress,
        [Parameter(Mandatory=$False,Position=2)]
        [string]$Netmask,
        [switch]$IncludeTextOutput,
        [switch]$IncludeBinaryOutput
        )

    #region HelperFunctions

    # Function to convert IP address string to binary
    function toBinary ($dottedDecimal){
     $dottedDecimal.split(".") | ForEach-Object {$binary=$binary + $([convert]::toString($_,2).padleft(8,"0"))}
     return $binary
    }

    # Function to binary IP address to dotted decimal string
    function toDottedDecimal ($binary){
     do {$dottedDecimal += "." + [string]$([convert]::toInt32($binary.substring($i,8),2)); $i+=8 } while ($i -le 24)
     return $dottedDecimal.substring(1)
    }

    # Function to convert CIDR format to binary
    function CidrToBin ($cidr){
        if($cidr -le 32){
            [Int[]]$array = (1..32)
            for($i=0;$i -lt $array.length;$i++){
                if($array[$i] -gt $cidr){$array[$i]="0"}else{$array[$i]="1"}
            }
            $cidr =$array -join ""
        }
        return $cidr
    }

    # Function to convert network mask to wildcard format
    function NetMasktoWildcard ($wildcard) {
        foreach ($bit in [char[]]$wildcard) {
            if ($bit -eq "1") {
                $wildcardmask += "0"
                }
            elseif ($bit -eq "0") {
                $wildcardmask += "1"
                }
            }
        return $wildcardmask
        }
    #endregion


    # Check to see if the IP Address was entered in CIDR format.
    if ($IPAddress -like "*/*") {
        $CIDRIPAddress = $IPAddress
        $IPAddress = $CIDRIPAddress.Split("/")[0]
        $cidr = [convert]::ToInt32($CIDRIPAddress.Split("/")[1])
        if ($cidr -le 32 -and $cidr -ne 0) {
            $ipBinary = toBinary $IPAddress
            Write-Verbose $ipBinary
            $smBinary = CidrToBin($cidr)
            Write-Verbose $smBinary
            $Netmask = toDottedDecimal($smBinary)
            $wildcardbinary = NetMasktoWildcard ($smBinary)
            }
        else {
            Write-Warning "Subnet Mask is invalid!"
            Exit
            }
        }

    # Address was not entered in CIDR format.
    else {
        if (!$Netmask) {
            $Netmask = Read-Host "Netmask"
            }
        $ipBinary = toBinary $IPAddress
        if ($Netmask -eq "0.0.0.0") {
            Write-Warning "Subnet Mask is invalid!"
            Exit
            }
        else {
            $smBinary = toBinary $Netmask
            $wildcardbinary = NetMasktoWildcard ($smBinary)
            }
        }


    # First determine the location of the first zero in the subnet mask in binary (if any)
    $netBits=$smBinary.indexOf("0")

    # If there is a 0 found then the subnet mask is less than 32 (CIDR).
    if ($netBits -ne -1) {
        $cidr = $netBits
        #validate the subnet mask
        if(($smBinary.length -ne 32) -or ($smBinary.substring($netBits).contains("1") -eq $true)) {
            Write-Warning "Subnet Mask is invalid!"
            Exit
            }
        # Validate the IP address
        if($ipBinary.length -ne 32) {
            Write-Warning "IP Address is invalid!"
            Exit
            }
        #identify subnet boundaries
        $networkID = toDottedDecimal $($ipBinary.substring(0,$netBits).padright(32,"0"))
        $networkIDbinary = $ipBinary.substring(0,$netBits).padright(32,"0")
        $firstAddress = toDottedDecimal $($ipBinary.substring(0,$netBits).padright(31,"0") + "1")
        $firstAddressBinary = $($ipBinary.substring(0,$netBits).padright(31,"0") + "1")
        $lastAddress = toDottedDecimal $($ipBinary.substring(0,$netBits).padright(31,"1") + "0")
        $lastAddressBinary = $($ipBinary.substring(0,$netBits).padright(31,"1") + "0")
        $broadCast = toDottedDecimal $($ipBinary.substring(0,$netBits).padright(32,"1"))
        $broadCastbinary = $ipBinary.substring(0,$netBits).padright(32,"1")
        $wildcard = toDottedDecimal ($wildcardbinary)
        $Hostspernet = ([convert]::ToInt32($broadCastbinary,2) - [convert]::ToInt32($networkIDbinary,2)) - 1
       }

    # Subnet mask is 32 (CIDR)
    else {

        # Validate the IP address
        if($ipBinary.length -ne 32) {
            Write-Warning "IP Address is invalid!"
            Exit
            }

        #identify subnet boundaries
        $networkID = toDottedDecimal $($ipBinary)
        $networkIDbinary = $ipBinary
        $firstAddress = toDottedDecimal $($ipBinary)
        $firstAddressBinary = $ipBinary
        $lastAddress = toDottedDecimal $($ipBinary)
        $lastAddressBinary = $ipBinary
        $broadCast = toDottedDecimal $($ipBinary)
        $broadCastbinary = $ipBinary
        $wildcard = toDottedDecimal ($wildcardbinary)
        $Hostspernet = 1
        $cidr = 32
        }

    #region Output

    # Include a ipcalc.pl style text output (not an object)
    if ($IncludeTextOutput) {
        Write-Host "`nAddress:`t`t$IPAddress"
        Write-Host "Netmask:`t`t$Netmask = $cidr"
        Write-Host "Wildcard:`t`t$wildcard"
        Write-Host "=>"
        Write-Host "Network:`t`t$networkID/$cidr"
        Write-Host "Broadcast:`t`t$broadCast"
        Write-Host "HostMin:`t`t$firstAddress"
        Write-Host "HostMax:`t`t$lastAddress"
        Write-Host "Hosts/Net:`t`t$Hostspernet`n"
        }

    # Output custom object with or without binary information.
    if ($IncludeBinaryOutput) {
        [PSCustomObject]@{
            Address = $IPAddress
            Netmask = $Netmask
            Wildcard = $wildcard
            Network = "$networkID/$cidr"
            Broadcast = $broadCast
            HostMin = $firstAddress
            HostMax = $lastAddress
            'Hosts/Net' = $Hostspernet
            AddressBinary = $ipBinary
            NetmaskBinary = $smBinary
            WildcardBinary = $wildcardbinary
            NetworkBinary = $networkIDbinary
            HostMinBinary = $firstAddressBinary
            HostMaxBinary = $lastAddressBinary
            BroadcastBinary = $broadCastbinary
            }
        }
    else {
        [PSCustomObject]@{
            Address = $IPAddress
            Netmask = $Netmask
            Wildcard = $wildcard
            Network = "$networkID/$cidr"
            Broadcast = $broadCast
            HostMin = $firstAddress
            HostMax = $lastAddress
            'Hosts/Net' = $Hostspernet
            }
        }
    #endregion
}
function Test-TaskStatus()
{
    <#  .SYNOPSIS
        version 0.2 - Written by Dave Hocking 2017.

        This function will check (and report on) or wait for a specifed VCD task state.
        If the operation is to "wait" for the state specified, you can set a timeout value
        If the timeout value is exceeded and the desired state hasn't changed, an error is thrown

        Requires RestAPI connection to vCloud Director - see "connect-ciREST"
        Requires a "$url" as a href for the task in VCD - returned when you submit a job

        .DESCRIPTION
        Here are the possible states you can check for:

        NAME	             DESCRIPTION
        queued               The task has been queued for execution.
        preRunning           The task is awaiting preprocessing or, if it is a blocking task, administrative action.
        running              The task is runnning.
        success              The task completed with a status of success.
        error                The task encountered an error while running.
        canceled             The task was canceled by the owner or an administrator.
        aborted              The task was aborted by an administrative action.

        .EXAMPLE
        If you want to script a task that should wait 5 mins for the task at $url to switch to "Success":
        Test-TaskStatus $url -wait -for success -mins 5

        .EXAMPLE
        If you want to script a task that should wait 2 mins for the task at $url to switch to "Success":
        Test-TaskStatus $url -wait -for success -mins 2 -quiet
         - The quiet switch will prevent periodic reporting of the task status
         - Useful if vCD isn't going to report true % progress!

        .EXAMPLE
        If you wanted to return $true or $false for the status or "error":
        Test-TaskStatus $url -check -for error

        .EXAMPLE
        If you wanted to simply report the task state given it's $url:
        Test-TaskStatus $url -report
    #>

    # Allow advanced parameter functions
    [CmdletBinding()]

    # Define the Parameters to pass the to function
    Param(
    # Require a target url
    [Parameter(Position=0,Mandatory=$True)]
    # Check that the $url object passed is indeed a VCD task object
    [ValidateScript({Invoke-WebRequest -Headers $ciHeaders -Uri $_})]
    $url
    ,
    [Parameter(Mandatory=$true,ParameterSetName="Wait")]
    [switch]$wait
    ,
    [Parameter(Mandatory=$true,ParameterSetName="Check")]
    [switch]$check
    ,
    [Parameter(Mandatory=$true,ParameterSetName="Report")]
    [switch]$report
    ,
    [Parameter(Mandatory=$true,ParameterSetName="Check")]
    [Parameter(Mandatory=$true,ParameterSetName="Wait")]
    [ValidateSet("queued","preRunning","running","success","error","canceled","aborted")]
    [string]$for
    ,
    # Validate that any -mins specified are with the range below
    [Parameter(ParameterSetName="Wait")]
    [ValidateRange(1,60)]
    [Int]$mins
    ,

    # Optional -quiet switch to prevent %-status reporting
    [Parameter(ParameterSetName="Wait")]
    [switch]$quiet
    )

    # Re-write variable for clarity
    $desiredStatus = $for

    # Set up the "wait" operation
    if ($wait)
    {
        Write-Host "Waiting for task status of " -NoNewline
        Write-Host "$desiredStatus" -ForegroundColor Yellow -NoNewline
        Write-Host "..."

        # Set interval between tests
        $intervalSecs = 10

        # If $mins hasn't been populated, use a default of 15
        if (!$mins)
        {
            $mins = 15
        }

        # Determine the correct number of iterations to run through
        $iterations = ($mins*60)/$intervalSecs

        # Create loop to run for the specified length of time
        for ($i=1; $i -le $iterations; $i++)
        {
            start-sleep $intervalSecs
            $task = (Invoke-RestMethod -Headers $ciHeaders -Method Get -Uri $url).task
            $urlStatus = $task.status
            $pctComplete = $task.progress

            # Check for -quiet switch and alter output as required
            if (!$PSBoundParameters.ContainsKey("quiet"))
            {
                write-host "Task Status is: " -NoNewline
                Write-Host "$($urlStatus)" -ForegroundColor Yellow -NoNewline
                write-host " / " -NoNewline
                Write-Host "$($pctComplete)%" -ForegroundColor Yellow
            }

            # Check if the actual task status matches any of the desiredStatus combinations
            $match = $desiredStatus.Contains($urlStatus.ToString())
            if ($match -eq $True)
            {
                $result = "complete"
                break
            }
        }

        if ($result)
        {
            "Desired status of `"" + $desiredStatus + "`" was reached."
        }

        # If status doesn't change, display error and stop script
        else
        {
            write-warning "Failed to reach desired status within the time allocated."
            $task = (Invoke-RestMethod -Headers $ciHeaders -Method Get -Uri $url).task
            Write-Host "The task status is `"" -NoNewline
            Write-Host "$($task.Status)" -ForegroundColor Yellow -NoNewline
            Write-Host "`""
            break
        }
    }

    # Set up the "check" operation
    if ($check)
    {
        $taskStatus = (Invoke-RestMethod -Headers $ciHeaders -Method Get -Uri $url).task.status
        if ($taskStatus -eq $desiredStatus)
        {
        return $true
        }
        else
        {
        return $false
        }
    }

    # Set up the "report" operation
    if ($report)
    {
        $task = (Invoke-RestMethod -Headers $ciHeaders -Method Get -Uri $url).task
        Write-Host "The task status is `"" -NoNewline
        Write-Host "$($task.Status)" -ForegroundColor Yellow -NoNewline
        Write-Host "`" and it's " -NoNewline
        Write-Host "$($task.Progress)" -NoNewline -ForegroundColor DarkYellow
        Write-Host "% complete."
    }
}

function Get-vCloudInventory()
{
    <#
    .SYNOPSIS
        version 0.1 - Written by Dave Hocking 2016.
        This function will output tables of vCloud Director objects for reporting purposes

        Requires PowerCLI Support and connections to vCloud Director and the Tenant vCenter(s) associated


    .DESCRIPTION
        No inputs required.

        - Gets all Organisations
        - Loops through each and collects inventory data in a table

    .EXAMPLE
        Get-vCloudInventory
        - Collects all data and displays output in a number of tables.

    .EXAMPLE
        Get-vCloudInventory -saveTo "C:\Temp"
        - Collects all data and displays output in a number of tables.
        - Saves a CSV and HTML copy of the tables locally in the path specified.

    .EXAMPLE
        Get-vCloudInventory -mailTo "dhocking@proact.co.uk tsimons@proact.co.uk"
        - Collects all data and displays output in a number of tables.
        - Emails the attachments to the users specifed, using the mailserver specifed in Import-vCDcommonVars()
        - Use Space-Delimitation for multiple recipients

    #>

     # Allow advanced parameter functions
    [CmdletBinding()]

    # Define the Parameters to pass the to function
    Param(
    # Optional path for local storage of HTML/CSV Output
    [Parameter(Mandatory=$false)]
    [ValidateScript({Test-Path $_ })]
    [string]$saveTo,

    # Optional list of email recipients for the csv/html export
    [Parameter(Mandatory=$false)]
    [string]$mailTo
    )

    # Get the Date
    $date = (Get-Date -Format g).split(' ')[0]

    # Start the transcript
    $transcriptPath = "$($env:USERPROFILE)\Documents\get-vCloudInventory.log"
    Start-Transcript -Path $transcriptPath

    # Requires connection to vCloud Server and vCenter
    Import-vCDcommonVars
    connect-ci | Out-Null
    connect-vi | Out-Null

    # Show Header
    show-header

    # Start-Processing
    Write-Output "Creating Tables to hold data..."

    # Get the Date / Time
    $date = (Get-Date -Format g).split(' ')[0]
    $time = (Get-Date -Format g).split(' ')[-1]

    #------------------------------------------------------------------

    # Create Table object for Org Inventory
    $orgTable = New-Object system.Data.DataTable "OrgInventory"

    # Define Columns
    $cols = @()
    $cols += New-Object system.Data.DataColumn Date,([string])
    $cols += New-Object system.Data.DataColumn Org_Name,([string])
    $cols += New-Object system.Data.DataColumn Tenant_ID,([string])
    $cols += New-Object system.Data.DataColumn Full_Name,([string])
    $cols += New-Object system.Data.DataColumn Org_VDCs,([int])
    $cols += New-Object system.Data.DataColumn Catalogs,([int])
    $cols += New-Object system.Data.DataColumn vApps,([int])
    $cols += New-Object system.Data.DataColumn VMs,([int])
    $cols += New-Object system.Data.DataColumn Templates,([int])

    #Add the Columns
    foreach ($col in $cols) {$orgTable.columns.add($col)}

    #------------------------------------------------------------------

    # Create Table Object for vApp Inventory
    $vAppTable = New-Object system.Data.DataTable "vAppInventory"

    # Define Columns
    $cols = @()
    $cols += New-Object system.Data.DataColumn Date,([string])
    $cols += New-Object system.Data.DataColumn Org_Name,([string])
    $cols += New-Object system.Data.DataColumn vCD_vApp_Name,([string])
    $cols += New-Object system.Data.DataColumn vCD_VM_Name,([string])
    $cols += New-Object system.Data.DataColumn vSphere_VM_Name,([string])
    $cols += New-Object system.Data.DataColumn Running,([bool])
    $cols += New-Object system.Data.DataColumn Tools,([string])
    $cols += New-Object system.Data.DataColumn Backup_Metadata,([string])
    $cols += New-Object system.Data.DataColumn Notes,([string])
    $cols += New-Object system.Data.DataColumn vCPUs,([int])
    $cols += New-Object system.Data.DataColumn Mem_GB,([int])
    $cols += New-Object system.Data.DataColumn Disk_Use_GB,([int])
    $cols += New-Object system.Data.DataColumn Net_PGname,([string])
    $cols += New-Object system.Data.DataColumn Net_IPaddr,([string])

    #Add the Columns
    foreach ($col in $cols) {$vAppTable.columns.add($col)}

    #------------------------------------------------------------------

    # Create Table Object for vAppTemplate Inventory
    $vAppTemplateTable = New-Object system.Data.DataTable "vAppTableInventory"

    # Define Columns
    $cols = @()
    $cols += New-Object system.Data.DataColumn Date,([string])
    $cols += New-Object system.Data.DataColumn Org_Name,([string])
    $cols += New-Object system.Data.DataColumn vCD_Template_Name,([string])
    $cols += New-Object system.Data.DataColumn vCD_Template_VM_Name,([string])
    $cols += New-Object system.Data.DataColumn vSphere_VM_Name,([string])
    $cols += New-Object system.Data.DataColumn Backup_Metadata,([string])
    $cols += New-Object system.Data.DataColumn Notes,([string])
    $cols += New-Object system.Data.DataColumn vCPUs,([int])
    $cols += New-Object system.Data.DataColumn Mem_GB,([int])
    $cols += New-Object system.Data.DataColumn Disk_Use_GB,([int])

    # Add the Columns
    foreach ($col in $cols) {$vAppTemplateTable.columns.add($col)}

    #------------------------------------------------------------------
    done

    # Get VDPortGroups
    Write-Output "Collecting dvPortgroup Names..."
    $VDPortgroup = Get-VDPortgroup
    done

    # Collect the Inventory Data for each Org
    Write-Output "Collecting Org/vSphere Data, this will take some time..."
    Write-Output "The current time is $($time)"

    foreach ($org in get-org)
    {
        # Get the objects you need for each Org
        $orgName = $org.Name
        $tenantID = ($org.Id).Split(':')[-1]
        $fullName = $org.FullName
        $orgVDCs = $org | Get-OrgVdc
        $catalogs = $org | Get-Catalog
        $vApps = $org | Get-ciVApp
        $vms = $org | Get-CIVM
        $templates = $catalogs | Get-CIVAppTemplate

        # Calculate the Quantities needed for inclusion in the table
        $noOrgVDCs = $orgVDCs.count
        $noCatalogs = $catalogs.count
        $noVapps = $vApps.count
        $noVMs = $vms.count
        $noTemplates = $templates.count

        #------------------------------------------------------------------

        # Populate a row in the Org Inventory Table
        $row = $orgTable.NewRow()

        # Enter data in the row
        $row.Date = $date
        $row.Org_Name = $orgName
        $row.Tenant_ID = $tenantID
        $row.Full_Name = $fullName
        $row.Org_VDCs = $noOrgVDCs
        $row.Catalogs = $noCatalogs
        $row.vApps = $noVapps
        $row.VMs = $noVMs
        $row.Templates = $noTemplates

        # Add the row to the table
        $orgTable.Rows.Add($row)

        #------------------------------------------------------------------

        # Loop through each vApp gathered for the Org in this loop
        foreach ($vApp in $vApps)
        {
            # Get the Vms inside the vApp
            $ciVMs = $vApp | Get-CIVM

            # Loop through each CIVM in the CIvApp
            foreach ($ciVM in $ciVMs)
            {
                # Get the VM Properties
                $vm = $ciVM.toVirtualMachine()

                # Get CIVM metadata
                $metadata = ($civm.ExtensionData.GetMetadata().metadataentry)

                # Search for the Backup Policy Data, this MUST be unique, meaning no conflicts are possible
                if (@($metadata.key).Contains("Backup Policy"))
                {
                    $backupMetadata = $metadata | where-object {$_.key -match "Backup Policy"}
                    $policy = "`"$($backupMetadata.TypedValue.value)`""

                }
                else
                {
                    $policy = "na"
                }

                # Collect the portgroups as a string of names
                $dvPgs = $vm.ExtensionData.Network
                $dvPgarray = @()
                foreach ($dvpg in $dvPgs)
                {
                    $dvPgarray += ($VDPortgroup | where-object {$_.key -match $dvpg.value}).Name
                }
                $dvPgs = $dvPgarray -join ","

                # Populate a row in the vApp Inventory Table
                $row = $vAppTable.NewRow()

                # Enter data in the row
                $row.Date = $date
                $row.Org_Name = $orgName
                $row.vCD_vApp_Name = $vApp.name
                $row.vCD_VM_Name = $ciVM.Name
                $row.vSphere_VM_Name = $vm.name
                $row.Running = $vm.powerstate
                $row.Tools = $vm.extensiondata.guest.ToolsStatus
                $row.Backup_Metadata = $policy
                $row.Notes = $vm.notes
                $row.vCPUs = $vm.numcpu
                $row.Mem_GB = $vm.memoryGB
                $row.Disk_Use_GB = $vm.usedSpaceGB
                $row.Net_PGname = $dvPgs
                $row.Net_IPaddr = $vm.ExtensionData.Guest.Net.ipaddress -join ","

                # Add the row to the table
                $vAppTable.Rows.Add($row)
            }
        }

        #------------------------------------------------------------------

        # Loop through each vAppTemplate gathered for the Org in this loop
        foreach ($vAppTemplate in $templates)
        {
            # Get the Vms inside the vAppTemplate
            $ciVMs = $vAppTemplate.ExtensionData.Children.Vm

            # Get CIvApp Template metadata
            $metadata = ($vAppTemplate.ExtensionData.GetMetadata().metadataentry)

            # Search for the Backup Policy Data, this MUST be unique, meaning no conflicts are possible
            if (@($metadata.key).Contains("Backup Policy"))
            {
                $backupMetadata = $metadata | where-object {$_.key -match "Backup Policy"}
                $policy = "`"$($backupMetadata.TypedValue.value)`""

            }
            else
            {
                $policy = "na"
            }

            # Loop through each CIVM in the CIvApp
            foreach ($ciVM in $ciVMs)
            {
                # Get the VM Properties
                $vmID = $ciVM.id.Split(':')[-1]
                $vm = get-vm -name "*($($vmID))"

                # Populate a row in the vApp Template Inventory Table
                $row = $vAppTemplateTable.NewRow()

                # Enter data in the row
                $row.Date = $date
                $row.Org_Name = $orgName
                $row.vCD_Template_Name = $vAppTemplate.name
                $row.vCD_Template_VM_Name = $ciVM.Name
                $row.vSphere_VM_Name = $vm.name
                $row.Backup_Metadata = $policy
                $row.Notes = $vm.notes
                $row.vCPUs = $vm.numcpu
                $row.Mem_GB = $vm.memoryGB
                $row.Disk_Use_GB = $vm.usedSpaceGB

                # Add the row to the table
                $vAppTemplateTable.Rows.Add($row)
            }
        }
        #------------------------------------------------------------------

    }

    # Get the new time
    $time = (Get-Date -Format g).split(' ')[-1]
    Write-Output "Processing complete, time is now $($time)"
    done

    # Display the tables
    Write-Host "Organisation Inventory" -ForegroundColor Yellow
    $orgTable | select-object Org_Name,Tenant_ID,Full_Name,Org_VDCs,Catalogs,vApps,VMs,Templates | format-table -AutoSize

    Write-Host "vApp Inventory" -ForegroundColor Yellow
    $vAppTable | select-object Org_Name,vCD_vApp_Name,vCD_VM_Name,Running,Tools,Backup_Metadata,Notes,vCPUs,Mem_GB,Disk_use_GB | Format-Table -AutoSize

    Write-Host "Template Inventory" -ForegroundColor Yellow
    $vAppTemplateTable | select-object Org_Name,vCD_Template_Name,vCD_Template_VM_Name,Backup_Metadata,Notes,vCPUs,Mem_GB,Disk_use_GB | Format-Table -AutoSize

    # Check if -mailTo or -sendTo were specified, then carry out tasks
    if ($PSBoundParameters.ContainsKey("mailTo") -or $PSBoundParameters.ContainsKey("saveTo"))
    {
        if ($PSBoundParameters.ContainsKey("saveTo"))
        {
            $outputDir = $saveTo
        }

        if ($PSBoundParameters.ContainsKey("mailTo"))
        {
            $outputDir = "$($env:USERPROFILE)\Documents"
        }

        Write-Output "Tables being converted to CSV/HTML for saving/sending..."

        # Define Export Locations
        $outHTMLOrgTable = "$($outputDir)\orgTable.html"
        $outHTMLvAppTable = "$($outputDir)\vappTable.html"
        $outHTMLTemplateTable = "$($outputDir)\templateTable.html"

        $outCSVOrgTable = "$($outputDir)\orgTable.csv"
        $outCSVvAppTable = "$($outputDir)\vappTable.csv"
        $outCSVTemplateTable = "$($outputDir)\templateTable.csv"

        # Build the Header with CSS for HTML export
        $Header = @"
        <style>
        TABLE {
            table-layout: fixed;
            font-family: "Myriad Web",Verdana,Helvetica,Arial,sans-serif;
            font-size: 10pt;
            border-collapse: collapse;
        }

        TH {
            color: white;
            width: 120px;
            text-align: center;
            word-wrap: break-word;
            border-width: 1px;
            padding: 3px;
            border-style: solid;
            border-color: #818181;
            background-color: #ff0000;
        }

        TD {
            text-align: center;
            border-width: 1px;
            padding: 3px;
            border-style: solid;
            border-color: #818181;
        }

        p.sansbold {
            font-family: Arial, Helvetica, sans-serif;
            font-weight: bold;
        }

        </style>
        <title>
        vCloud Director System Inventory
        </title>
"@

        # Define the columns needed in the HTML export
        $orgTableOut = $orgTable | select-object Date,Org_Name,Tenant_ID,Full_Name,Org_VDCs,Catalogs,vApps,VMs,Templates
        $vAppTableOut = $vAppTable | select-object Date,Org_Name,vCD_vApp_Name,vCD_VM_Name,vSphere_VM_Name,Running,Tools,Backup_Metadata,Notes,vCPUs,Mem_GB,Disk_use_GB,Net_PGname,Net_IPaddr
        $vAppTemplateTableOut = $vAppTemplateTable | select-object Date,Org_Name,vCD_Template_Name,vCD_Template_VM_Name,vSphere_VM_Name,Backup_Metadata,Notes,vCPUs,Mem_GB,Disk_use_GB

        # Export content to HTML and apply CSS from Header, setting pre export content
        $orgTableHTMLPre = @"
        <p class=`"sansbold`">Organisation Inventory</p>
"@
        $vAppTableHTMLPre = @"
        <p class=`"sansbold`">vApp Inventory</p>
"@
        $vAppTemplateTableHTMLPre = @"
        <p class=`"sansbold`">vApp Template Inventory</p>
"@

        ""
        Write-Host "Exporting Tables to HTML..." -ForegroundColor Yellow
        $orgTableOut | ConvertTo-Html -Head $Header -PreContent $orgTableHTMLPre > $outHTMLOrgTable
        $vAppTableOut | ConvertTo-Html -Head $Header -PreContent $vAppTableHTMLPre > $outHTMLvAppTable
        $vAppTemplateTableOut | ConvertTo-Html -Head $Header -PreContent $vAppTemplateTableHTMLPre > $outHTMLTemplateTable
        Write-Host "Check in $($outputDir)"
        done

        Write-Host "Exporting Tables to CSV..." -ForegroundColor Yellow
        $orgTableOut | ConvertTo-Csv > $outCSVOrgTable
        $vAppTableOut | ConvertTo-Csv > $outCSVvAppTable
        $vAppTemplateTableOut | ConvertTo-Csv > $outCSVTemplateTable
        Write-Host "Check in $($outputDir)"
        done

        Write-Output "Table Processing Complete."

        # Stop logging the commands
        Stop-Transcript

        # If the option was to mail the reports, do that now
        if ($PSBoundParameters.ContainsKey("mailTo"))
        {
           # $mailFrom = "$($env:COMPUTERNAME) <noreply@$($env:COMPUTERNAME).$($env:USERDNSDOMAIN)>"
            # Had to set the mail from field to my mailbox, to ensure delivery through the relays
            $mailFrom = "$($env:COMPUTERNAME) <$($emailAuthor)>"
            $subject = "vCloud Organisation Inventory"
            $body = @"
<font face="arial">
<b> Please see the attachments for CSV and HTML reports of vCloud Inventory Objects </b>
<p>
The list of Organisations, their Tenant ID, vApps and the associated vSphere Names <br>
will help join the Customer entities to the objects visible in vSphere / CommVault <br>
</p>
<p>
This email also contains a log of the execution state of the script and should be <br>
checked for correct operation.<br>
</p>
<p>
This email report should be generated daily, the CSV files can be used to track changes <br>
such as the growth in consumed resources or provisioned tenants. <br>
</p>
<p>
This mail was automatically generated on the PowerShell Host "$($env:COMPUTERNAME)" <br>
If you stop receiving these messages, there may be an issue that needs resolving. <br>
If there is a problem or enhancement to make, email $($emailAuthor) for assistance. <br>
</p>
</font>
<FONT COLOR="#FF00000" FACE="consolas">
<PRE>
 ___ ___  ___   _   ___ _____
| _ \ _ \/ _ \ /_\ / __|_   _|
|  _/   / (_) / _ \ (__  | |
|_| |_|_\\___/_/ \_\___| |_|
 delivering business agility
</PRE>

"@

            # Send the mail message with attachements
            Write-Host "Sending eMail to Recipients - $($mailTo)..." -ForegroundColor Yellow
            Send-MailMessage -Attachments "$($outCSVOrgTable)","$($outCSVvAppTable)","$($outCSVTemplateTable)",`
                                          "$($outHTMLOrgTable)","$($outHTMLvAppTable)","$($outHTMLTemplateTable)", `
                                          "$($transcriptPath)" `
                             -Body $body -From $mailFrom -SmtpServer $mailServer -Subject $subject -To $mailTo -BodyAsHtml
            done
        }

    }

    # If running with no additional switches
    else
    {
        # Stop logging the commands
        Stop-Transcript
    }

    # All done, disconnect sessions
    Write-Host "Disconnecting CI/VI Sessions..." -ForegroundColor Yellow
    disconnect-ci
    disconnect-vi
    done

}

function Set-vCloudBackupPolicy()
{
    <#
    .SYNOPSIS
        version 0.1 - Written by Dave Hocking 2016.
        Created to convert vCD Metadata on "Backup Policy" into VM Notes in vCenter
        CommVault Backup Policies are based on vSphere Properties, this acts as a go-between

        Requires PowerCLI Support and connections to vCloud Director and the Tenant vCenter(s) associated


    .DESCRIPTION
        No inputs required.

        - Gets all Organisations
        - Loops through each and collects inventory data in a table

    .EXAMPLE
        Set-vCloudBackupPolicy
        - Collects all CI metadata and sets the VM Notes fields accordingly.

    .EXAMPLE
        Set-vCloudBackupPolicy -mailTo "dhocking@proact.co.uk tsimons@proact.co.uk"
        - Collects all CI metadata and sets the VM Notes fields accordingly.
        - Emails the attachments to the users specifed, using the mailserver specifed in Import-vCDcommonVars()
        - Use Space-Delimitation for multiple recipients

    #>

     # Allow advanced parameter functions
    [CmdletBinding()]

    # Define the Parameters to pass the to function
    Param(
    # Optional list of email recipients for the transcript
    [Parameter(Mandatory=$false)]
    [string]$mailTo
    )

    # Get the Date
    $date = (Get-Date -Format g).split(' ')[0]

    # Start the transcript
    $transcriptPath = "$($env:USERPROFILE)\Documents\Set-vCloudBackupPolicy.log"
    Start-Transcript -Path $transcriptPath
    Write-Output "Importing common variables and initialising PowerCLI support..."

    # Get common variables imported
    Import-vCDcommonVars

    # Requires connection to ci-server and...
    # Requires connection to tenant vi-server
    connect-ci | Out-Null
    connect-vi | Out-Null
    done


    # Collect All CUSTOMER Orgs - exclude provider org name from common vars
    Write-Output "Collecting all customer orgs."
    $orgs = Get-Org | where-object {$_.name -notmatch $providerOrgName}

    # Loop through each org
    foreach ($org in $orgs)
    {
        Write-Output "Customer : $($org.name)"
        Write-Output "Processing vApps..."
        # Clear out the hashtables
        $vmHashTable = @{}
        $vAppTemplateHashTable = @{}

        # Loop through each VM in each Org
        foreach ($civm in ($org | get-civm))
        {
            # Get the vSphere VM name (as this is Unique)
            $vm = $civm.ToVirtualMachine()

            # Get all VM metadata
            $metadata = ($civm.ExtensionData.GetMetadata().metadataentry)

            # Search for the Backup Policy Data, this MUST be unique, meaning no conflicts are possible
            if (@($metadata.key).Contains("Backup Policy"))
            {
                $backupMetadata = $metadata | where-object {$_.key -match "Backup Policy"}
                $policy = $backupMetadata.TypedValue.value
                Write-Output "Policy Data found for $($vm.name) : $policy"

            }

            # Label the VM with no backup policy defined, for inclusion into the Default Policy
            else
            {
                $policy = "DEFAULT"
                Write-Output "No Policy Data found for $($vm.name) : $policy will be used"
            }

            # Add the VMname and Policy to the Hashtable
            $vmHashTable.Add("$($vm.name)","$($policy)")

            # Set the desired state for the VM Notes field (CommVault uses this to config backup jobs)
            $setNotes = "$($org.Name)_$($policy)"

            # Get the VM Notes and compare to the desired state
            if ($vm.notes -ne $setNotes)
            {
                # Set the VM Notes Field to match the Backup Policy metadata
                Write-Output "VM Notes field is not equal to the desired option, fixing..."
                $vm | set-vm -Notes $($setNotes) -Confirm:$false | Out-Null
            }
        }
        Write-Output "Processing complete for vApps"
        Write-Output "Starting processing vApp Templates..."
        # Loop through each vApp Template in each Org
        foreach ($ciVappTemplate in ($org | Get-Catalog | Get-CIVAppTemplate))
        {

            # Get the UID for the folder holding the VM inside vAppTemplate
            $uid = ($ciVappTemplate.ExtensionData.Id).Split(':')[-1]

            # Get the vSphere VM name (as this is Unique)
            $vm = get-folder -Name *$($uid)* | Get-VM

            # Get all vAppTemplate metadata
            $metadata = ($ciVappTemplate.ExtensionData.GetMetadata().metadataentry)

            # Search for the Backup Policy Data, this MUST be unique, meaning no conflicts are possible
            if (@($metadata.key).Contains("Backup Policy"))
            {
                $backupMetadata = $metadata | where-object {$_.key -match "Backup Policy"}
                $policy = $backupMetadata.TypedValue.value
                Write-Output "Policy Data found for $($vm.name) : $policy"
            }

            # Label the vAppTemplate with no backup policy defined, for inclusion into the Default Policy
            else
            {
                $policy = "DEFAULT"
                Write-Output "No Policy Data found for $($vm.name) : $policy will be used"
            }

            # Set the desired state for the VM Notes field (CommVault uses this to config backup jobs)
            $setNotes = "$($org.Name)_$($policy)"

            # Get the VM Notes and compare to the desired state
            if ($vm.notes -ne $setNotes)
            {
                # Set the VM Notes Field to match the Backup Policy metadata
                Write-Output "VM Notes field is not equal to the desired option, fixing..."
                $vm | set-vm -Notes $($setNotes) -Confirm:$false | Out-Null
            }

            # Add the VM template name and Policy to the Hashtable
            $vAppTemplateHashTable.Add("$($vm.name)","$($policy)")

        }
        Write-Output "Processing complete for vApp Templates"

        # Show the hashtable, sorted on Backup Policy
        #$vmHashTable | sort value

        # Show the hashtable, sorted on Backup Policy
        #$vAppTemplateHashTable | sort value
        ""
    }

    # Stop processing the transcript
    Stop-Transcript

    if ($PSBoundParameters.ContainsKey("mailTo"))
    {
        # Send the mail message with attachments
        $mailFrom = "$($env:COMPUTERNAME) <$($emailAuthor)>"
        $subject = "vCloud Metadata -> vSphere Notes"
        $body = @"
<font face="arial">
<b> Please see the attachments for the transcript logs of this process.</b>
<p>
This email contains a log of the execution state of the script and should be <br>
checked for correct operation.<br>
</p>
<p>
This mail was automatically generated on the PowerShell Host "$($env:COMPUTERNAME)" <br>
If you stop receiving these messages, there may be an issue that needs resolving. <br>
If there is a problem or enhancement to make, email $($emailAuthor) for assistance. <br>
</p>
</font>
<FONT COLOR="#FF00000" FACE="consolas">
<PRE>
 ___ ___  ___   _   ___ _____
| _ \ _ \/ _ \ /_\ / __|_   _|
|  _/   / (_) / _ \ (__  | |
|_| |_|_\\___/_/ \_\___| |_|
 delivering business agility
</PRE>

"@

        Write-Host "Sending eMail to Recipients - $($mailTo)..." -ForegroundColor Yellow
        Send-MailMessage -Attachments "$($transcriptPath)" `
                        -Body $body -From $mailFrom -SmtpServer $mailServer -Subject $subject -To $mailTo -BodyAsHtml
        done
    }

    # All done, tidy up.
    disconnect-ci
    disconnect-vi
}

    # Define function for displaying end result
function add-vCloudOrgEndResult()
{
    Import-vCDcommonVars

    #$outURL = "vCloud URL : https://$($ciserver)/cloud/org/$orgName"
    # Commented out because the Netscaler will be handling SAML and user-redirection, publishing one URL for everyone.
    $outURL = "vCloud URL : https://$($ciserver)"
    $outOrgName = "vCloud Org : $($orgName)"
    #$outEmail = "Username   : $($adminEmail)"
    #$outPass = "Password   : $($user.Password)"
    # Commented out because user creations to be done in AD, with 2FA signup, by MCS staff for now
    $outOrgID = "Tenant ID  : $($orgID)"
    $outESGip = "Gateway IP : $($edgeGatewayExtIP)"

    Write-Host "_______________________________________________________________" -ForegroundColor DarkGreen
    Write-Host $outURL
    #Write-Host $outEmail
    #Write-Host $outPass
    write-host $outOrgName
    Write-Host $outOrgID
    Write-Host $outESGip
    Write-Host ""
    # Display message about user creation
    Write-Host "No users have been created in the Organisation at this stage"
    Write-Host "MCS Staff to create the users in the OU : $($orgADOU)"
    Write-Host ""
    # Display message about NAT/FW rule creation
    Write-Host "Consider using `"Add-vCDEdgeFWRule`" and `"Add-vCDEdgeNatRule`""
    Write-Host "To customise the FW/NAT rules on the customer edge"
    Write-Host "_______________________________________________________________" -ForegroundColor DarkGreen
    Write-Host ""

    #Write-Output "$($outURL)`n$($outEmail)`n$($outPass)`n$($outOrgID)" | clip
    Write-Output "$($outURL)`n$($outOrgName)`n$($outOrgID)`n$($outESGip)" | clip
    Write-Host -BackgroundColor White -ForegroundColor Red "THE ABOVE DETAILS ARE NOW IN YOUR CLIPBOARD"
    ""
}

function Add-vCloudOrg()
{
    <#
        .SYNOPSIS
            version 0.6 - Written by Dave Hocking 2017.
            This function will create the vCloud Director objects associated with a customer

            Requires PowerCLI Support


        .DESCRIPTION
            Prompts for the inputs, before creating the organisation and associated objects to best-practice.

            - Creates the Organisation
            - Prompts the user to create one or more Org VDCs

            RELATED COMMANDS
            - Add-vCloudOrgVDC
            - Remove-vCloudOrg
            - Remove-vCloudOrgVDC

        .EXAMPLE
           Add-vCloudOrg -corpName "Acme Widgets Inc" -countryCode "DK" -companyCode "ACM"
            - Creates the vCD Organisation "DK-ACM" for the full name "Acme Widgets Inc"

    #>

    # Allow advanced parameter functions
    [CmdletBinding()]

    # Define the Parameters to pass the to function
    Param(
    [Parameter(Mandatory=$true)]
    [String]$corpName,

    [Parameter(Mandatory=$true)]
    [ValidateSet("BE","CZ","DE","DK","EE","ES","FI","GB","LT","LV","NL","NO","SE","SK","US")]
    [String]$countryCode,

    [Parameter(Mandatory=$true)]
    [ValidateLength(3,3)]
    [String]$companyCode,

    #[Parameter(Mandatory=$true)]
    #[ValidateScript({[bool]($_ -as [Net.Mail.MailAddress])})]
    #[String]$adminEmail,

    [Parameter(Mandatory=$false)]
    [Switch]$labs
    )


    # Import Modules / Functions / Variables
    Import-vCDcommonVars

    # Display company header
    show-header

    # Populate Country Codes
    $countryCodes=@{}
    $countryCodes.Add("BE","Belgium")
    $countryCodes.Add("CZ","Czech Republic")
    $countryCodes.Add("DE","Germany")
    $countryCodes.Add("DK","Denmark")
    $countryCodes.Add("EE","Estonia")
    $countryCodes.Add("ES","Spain")
    $countryCodes.Add("FI","Finland")
    $countryCodes.Add("GB","Great Britain")
    $countryCodes.Add("LT","Lithuania")
    $countryCodes.Add("LV","Latvia")
    $countryCodes.Add("NL","Netherlands")
    $countryCodes.Add("NO","Norway")
    $countryCodes.Add("SE","Sweden")
    $countryCodes.Add("SK","Slovak Republic")
    $countryCodes.Add("US","U.S.A.")

    # Check the correct Country code has been selected
    Write-Host "The country-code entered for this Organisation was $($countryCode)"
    Write-Host "This code is to be used for " -NoNewline
    Write-Host "$($countryCodes.$($CountryCode))" -ForegroundColor Yellow
    confirm-continue
    if ($break -eq $true)
    {
        break
    }

    #  Set the OrgName
    $orgName = "$($countryCode)-$($companyCode)"

    # Detect if user has called the -labs switch (or if it's been added from above)
    if ($PSBoundParameters.ContainsKey('labs')) {$ciserver = $ciLabsServer; $viserver = $viLabsServer}

    # Use the connect-ci function to handle the correct connection to CI servers
    connect-ci

    # Check for existance of Orgs that match the given name (are we duplicating work?)
    $exists = get-org $orgName -ea ignore
    if ($exists)
    {
        write-warning "An Org with this name already exists. Aborting."
        break
    }

    # Build base Org from supplied company name / org name
    Write-Host "Building vCloud Organisation..."
    New-Org -Name $orgName -FullName $corpName -ErrorVariable noBuild | Out-Null
    if ($noBuild)
    {
        write-warning "Error creating base vCD Organisation for `"$corpName`""
        break
    }
    ""
    done

    # Collect new Org as object
    Write-Host "Collecting new vCloud Organisation as object..."
    $org = Get-Org -Name $orgName
    done

    # Get OrgID for population of Metadata/Description Field - used to tie NSX components to Orgs
    Write-Host "Getting Org Unique ID..."
    $orgID = ($org.Id.Split(':'))[-1]
    Write-Host "$($orgID)" -ForegroundColor Yellow
    done

    # Write the Org ID to the Description Field - not ideal, should be metadata, but this is broken
    Write-Host "Writing ID to Description field..."
    $org | Set-Org -Description "Tenant ID : $(($org.id.Split(':'))[-1])" | Out-Null
    done

    # Set Runtime and Storage Leases for Organisation
    Write-Host "Setting Correct Runtime and Storage Leases for vCloud Organisation..."
    $org.ExtensionData.Settings.VAppLeaseSettings.DeleteOnStorageLeaseExpiration = $true
    $org.ExtensionData.Settings.VAppLeaseSettings.DeploymentLeaseSeconds = 0
    $org.ExtensionData.Settings.VAppLeaseSettings.StorageLeaseSeconds = 0
    $org.ExtensionData.Settings.VAppTemplateLeaseSettings.DeleteOnStorageLeaseExpiration = $true
    $org.ExtensionData.Settings.VAppTemplateLeaseSettings.StorageLeaseSeconds = 0
    # Update settings on vCloud
    $org.ExtensionData.Settings.VAppLeaseSettings.UpdateServerData() | out-null
    $org.ExtensionData.Settings.VAppTemplateLeaseSettings.UpdateServerData() | out-null
    done

    # Remove some of the rights of the Org Admin Role to limit customer capabilities
    Write-Host "Limiting VCD Org Admin Role Rights..."
    Remove-ExcessAdminRoleRights -orgName $orgName -server $ciServer -quiet

    <#
    Create new Admin account for Org
    Write-Host "Creating new Admin account for vCloud Organisation..."
    $user = New-Object VMware.VimAutomation.Cloud.Views.User
    $user.Name = $adminEmail
    $role = Search-Cloud -QueryType Role -Name "Organization Administrator" | Get-CIView
    #$user.password = New-SWRandomPassword -MinPasswordLength 12 -InputStrings 1234567890abcdefghijklmnopqrstuvwxyzABCDEFGHJKLMNPQRSTUVWXYZ!^=-+/,.
    $user.Role = $role.href
    $user.EmailAddress = $adminEmail
    $user.IsEnabled = $true
    #$user.ExtensionData.ProviderType = "SAML"
    $org.ExtensionData.createUser($user) | Out-Null
    done

    # Detect Labs Use and rewrite the ADBase OU variable
    if ($PSBoundParameters.ContainsKey('labs'))
    {
        $ADbaseOU = $labADbaseOU
    }

    # Check for existing OU and/or build one to spec
#    new-vCloudOrgADOU
#    done

    #>

    Write-Host "Empty vCloud Organisation has been built."
    ""

    # Set the EdgeGatewayIP to "Not Built", should the user exit the script at this stage
    $edgeGatewayExtIP = "Not Yet Built"

    # Prompt user to choose building an Org VDC or not
    $choices = @()
    $choices += "Add Org VDC(s)"
    $choices += "Continue without creating Org VDC(s)"
    show-arrayMenu($choices)
    if($break -eq $true)
    {
        # Display output and stop processing
        add-vCloudOrgEndResult
        Write-Host "Exiting...."
        break
    }

    # If the user chose to build Org VDC, you need to know how many Provider VDCs there are
    # and how many the user wants to make available to the Org via VDCs
    if ($object -eq "Add Org VDC(s)")
    {
        # Continue, allowing the building of VDCs
        Write-Host "Calling `"Add-vCloudOrgVDC $($orgName)`" to build VDCs..."

        # Detect and handle labs use
        if ($PSBoundParameters.ContainsKey('labs'))
        {
            Add-vCloudOrgVDC $orgName -labs
        }
        else
        {
            Add-vCloudOrgVDC $orgName
        }
    }

   done

    # Tidy up afterwards
    disconnect-ci
}

function Add-vCloudOrgVDC()
{
    <#
        .SYNOPSIS
            version 0.3 - Written by Dave Hocking 2016.
            This function will create the vCloud Director VDC objects associated with an Organisation
            Requires PowerCLI Support

        .DESCRIPTION
            Prompts for the inputs, before creating the organisation VDC to spec.
            Uses Add-vCDedgeNATrule and Add-vCDEdgeFWrule to pre-build common FW and NAT policies

            - Creates the correct vDCs

            RELATED COMMANDS
            - Add-vCloudOrg
            - Remove-vCloudOrg
            - Remove-vCloudOrgVDC
            - Add-vCDedgeNATrule
            - Add-vCDEdgeFWrule

        .EXAMPLE
           Add-vCloudOrgVDC -orgName "GB-TES"
            - Creates the vCD Org VDC for the Org "GB-TES" using interactive prompts

    #>

    # Allow advanced parameter functions
    [CmdletBinding()]

    # Define the Parameters to pass the to function
    Param(
    [Parameter(Mandatory=$true)]
    [String]$orgName,
    [Parameter(Mandatory=$false)]
    [Switch]$labs
    )

    # Import Modules / Functions / Variables
    Import-vCDcommonVars

    # Detect if user has called the -labs switch (or if it's been added from above)
    if ($PSBoundParameters.ContainsKey('labs')) {$ciserver = $ciLabsServer; $viserver = $viLabsServer}

    # Use the connect-ci function to connect to the CI server
    connect-ci

    # Get the Org Objects
    write-host "Getting the Org object for the $($orgName) Org Name"
    $org = get-org -name $orgName -ea ignore
    if (!$org)
    {
        write-warning "NO ORG COULD BE COLLECTED WITH THAT NAME"
        break
    }
    done

    # Connect via REST as the OrgVDC Template functionality is only available in UI and API (no PoSH)
    write-host "Connecting to REST API, Authentication needed."
    # Handle the labs running a different version than live...
    if ($PSBoundParameters.ContainsKey('labs'))
    {
        connect-ciREST -server $ciserver
    }
    else
    {
        connect-ciREST -server $ciserver
    }

    ""

    # Get Orgs
    Write-Host "Getting Org details..."
    $resource = "/org"
    $url = $baseurl + $resource
    $response = Invoke-RestMethod -Uri $url -Headers $ciHeaders -Method GET
    $orgs = $response.OrgList.org

    # Get link for a given OrgName
    $orgURL = ($orgs | where-object {$_.name -match $orgName}).href
    done

    # Get the OrgVDC Templates Link
    Write-Host "Getting OrgVDC Templates..."
    $response = Invoke-RestMethod -Uri $orgURL -Headers $ciHeaders -Method GET
    $orgVDCtemplatesURL = ($response.Org.Link | where-object {$_.type -match "vdcTemplates"}).href
    if ($orgVDCtemplatesURL -eq $null)
    {
        Write-Warning "No OrgVDC Templates URL returned! - This script needs > vCD v8.1"
        disconnect-ciREST
        break
    }

    # Get a list of available templates
    $response = Invoke-RestMethod -Uri $orgVDCtemplatesURL -Headers $ciHeaders -Method GET
    $orgVDCtemplates = $response.VdcTemplateList.VdcTemplate
    done

    # If 0 Templates exist, quit with friendly warning
    if (@($orgVDCtemplates).Length -eq 0)
    {
        Write-Warning "This deployment script was written to use OrgVDC Templates, create one"
        disconnect-ciREST
        break
    }

    # If >1 Template exists, prompt user to choose a template to use
    if (@($orgVDCtemplates).Length -gt 1)
    {
        Write-Host "More than one OrgVDC Template exists, choose one to use:"
        # Use show-arrayMenu to return $object for the name of the Template desired
        show-arrayMenu $orgVDCtemplates.name
        # If the user quits, capture this and handle it cleanly
        if ($break)
        {
            disconnect-ciREST
            break
        }
        $orgVDCtemplateURL = ($orgVDCtemplates | where-object {$_.name -match $object}).href
        # Set the template name as a string, for use in the VDC name
        $orgVDCtemplateName = $object
    }

    # Therefore, if 1 Template exists, use the first (and only) entry from the list
    else
    {
        $orgVDCtemplateURL = @($orgVDCtemplates)[0].href
        # Set the template name as a string, for use in the VDC name
        $orgVDCtemplateName = @($orgVDCtemplates)[0].name
    }

    # Set the OrgVDCname variable
    $orgVDCname = $orgVDCtemplateName

    # Proceed with building the OrgVDC
    Write-Host "Submitting request to VCD..."
    $resource = "/action/instantiate"
    $url = $orgURL + $resource
    # Define the request body
    $body = "<?xml version=`"1.0`" encoding=`"UTF-8`"?>
    <InstantiateVdcTemplateParams xmlns=`"http://www.vmware.com/vcloud/v1.5`" name=`"$($orgName)-$($orgVDCtemplateName)`">
    <Source href=`"$($orgVDCtemplateURL)`"/>
    </InstantiateVdcTemplateParams>"
    # Use a temporary header
    $tmpheader = $ciHeaders
    $tmpheader += @{"Content-Type" = "application/vnd.vmware.vcloud.instantiateVdcTemplateParams+xml"}

    # Build the OrgVDC from the template
    $response = Invoke-RestMethod -Uri $url -Headers $tmpheader -Method POST -Body $body

    # Check the response was a task of status "queued"
    if ($response.Task.status -notmatch "queued")
    {
        Write-Warning "Couldn't successfully submit request to vCD or task not accepted."
        Write-Host "URL was $($url)"
        Write-Host "Status was `"$($response.Task.status)`""
        Write-Host "Headers:" -NoNewline
        $ciHeaders

        disconnect-ciREST
        break
    }

    # Check for task success
    $url = $response.Task.href
    Test-TaskStatus -wait -for success -url $url -mins 20 -quiet
    done

    # Get the OrgVDC that ends with the same name as the template that was chosen during the build
    # then store the OrgVDC object in the OrgVDC variable
    $orgVDC = get-org $org | get-orgvdc -Name *$orgVDCname

    # Re-write the partial OrgVDC name (was just the name of the template) with the full Name - used for SubAllocation/FW and NAT rules
    $orgVDCname = $orgVDC.name

    # Attempting to Sub-Allocate IP's to the Edge for NAT rule Creation, first get the XMl for the org...
    Write-Host "Attempting to sub-allocate the external IP to the Edge Gateway..."
    $response = Invoke-RestMethod -Uri $orgURL -Headers $ciHeaders -Method get

    # ..now get the vDCs for the Org and check if more than one exists...
    $orgVDCurls = $response.Org.Link | Where-Object {$_.type -eq "application/vnd.vmware.vcloud.vdc+xml"}
    if (($orgVDCurls).count -gt 1)
    {
        <#
        # Prompt the user to choose an OrgVDC to work on
        Write-Host "More than one OrgVDC exists, choose the one you just added."
        show-arrayMenu $orgVDCurls.name
        # If the user quits, capture this and handle it cleanly
        if ($break)
        {
            disconnect-ciREST
            break
        }
        $orgVDCURL = ($orgVDCurls | where-object {$_.name -match $object}).href
        # Use the Object selected here to populate the OrgVDC name, used in FW and NAT rule creation later
        $orgVDCname = $object
        #>

        # re-wrote this section to remove the need to select the orgVDC just added
        $orgVDCURL = ($orgVDCurls | where-object {$_.name -match $orgVDCname}).href
    }
    # If there's only one returned, then just grab that URL to work on
    else
    {
        $orgVDCURL = $orgVDCurls.href
    }

    # ..now find the link for the edges on that OrgVDC
    Write-Host "Getting Edge Gateways that are a member of the OrgVDC..."
    $response = Invoke-RestMethod -Uri $orgVDCURL -Headers $ciHeaders -Method get
    $edgeGatewaysURL = ($response.Vdc.Link | Where-Object {$_.rel -match "EdgeGateways"}).href

    # ..now list all the edge Gateways
    $response = Invoke-RestMethod -Uri $edgeGatewaysURL -Headers $ciHeaders -Method get
    # Check if more than one Edge is being returned
    if (($response.QueryResultRecords.EdgeGatewayRecord).count -gt 1)
    {
        Write-Host "More than one ESG exists, each one will be configured to have a sub-allocation pool"
    }

    # Create a temporary header for putting Edge Config Documents onto vCD
    $tmpheader = $ciHeaders
    $tmpheader += @{"Content-Type" = "application/vnd.vmware.admin.edgeGateway+xml"}

    # Loop through the Edges and proceed to set the Sub-Allocation
    foreach ($esgUrl in $response.QueryResultRecords.EdgeGatewayRecord.href)
    {
        # Get the ESG configuration
        $response = Invoke-RestMethod -Uri $esgUrl -Headers $ciHeaders -Method get

        # Get the ESG Name in this loop, also used in the FW and NAT rule creation later
        $edgeName = $response.EdgeGateway.name

        # Call the Add-vCDEdgeSubAllocation function to do the work for us
        add-vCDEdgeSubAllocation -orgVDC $orgVDCname -edge $edgeName -server $ciserver -quiet

        # Get the ESG Uplink and External IP Address for SNAT work later
        $uplink = $response.EdgeGateway.Configuration.GatewayInterfaces.GatewayInterface | where-object {$_.InterfaceType -eq "Uplink"}
        $edgeGatewayExtIP = $uplink.SubnetParticipation.IpAddress

        # Get the ESG internal networks for the NAT/FW Work Later
        $internalInterfaces = ($response.EdgeGateway.Configuration.GatewayInterfaces.GatewayInterface | Where-Object {$_.InterfaceType -eq "internal"}).SubnetParticipation
        # Loop through any internal interfaces if they exist
        if (@($internalInterfaces).count -gt 0)
        {
            Write-Host "Detected Internal Interfaces, calculating CIDR blocks"
            # Start an array to hold the Network/CIDR blocks
            $internalCIDRs = @()
            foreach ($internalInterface in $internalInterfaces)
            {
                $subnetMask = $internalInterface.Netmask
                $ipAddress = $internalInterface.IpAddress
                # Use the IPCalc function to calculate the Network Address and CIDR Block
                $internalCIDRs += (ipcalc -IPAddress $ipAddress -Netmask $subnetMask).network
            }

            Write-Host "$(@($internalInterfaces).count)" -ForegroundColor Yellow -NoNewline
            Write-Host " Internal Interface(s) detected, setting SNAT rules for IP Masq"
            foreach ($internalCIDR in $internalCIDRs)
            {
                # Create rules to give SNAT functionality
                write-host "Adding Default Edge SNAT Rule for " -NoNewline
                Write-Host "$($internalCIDR)" -ForegroundColor yellow
                Add-vCDedgeNATrule -quiet -orgVDC $orgVDCname -edge $edgeName -enabled true -description "NAT Masq" -iface "External Pool" -type SNAT -translatedIP $edgeGatewayExtIP -originalIP $internalCIDR -server $ciserver
                # Doesn't yet check for success, future improvement needed
                done
            }

            # Create rule for FW access, outbound to KMS server
            write-host "Adding Default Edge FW Rule for KMS Access..."
            Add-vCDedgeFWrule -quiet -orgVDC $orgVDCname -edge $edgeName -enabled true -name "KMS Access" -action allow -proto tcp -destprt 1688 -dest 185.39.247.11 -src internal -server $ciserver
            # Doesn't yet check for success, future improvement needed
            done
        }
        else
        {
            # Set a flag to avoid trying to add NAT/FW Rules
            Write-Host "NO Detected Internal Interfaces, skipping SNAT/FW rule creation"
        }

        # Rename the Edge from the Default name to one that's unique to the customer
        $newEdgeName = "$($orgVDCname)-ESG1"

        # Call the Set-vCDEdgeName function to do the work for us in vCD
        Write-Host "Renaming the vCD Edge Object..."
        Set-vCDEdgeName -orgVDC $orgVDCname -edge $edgeName -name $newEdgeName -server $ciserver -quiet
        done

        # Call the Set-NSXEdgeName function to do the work for us in vSphere, requires connection to Power-NSX
        <#
        write-host "Connecting to NSX Manager..."
        connect-nsxserver -server xxxxxxxx
        $NSXedge = Get-NsxEdge -objectId edge-102
        $NSXedge.name = "vse-GB-TSI-SKJ-A1-ESG1 (628c84b7-840d-4779-9def-9dd9cab22f72)"
        $NSXedge | Set-NsxEdge
        done
        #>
    }

    # Fix the memory resource guarantee to 5% - templates have a bug of a MINIMUM of 20% guaranteed

    # Setting 5% Memory Reservation for vDC
    Write-Host "Setting 5% Memory reservation..."
    $orgVDC | Set-OrgVdc -MemoryGuaranteedPercent 5 -ErrorVariable problem | Out-Null
    if ($problem)
    {
        write-warning "Couldn't set Memory Reservation %-age"
        break
    }
    done

    # Create a new catalog for the OrgVDC
    Write-Host "Creating new default catalog for the OrgVDC"
    $adminCatalog = New-Object  VMware.VimAutomation.Cloud.Views.AdminCatalog
    $adminCatalog.name = "$($orgvdc.name)_Catalog1"
    $adminCatalog.IsPublished = $false
    (Get-Org $Org | Get-CIView).CreateCatalog($adminCatalog) | Out-Null
    done

    # Get all storage profiles assigned to this OrgVDC
    Write-Host "Getting Storage Profiles assigned to this OrgVDC..."
    $storProfiles = $orgVDC.ExtensionData.vdcstorageprofiles.vdcstorageprofile
    done

    # Check how many storage profiles are returned, if more than 1, prompt user to choose one for catalog storage
    if (@($storProfiles).count -gt 1)
    {
        # Prompt user to choose the profile they wish to use as the default
        Write-Host "Which profile do you want as default for Catalog items? - Usually the cheapest"
        show-arrayMenu $storProfiles.name
        if ($break)
        {
            break
        }
        # Get the profile object, rather than just the string as returned from the menu
        $defaultProfile = $storProfiles | where-object {$_.name -match $object}

        # Set the catalog to use the default storage profile from above
        Write-Host "Setting default catalog profile to $($object)"
        $catalog = get-catalog -name "$($orgvdc.name)_Catalog1"
        $profileRef = $defaultProfile.href
        $newSettings = $catalog.ExtensionData
        $newSettings.CatalogStorageProfiles.VdcStorageProfile = $profileref
        $newSettings.UpdateServerData() | Out-Null
        done
    }

    # Quickly grab the OrgID again, should this function not have been called from the Add-vCloudOrg function
    $orgID = ($org.Id.Split(':'))[-1]

    # Display end results
    add-vCloudOrgEndResult

    # Disconnect from REST
    disconnect-ciREST

    # Tidy up afterwards
    disconnect-ci
}

function new-vCloudOrgADOU()
{
    #########  PHCUSER Creation Script - Tim Simons / Dave Hocking 2017 #############
    # This script creates a new AD Organisational Unit for a PHC Organisation.
    # Check for existing OU and/or build one to spec

    Write-Host "Building AD OU for `"$($OrgName)`" under `"$($ADbaseOU)`""
    New-ADOrganizationalUnit -Path $ADbaseOU -name $OrgName -ErrorVariable $addADouErrorvar
    if ($addADouErrorvar)
    {
        Write-Warning "Issues occurred during AD OU Build, check for errors and/or manually correct"
    }
}

function Add-ExtraOrgAdminRights()
{
      <#
        .SYNOPSIS
            version 0.1 - Written by Dave Hocking 2017.
            This function will attempt to add a predefined list of Org rights to a given Organisation, using the vCloud API
            
            Requires PowerCLI Support and an existing connection to the vCloud Server (connect-ciserver)
            The script makes a connection to vCD via the RestAPI via the function "connect-ciRest" from this same module file, using the supplied $server
            
        .DESCRIPTION
            Takes a supplied value for the Org's name and assigns the following rights...
            "Organization vDC Distributed Firewall: Configure Rules"
            "Organization vDC Distributed Firewall: Enable/Disable"
            "Organization vDC Distributed Firewall: View Rules"
            "Organization vDC Gateway: Configure Services"
            "Organization vDC Gateway: Configure Syslog"
            "Organization vDC Gateway: Configure System Logging"
            "Organization vDC Gateway: Configure BGP Routing"
            "Organization vDC Gateway: Configure DHCP"
            "Organization vDC Gateway: Configure Firewall"
            "Organization vDC Gateway: Configure IPSec VPN"
            "Organization vDC Gateway: Configure L2 VPN"
            "Organization vDC Gateway: Configure Load Balancer"
            "Organization vDC Gateway: Configure NAT"
            "Organization vDC Gateway: Configure OSPF Routing"
            "Organization vDC Gateway: Configure Remote Access"
            "Organization vDC Gateway: Configure SSL VPN"
            "Organization vDC Gateway: Configure Static Routing"
            "Organization vDC Gateway: View BGP Routing"
            "Organization vDC Gateway: View DHCP"
            "Organization vDC Gateway: View Firewall"
            "Organization vDC Gateway: View IPSec VPN"
            "Organization vDC Gateway: View L2 VPN"
            "Organization vDC Gateway: View Load Balancer"
            "Organization vDC Gateway: View NAT"
            "Organization vDC Gateway: View OSPF Routing"
            "Organization vDC Gateway: View Remote Access"
            "Organization vDC Gateway: View SSL VPN"
            "Organization vDC Gateway: View Static Routing"
            
            Written and tested against vCD 8.20

            -------------------------------------------------------------------------------------------------------------------

            Collects the following information
            - Org Name (e.g. "GB-DJH") [ string ] MANDATORY
            - vCloud Server Name (e.g. "vcloud.isp.com") [ string ] MANDATORY

            -------------------------------------------------------------------------------------------------------------------

            RELATED COMMANDS
            Test-vCDorgRights

        .EXAMPLE
           Add-ExtraOrgAdminRights -org GB-DJH -server "vcloud.isp.com"
            - connects to the vCD server vcloud.isp.com and attempts to add a collection of rights (defined in the script's function) to the Org

    #>

    # Allow advanced parameter functions
    [CmdletBinding()]
    
    # Define the Parameters to pass the to function
    Param(

    [Parameter(Mandatory=$true)]
    [ValidateScript({get-org $_})]
    [String]$orgName,

    [Parameter(Mandatory=$true)]
    [string]$server,

    # Hidden option for output formatting
    [Parameter(DontShow)]
    [switch]$quiet
    )
    
    # Set the rights variable as an empty array, then populate with the desired right names
    $rights = @()
    $rights += "Organization vDC Distributed Firewall: Configure Rules"
    $rights += "Organization vDC Distributed Firewall: Enable/Disable"
    $rights += "Organization vDC Distributed Firewall: View Rules"
    $rights += "Organization vDC Gateway: Configure Services"
    $rights += "Organization vDC Gateway: Configure Syslog"
    $rights += "Organization vDC Gateway: Configure System Logging"
    $rights += "Organization vDC Gateway: Configure BGP Routing"
    $rights += "Organization vDC Gateway: Configure DHCP"
    $rights += "Organization vDC Gateway: Configure Firewall"
    $rights += "Organization vDC Gateway: Configure IPSec VPN"
    $rights += "Organization vDC Gateway: Configure L2 VPN"
    $rights += "Organization vDC Gateway: Configure Load Balancer"
    $rights += "Organization vDC Gateway: Configure NAT"
    $rights += "Organization vDC Gateway: Configure OSPF Routing"
    $rights += "Organization vDC Gateway: Configure Remote Access"
    $rights += "Organization vDC Gateway: Configure SSL VPN"
    $rights += "Organization vDC Gateway: Configure Static Routing"
    $rights += "Organization vDC Gateway: View BGP Routing"
    $rights += "Organization vDC Gateway: View DHCP"
    $rights += "Organization vDC Gateway: View Firewall"
    $rights += "Organization vDC Gateway: View IPSec VPN"
    $rights += "Organization vDC Gateway: View L2 VPN"
    $rights += "Organization vDC Gateway: View Load Balancer"
    $rights += "Organization vDC Gateway: View NAT"
    $rights += "Organization vDC Gateway: View OSPF Routing"
    $rights += "Organization vDC Gateway: View Remote Access"
    $rights += "Organization vDC Gateway: View SSL VPN"
    $rights += "Organization vDC Gateway: View Static Routing"

    if (!$PSBoundParameters.ContainsKey('quiet'))
    {
       # Do some output
        Write-Host "Adding the following list of rights to the Org " -NoNewline
        Write-Host "$($orgName)" -ForegroundColor Yellow
        Write-Host ""
        $rights
    }

    # Loop through the above rights and assign to them to a given Org
    foreach ($right in $rights)
    {
        Add-vCDorgRights -org $orgName -rightName $right -server $server -quiet
    }

    done
}
function Add-vCDorgRights()
{
    # Need to support the addition of multiple rights, CSV/XML import? 
    
    <#
        .SYNOPSIS
            version 0.1 - Written by Dave Hocking 2017.
            This function will attempt to add a specified vCD right to a candidate Org, using the vCloud API.
            
            Requires PowerCLI Support and an existing connection to the vCloud Server (connect-ciserver)
            The script makes a connection to vCD via the RestAPI via the function "connect-ciRest" from this same module file, using the supplied $server
            
        .DESCRIPTION
            Takes a supplied value for the right-reference and attempts to assign it to the Org's list of assigned rights
            
            Written and tested against vCD 8.20

            -------------------------------------------------------------------------------------------------------------------

            Collects the following information
            - Org Name (e.g. "GB-DJH") [ string ] MANDATORY
            - Right Ref / Right Name (e.g. "4886663f-ae31-37fc-9a70-3dbe2f24a8c5" / "Catalog: Add vApp from My Cloud") [ string ] MANDATORY
            - vCloud Server Name (e.g. "vcloud.isp.com") [ string ] MANDATORY

            ...checks for the presence of the right-ref in the Org's XML and if it's missing, it will assign it.

            Hidden parameters (switches) exist for -showXML and -showHeader to assist with any debugging that may be needed.
        
            -------------------------------------------------------------------------------------------------------------------

            RELATED COMMANDS
            Test-vCDorgRights

        .EXAMPLE
           Add-vCDorgRights -org GB-DJH -rightRef "4886663f-ae31-37fc-9a70-3dbe2f24a8c5" -server "vcloud.isp.com"
            - connects to the vCD server vcloud.isp.com and gathers the Rights assigned to the org "GB-DJH"
            - checks for an entry matching the reference, in this case it's "Catalog: Add vApp from My Cloud"
            - if the right is missing, it is added to the supplied org

            .EXAMPLE
           Add-vCDorgRights -org GB-DJH -rightName "Catalog: Add vApp from My Cloud" -server "vcloud.isp.com"
            - connects to the vCD server vcloud.isp.com and gathers the Rights assigned to the org "GB-DJH"
            - checks for an entry matching the reference, in this case it's "Catalog: Add vApp from My Cloud"
            - if the right is missing, it is added to the supplied org
    #>

    # Allow advanced parameter functions
    [CmdletBinding()]

    # Define the Parameters to pass the to function
    Param(

    [Parameter(Mandatory=$true)]
    [ValidateScript({get-org $_})]
    [String]$orgName,

    [Parameter(Mandatory=$false)]
    [String]$rightRef,
    
    [Parameter(Mandatory=$false)]
    [String]$rightName,

    [Parameter(Mandatory=$true)]
    [string]$server,

    # Hidden options for debugging
    [Parameter(DontShow)]
    [switch]$showXML,

    [Parameter(DontShow)]
    [switch]$quiet,

    [Parameter(DontShow)]
    [switch]$showHeader
    )

     # Check for the supply of both a rightRef or rightName var
     if ($rightRef -and $rightName)
     {
         Write-Warning "Supply only one of [rightRef | rightName], cannot continue with both supplied"
         break
     }
     
     # Check for the supply of either a rightRef or rightName var
     if (-not $rightRef -and -not $rightName)
     {
         Write-Warning "No right reference or right name supplied, cannot continue with none supplied"
         break
     }

    # Hidden option for Header output
    if ($PSBoundParameters.ContainsKey('showHeader'))
    {
        # Display the headers (from connect-ciREST)
        write-Host "`nDisplaying headers..."
        $ciHeaders
    }

    # Make a ciRest connection if no existing headers detected
    if (!$ciHeaders)
    {
        connect-ciREST -server $server
    }
    else
    {   
        if (!$PSBoundParameters.ContainsKey('quiet'))
        {
            Write-Host "`nUsing existing vCloud Rest connection..."
        }
    }

    if (!$PSBoundParameters.ContainsKey('quiet'))
    {
        Write-Host "Collecting data from vCloud Director..."
    }

    # If the user has supplied a rightName, convert this into a rightReference
    if ($rightName)
    {
        $rightRef = Get-vCDRightReference -rightName $rightName -server $server -quiet
        if($rightRef -eq $false)
        {
            write-warning "Couldn't find a right with the supplied Name."
            write-host "Make a vCloud API call to the following URL for a full list of Rights and Names..."
            write-host "https://$($server)/api/admin?fields=RightReferences"
            break
        }
        else
        {
            if (!$PSBoundParameters.ContainsKey('quiet'))
            {
                Write-Host "Converted" -NoNewline
                Write-Host " $($rightName) " -ForegroundColor Yellow -NoNewline
                Write-Host "into" -NoNewline
                Write-Host " $($rightRef) " -ForegroundColor Yellow
            }
        }
    }

    else
    {
        $rightName = Get-vCDRightName -rightRef $rightRef -server $server -quiet
        if($rightName -eq $false)
        {
            write-warning "Couldn't find a right with the supplied Reference."
            write-host "Make a vCloud API call to the following URL for a full list of Rights and Names..."
            write-host "https://$($server)/api/admin?fields=RightReferences"
            break
        }
        else
        {
            if (!$PSBoundParameters.ContainsKey('quiet'))
            {
                Write-Host "Converted" -NoNewline
                Write-Host " $($rightRef) " -ForegroundColor Yellow -NoNewline
                Write-Host "into" -NoNewline
                Write-Host " $($rightName) " -ForegroundColor Yellow
            }
        }
    }
    
    # Check if the right has already been assigned to the org
    $rightExists = Test-vCDorgRights -org $orgName -rightRef $rightRef -server $server -quiet
    if ($rightExists)
    {
        # Don't do any work as the requested right already exists
        Write-Host "$($RightName)" -NoNewline -ForegroundColor Yellow
        Write-Host " is already assigned to the Organisation"
    }
    else
    {
        # Process the Org's XML and add the rightRef to the list of assigned rights
        if (!$PSBoundParameters.ContainsKey('quiet'))
        {
            Write-Host "Adding right " -NoNewline
            Write-Host "$($rightRef)" -NoNewline -ForegroundColor Yellow
            Write-Host " to the Org " -NoNewline
            Write-Host "$($orgName)" -ForegroundColor Yellow
        }
        
        # Get the existing set of rights
            #https://{{vcdServer}}/api/admin/org/0712aa06-d241-4423-a6e7-80daeee8f71e/rights
        if (!$PSBoundParameters.ContainsKey('quiet'))
        {
            write-host "Getting the list of assigned rights for the Org..."
        }

        # The admin URL for the supplied object needs changing, bit dirty to use string work here, but hey. Who's looking?
        $orgAdminURL = (get-org $orgName).href
        $orgRightsUrl = "$($orgAdminURL)/rights"
        $response = Invoke-RestMethod -Method Get -Uri $orgRightsUrl -Headers $ciHeaders
        $rightsXML = $response.InnerXml

        # Build out the string you need to add to the XML
        $newRightsString = "<RightReference href=`"https://$($server)/api/admin/right/$($rightRef)`" name=`"$($rightName)`" type=`"application/vnd.vmware.admin.right+xml`" />"
        if (!$PSBoundParameters.ContainsKey('quiet'))
        {
            write-host "Proposed new XML : " -NoNewline
            write-host "$($newRightsString)" -ForegroundColor Yellow
        }

        # Add </OrgRights> to the new string
        $newRightsString += "</OrgRights>"

        # Do a string replacement on the </OrgRights> to include the new role you want to add
        if (!$PSBoundParameters.ContainsKey('quiet'))
        {
            Write-Host "Adding new XML to the existing set"
        }
        $newRightsXML = $rightsXML.Replace('</OrgRights>',$newRightsString)
        
        # Hidden option for XML output
        if ($PSBoundParameters.ContainsKey('showXML'))
        {
            # Display the collected config
            write-Host "`nDisplaying Proposed XML config..."
            $newRightsXML
        }

        # Create a temporary copy of the headers, including the content-type of "application/vnd.vmware.admin.org.rights+xml"
        if (!$PSBoundParameters.ContainsKey('quiet'))
        {
            Write-Host "Setting Content-Type for the API call"
        }
        $tmpheader = $ciHeaders
        $tmpheader += @{"Content-Type" = "application/vnd.vmware.admin.org.rights+xml"}
        
        # Use the new header to submit the request to the vCD server as a PUT
        if (!$PSBoundParameters.ContainsKey('quiet'))
        {
            Write-Host "Submitting changes to vCD..."
        }
        $response = Invoke-RestMethod -Method Put -Uri $orgRightsUrl -Headers $tmpheader -Body $newRightsXML
            
        # Trash the old temp copy of the headers
        $tmpheader = $null
        
        # Test for task success and handle the outcome
        if (!$PSBoundParameters.ContainsKey('quiet'))
        {
            Write-Host "Testing for presence of the new right..."
        }

        if (Test-vCDorgRights -org $orgName -rightRef $rightRef -server $server -quiet)
        {
            if (!$PSBoundParameters.ContainsKey('quiet'))
            {
                done
            }
        }
        
        else
        {
            Write-Warning "Couldn't find the right, something has broken in the process :("
            break    
        }      
    }
}

function Test-vCDorgRights()
{
    <#
        .SYNOPSIS
            version 0.1 - Written by Dave Hocking 2017.
            This function will test for the presence of a specified vCD right against a candidate Org, using the vCloud API.
            It can optionally export a list of assigned rights.

            Requires PowerCLI Support and an existing connection to the vCloud Server (connect-ciserver)
            The script makes a connection to vCD via the RestAPI via the function "connect-ciRest" from this same module file, using the supplied $server
            Optionally, you can specify -exportPath to save a CSV for further use - The Parent Directory must exist.

        .DESCRIPTION
            Gathers details for the rights assigned to an Org (not necessarily assigned to any roles)
            Takes a supplied value for the right-reference and attempts to locate it in the Org's list of assigned rights
            If the right is present, the function returns $true, if the right is missing, it returns $false

            Written and tested against vCD 8.20

            -------------------------------------------------------------------------------------------------------------------

            Collects the following information
            - Org Name (e.g. "GB-DJH") [ string ] MANDATORY
            - Right Name (e.g. "Catalog: Add vApp from My Cloud") [ string ]
            - Right Ref (e.g. "4886663f-ae31-37fc-9a70-3dbe2f24a8c5") [ string ]
            - vCloud Server Name (e.g. "vcloud.isp.com") [ string ] MANDATORY

            ...checks for the presence of the right-ref in the Org's XML and returns $true or $false
            ...if desired, the -exportpath switch can be used to export the XML defining the rights that are assigned.

            Hidden parameters (switches) exist for -showXML and -showHeader to assist with any debugging that may be needed.
        
            -------------------------------------------------------------------------------------------------------------------

            RELATED COMMANDS
            Add-vCDorgCustomRole

        .EXAMPLE
           Test-vCDorgRights -org GB-DJH -rightRef "4886663f-ae31-37fc-9a70-3dbe2f24a8c5" -server "vcloud.isp.com"
            - connects to the vCD server vcloud.isp.com and gathers the Rights assigned to the org "GB-DJH"
            - checks for an entry matching the reference, in this case it's "Catalog: Add vApp from My Cloud"
            - returns $true or $false as appropriate

        .EXAMPLE
           Test-vCDorgRights -org GB-DJH -rightName "Catalog: Add vApp from My Cloud" -server "vcloud.isp.com"
            - connects to the vCD server vcloud.isp.com and gathers the Rights assigned to the org "GB-DJH"
            - checks for an entry matching the reference, in this case it's "Catalog: Add vApp from My Cloud"
            - returns $true or $false as appropriate
    
        .EXAMPLE
           Test-vCDorgRights -org GB-DJH -rightRef "4886663f-ae31-37fc-9a70-3dbe2f24a8c5" -server "vcloud.isp.com" -exportPath c:\temp\export.xml
            - connects to the vCD server vcloud.isp.com and gathers the Rights assigned to the org "GB-DJH"
            - checks for an entry matching the reference, in this case it's "Catalog: Add vApp from My Cloud"
            - returns $true or $false as appropriate
            - saves the collected XML file as specified by the -exportPath switch (The C:\temp folder must exist!)

    #>

    # Allow advanced parameter functions
    [CmdletBinding()]

    # Define the Parameters to pass the to function
    Param(

    [Parameter(Mandatory=$true)]
    [ValidateScript({get-org $_})]
    [String]$orgName,

    [Parameter(Mandatory=$false)]
    [String]$rightRef,
    
    [Parameter(Mandatory=$false)]
    [String]$rightName,

    [Parameter(Mandatory=$true)]
    [string]$server,

    [Parameter(Mandatory=$false)]
    #[ValidateScript({test-path (Split-Path $exportPath)})]
    [string]$exportPath,

    # Hidden options for debugging
    [Parameter(DontShow)]
    [switch]$showXML,

    [Parameter(DontShow)]
    [switch]$quiet,

    [Parameter(DontShow)]
    [switch]$showHeader
    )

    # Check for the supply of both a rightRef or rightName var
    if ($rightRef -and $rightName)
    {
        Write-Warning "Supply only one of [rightRef | rightName], cannot continue with both supplied"
        break
    }
    
    # Check for the supply of either a rightRef or rightName var
    if (-not $rightRef -and -not $rightName)
    {
        Write-Warning "No right reference or right name supplied, cannot continue with none supplied"
        break
    }

    # Hidden option for Header output
    if ($PSBoundParameters.ContainsKey('showHeader'))
    {
        # Display the headers (from connect-ciREST)
        write-Host "`nDisplaying headers..."
        $ciHeaders
    }

    # Make a ciRest connection if no existing headers detected
    if (!$ciHeaders)
    {
        connect-ciREST -server $server
    }
    else
    {   
        if (!$PSBoundParameters.ContainsKey('quiet'))
        {
            Write-Host "`nUsing existing vCloud Rest connection..."
        }
    }

    if (!$PSBoundParameters.ContainsKey('quiet'))
    {
        Write-Host "Collecting data from vCloud Director..."
    }

    # The admin URL for the supplied object needs changing, bit dirty to use string work here, but hey. Who's looking?
    $orgAdminURL = (get-org $orgName).href
    $orgUrl = "$($orgAdminURL)/rights"

    # Collect the list of assigned rights for the org
    if (!$PSBoundParameters.ContainsKey('quiet'))
    {
        write-host "Collecting assigned Rights for Org : " -NoNewline
        Write-Host $orgName -ForegroundColor Yellow
    }
    $response = Invoke-RestMethod -Method Get -Uri $orgUrl -Headers $ciHeaders
    
    # Swap in the supplied reference for the right (or the name) and search for it
    if ($rightRef){$right = $rightRef}
    if ($rightName){$right = $rightName}

    if (!$PSBoundParameters.ContainsKey('quiet'))
    {
        Write-Host "Checking for presence of Right : " -NoNewline
        write-host $right -ForegroundColor Yellow
    }
    
    # Reset vars
    $containsRight = $null
    
    # Check the returned XML for the supplied right-reference or right-name
    $containsRight = $response.InnerXml.Contains("$($right)")
    

    # Return true/false
    if ($containsRight -eq $true)
    {
        if (!$PSBoundParameters.ContainsKey('quiet'))
        {
            Write-Host "Reference/Right Found"
        }
        return $true
    }
    
    if ($containsRight -eq $false)
    {
        if (!$PSBoundParameters.ContainsKey('quiet'))
        {
            Write-Host "Reference/Right NOT Found"
        }
        return $false
    }

    # Check for exportPath option for XML export
    if ($PSBoundParameters.ContainsKey('exportPath'))
    {
        # Output the data to file
        write-Host "Exporting Organisation's Assigned Rights to `"" -NoNewline
        write-host "$($exportPath)" -NoNewline -ForegroundColor Yellow
        write-host "`""
        write-host "Existing file will be overwritten, make sure path exists also."
        confirm-continue
        if ($break)
        {
            break
        }

        write-Host "Exporting..."
        $response.InnerXml | out-file $($exportPath)
        done
    }
}

function Remove-ExcessAdminRoleRights()
{
    # Need to support the addition of multiple rights, CSV/XML import? 
    
    <#
        .SYNOPSIS
            version 0.1 - Written by Tom Bainbridge 2017.
            This function will attempt to add a specified vCD right to a candidate Org Role, using the vCloud API.
            
            Requires PowerCLI Support and an existing connection to the vCloud Server (connect-ciserver)
            The script makes a connection to vCD via the RestAPI via the function "connect-ciRest" from this same module file, using the supplied $server
            
        .DESCRIPTION
            Takes a supplied value for the right-reference and attempts to assign it to the Org's list of assigned rights
            
            Written and tested against vCD 8.20

            -------------------------------------------------------------------------------------------------------------------

            Collects the following information
            - Org Name (e.g. "GB-DJH") [ string ] MANDATORY
            - Role Name (e.g. "Organization Administrator") [ string ] MANDATORY
            - Right Ref / Right Name (e.g. "4886663f-ae31-37fc-9a70-3dbe2f24a8c5" / "Catalog: Add vApp from My Cloud") [ string ] MANDATORY
            - vCloud Server Name (e.g. "vcloud.isp.com") [ string ] MANDATORY

            ...checks for the presence of the right-ref in the Org's Role XML and if it's missing, it will assign it.

            Hidden parameters (switches) exist for -showXML and -showHeader to assist with any debugging that may be needed.
        
            -------------------------------------------------------------------------------------------------------------------

            RELATED COMMANDS
            Test-vCDorgAdminRoleRight

        .EXAMPLE
           Add-vCDorgAdminRo

        .EXAMPLE
           Add-vCDorgAdmin
    #>

        # Allow advanced parameter functions
    [CmdletBinding()]

    # Define the Parameters to pass the to function
    Param(

    [Parameter(Mandatory=$true)]
    [ValidateScript({get-org $_})]
    [String]$orgName,

    [Parameter(Mandatory=$true)]
    [string]$server,

    [Parameter(DontShow)]
    [switch]$quiet
    )

    $roleName = "Organization Administrator"

    $global:Rights = @()
    $global:Rights += "Group / User: View"
    $global:Rights += "Organization: Edit Leases Policy"
    $global:Rights += "Organization: Edit Federation Settings"
    $global:Rights += "Organization: Edit Properties"
    $global:Rights += "Organization: Edit Quotas Policy"
    $global:Rights += "Organization: Edit Password Policy"
    $global:Rights += "Organization: Edit SMTP Settings"

    #$org = get-org -name $orgName -ea ignore
    #if (!$org)
    #{
    #    write-warning "NO ORG COULD BE COLLECTED WITH THAT NAME"
    #    break
    #}

    if (!$PSBoundParameters.ContainsKey('quiet'))
    {
        Write-Host "Removing rights from role " -NoNewline
        Write-Host $roleName -ForegroundColor Yellow -NoNewline
        Write-Host " under organisation " -NoNewline
        Write-Host $orgName -ForegroundColor Yellow
    }

    foreach($rightName in $global:Rights ){

        if (!$PSBoundParameters.ContainsKey('quiet'))
        {
            $message = "{0,-40}" -f $rightName
            Write-Host $message -NoNewline
        }

        if(Test-vCDorgAdminRoleRight -org $orgName -roleName $roleName -rightName $rightName -server $server -quiet){

            Remove-vCDorgAdminRoleRight -org $orgName -roleName $roleName -rightName $rightName -server $server -quiet

            if (!$PSBoundParameters.ContainsKey('quiet'))
            {
                Write-Host "Removed" -ForegroundColor Green
            }

        } else {
            if (!$PSBoundParameters.ContainsKey('quiet'))
            {
                Write-Host "Role currently does not have this right" -ForegroundColor Yellow
            }
        }
    }

}

function Add-vCDorgAdminRoleRight()
{
    # Need to support the addition of multiple rights, CSV/XML import? 
    
    <#
        .SYNOPSIS
            version 0.1 - Written by Tom Bainbridge 2017.
            This function will attempt to add a specified vCD right to a candidate Org Role, using the vCloud API.
            
            Requires PowerCLI Support and an existing connection to the vCloud Server (connect-ciserver)
            The script makes a connection to vCD via the RestAPI via the function "connect-ciRest" from this same module file, using the supplied $server
            
        .DESCRIPTION
            Takes a supplied value for the right-reference and attempts to assign it to the Org's list of assigned rights
            
            Written and tested against vCD 8.20

            -------------------------------------------------------------------------------------------------------------------

            Collects the following information
            - Org Name (e.g. "GB-DJH") [ string ] MANDATORY
            - Role Name (e.g. "Organization Administrator") [ string ] MANDATORY
            - Right Ref / Right Name (e.g. "4886663f-ae31-37fc-9a70-3dbe2f24a8c5" / "Catalog: Add vApp from My Cloud") [ string ] MANDATORY
            - vCloud Server Name (e.g. "vcloud.isp.com") [ string ] MANDATORY

            ...checks for the presence of the right-ref in the Org's Role XML and if it's missing, it will assign it.

            Hidden parameters (switches) exist for -showXML and -showHeader to assist with any debugging that may be needed.
        
            -------------------------------------------------------------------------------------------------------------------

            RELATED COMMANDS
            Test-vCDorgAdminRoleRight

        .EXAMPLE
           Add-vCDorgAdminRoleRight -org GB-DJH -roleName "Organization Administrator" -rightRef "4886663f-ae31-37fc-9a70-3dbe2f24a8c5" -server "vcloud.isp.com"
            - connects to the vCD server vcloud.isp.com
            - gathers the Rights assigned to role "Organization Administrator" of the org "GB-DJH"
            - checks for an entry matching the reference, in this case it's "Catalog: Add vApp from My Cloud"
            - if the right is missing, it is added to the supplied org

        .EXAMPLE
           Add-vCDorgAdminRoleRight -org GB-DJH -roleName "Organization Administrator" -rightName "Catalog: Add vApp from My Cloud" -server "vcloud.isp.com"
            - connects to the vCD server vcloud.isp.com
            - gathers the Rights assigned to role "Organization Administrator" of the org "GB-DJH"
            - checks for an entry matching the reference, in this case it's "Catalog: Add vApp from My Cloud"
            - if the right is missing, it is added to the supplied org
    #>

    # Allow advanced parameter functions
    [CmdletBinding()]

    # Define the Parameters to pass the to function
    Param(

    [Parameter(Mandatory=$true)]
    [ValidateScript({get-org $_})]
    [String]$orgName,

    [Parameter(Mandatory=$true)]
    [String]$roleName,

    [Parameter(Mandatory=$false)]
    [String]$rightRef,
    
    [Parameter(Mandatory=$false)]
    [String]$rightName,

    [Parameter(Mandatory=$true)]
    [string]$server,

    # Hidden options for debugging
    [Parameter(DontShow)]
    [switch]$showXML,

    [Parameter(DontShow)]
    [switch]$quiet,

    [Parameter(DontShow)]
    [switch]$showHeader
    )

     # Check for the supply of both a rightRef or rightName var
     if ($rightRef -and $rightName)
     {
         Write-Warning "Supply only one of [rightRef | rightName], cannot continue with both supplied"
         break
     }
     
     # Check for the supply of either a rightRef or rightName var
     if (-not $rightRef -and -not $rightName)
     {
         Write-Warning "No right reference or right name supplied, cannot continue with none supplied"
         break
     }

    # Hidden option for Header output
    if ($PSBoundParameters.ContainsKey('showHeader'))
    {
        # Display the headers (from connect-ciREST)
        write-Host "`nDisplaying headers..."
        $ciHeaders
    }

    # Make a ciRest connection if no existing headers detected
    if (!$ciHeaders)
    {
        connect-ciREST -server $server
    }
    else
    {   
        if (!$PSBoundParameters.ContainsKey('quiet'))
        {
            Write-Host "`nUsing existing vCloud Rest connection..."
        }
    }

    if (!$PSBoundParameters.ContainsKey('quiet'))
    {
        Write-Host "Collecting data from vCloud Director..."
    }

    # If the user has supplied a rightName, convert this into a rightReference
    if ($rightName)
    {
        $rightRef = Get-vCDRightReference -rightName $rightName -server $server -quiet
        if($rightRef -eq $false)
        {
            write-warning "Couldn't find a right with the supplied Name."
            write-host "Make a vCloud API call to the following URL for a full list of Rights and Names..."
            write-host "https://$($server)/api/admin?fields=RightReferences"
            break
        }
        else
        {
            if (!$PSBoundParameters.ContainsKey('quiet'))
            {
                Write-Host "Converted" -NoNewline
                Write-Host " $($rightName) " -ForegroundColor Yellow -NoNewline
                Write-Host "into" -NoNewline
                Write-Host " $($rightRef) " -ForegroundColor Yellow
            }
        }
    }

    else
    {
        $rightName = Get-vCDRightName -rightRef $rightRef -server $server -quiet
        if($rightName -eq $false)
        {
            write-warning "Couldn't find a right with the supplied Reference."
            write-host "Make a vCloud API call to the following URL for a full list of Rights and Names..."
            write-host "https://$($server)/api/admin?fields=RightReferences"
            break
        }
        else
        {
            if (!$PSBoundParameters.ContainsKey('quiet'))
            {
                Write-Host "Converted" -NoNewline
                Write-Host " $($rightRef) " -ForegroundColor Yellow -NoNewline
                Write-Host "into" -NoNewline
                Write-Host " $($rightName) " -ForegroundColor Yellow
            }
        }
    }
    
    # Check if the right has already been assigned to the org
    $rightExists = Test-vCDorgAdminRoleRight -org $orgName -roleName $roleName -rightRef $rightRef -server $server -quiet
    if ($rightExists)
    {
        # Don't do any work as the requested right already exists
        Write-Host "$($RightName)" -NoNewline -ForegroundColor Yellow
        Write-Host " is already assigned to the $($roleName) Role"
    }
    else
    {
        # Process the Org's XML and add the rightRef to the list of assigned rights
        if (!$PSBoundParameters.ContainsKey('quiet'))
        {
            Write-Host "Adding right " -NoNewline
            Write-Host "$($rightRef)" -NoNewline -ForegroundColor Yellow
            Write-Host " to " -NoNewline
            Write-Host "$($roleName)"
            Write-Host " for "
            Write-Host "$($orgName)" -ForegroundColor Yellow
        }
        
        # Get the existing set of rights
            #https://{{vcdServer}}/api/admin/org/0712aa06-d241-4423-a6e7-80daeee8f71e/rights
        if (!$PSBoundParameters.ContainsKey('quiet'))
        {
            write-host "Getting the list of assigned rights for the Org Admin Role..."
        }

        # The admin URL for the supplied object needs changing, bit dirty to use string work here, but hey. Who's looking?
        $orgUrl = (get-org $orgName).href

        $response = Invoke-RestMethod -Method Get -Uri $orgUrl -Headers $ciHeaders
        $orgAdminRoleRightsUrl = $($([xml]$response).AdminOrg.RoleReferences.rolereference | where{$_.name -eq $roleName}).href
        
        Start-Sleep -milliseconds 500
        
        $response = Invoke-RestMethod -Method Get -Uri $orgAdminRoleRightsUrl -Headers $ciHeaders
        $rightsXML = $response.InnerXml

        # Build out the string you need to add to the XML
        $newRightsString = "<RightReference href=`"https://$($server)/api/admin/right/$($rightRef)`" name=`"$($rightName)`" type=`"application/vnd.vmware.admin.right+xml`" />"
        if (!$PSBoundParameters.ContainsKey('quiet'))
        {
            write-host "Proposed new XML : " -NoNewline
            write-host "$($newRightsString)" -ForegroundColor Yellow
        }

        # Add </RightReferences> to the new string
        $newRightsString += "</RightReferences>"

        # Do a string replacement on the </RightReferences> to include the new role you want to add
        if (!$PSBoundParameters.ContainsKey('quiet'))
        {
            Write-Host "Adding new XML to the existing set"
        }
        $newRightsXML = $rightsXML.Replace('</RightReferences>',$newRightsString)
        
        # Hidden option for XML output
        if ($PSBoundParameters.ContainsKey('showXML'))
        {
            # Display the collected config
            write-Host "`nDisplaying Proposed XML config..."
            $newRightsXML
        }

        # Create a temporary copy of the headers, including the content-type of "application/vnd.vmware.admin.org.rights+xml"
        if (!$PSBoundParameters.ContainsKey('quiet'))
        {
            Write-Host "Setting Content-Type for the API call"
        }
        $tmpheader = $ciHeaders
        $tmpheader += @{"Content-Type" = "application/vnd.vmware.admin.role+xml"}
        
        # Use the new header to submit the request to the vCD server as a PUT
        if (!$PSBoundParameters.ContainsKey('quiet'))
        {
            Write-Host "Submitting changes to vCD..."
        }
        $response = Invoke-RestMethod -Method Put -Uri $orgAdminRoleRightsUrl -Headers $tmpheader -Body $newRightsXML
            
        # Trash the old temp copy of the headers
        $tmpheader = $null
        
        Start-Sleep -milliseconds 500
        # Test for task success and handle the outcome
        if (!$PSBoundParameters.ContainsKey('quiet'))
        {
            Write-Host "Testing for presence of the new right..."
        }

        if (Test-vCDorgAdminRoleRight -org $orgName -roleName $roleName -rightRef $rightRef -server $server -quiet)
        {
            if (!$PSBoundParameters.ContainsKey('quiet'))
            {
                done
            }
        }
        
        else
        {
            Write-Warning "Couldn't find the right, something has broken in the process :("
            break
        }
    }
}

function Remove-vCDorgAdminRoleRight()
{
    # Need to support the addition of multiple rights, CSV/XML import? 
    
    <#
        .SYNOPSIS
            version 0.1 - Written by Tom Bainbridge 2017.
            This function will attempt to remove a specified vCD right to a candidate Org Role, using the vCloud API.
            
            Requires PowerCLI Support and an existing connection to the vCloud Server (connect-ciserver)
            The script makes a connection to vCD via the RestAPI via the function "connect-ciRest" from this same module file, using the supplied $server
            
        .DESCRIPTION
            Takes a supplied value for the right-reference and attempts to assign it to the Org's list of assigned rights
            
            Written and tested against vCD 8.20

            -------------------------------------------------------------------------------------------------------------------

            Collects the following information
            - Org Name (e.g. "GB-DJH") [ string ] MANDATORY
            - Role Name (e.g. "Organization Administrator") [ string ] MANDATORY
            - Right Ref / Right Name (e.g. "4886663f-ae31-37fc-9a70-3dbe2f24a8c5" / "Catalog: Add vApp from My Cloud") [ string ] MANDATORY
            - vCloud Server Name (e.g. "vcloud.isp.com") [ string ] MANDATORY

            ...checks for the presence of the right-ref in the Org's Role XML and if it's missing, it will assign it.

            Hidden parameters (switches) exist for -showXML and -showHeader to assist with any debugging that may be needed.
        
            -------------------------------------------------------------------------------------------------------------------

            RELATED COMMANDS
            Test-vCDorgAdminRoleRight

        .EXAMPLE
           Remove-vCDorgAdminRoleRight -org GB-DJH -roleName "Organization Administrator" -rightRef "4886663f-ae31-37fc-9a70-3dbe2f24a8c5" -server "vcloud.isp.com"
            - connects to the vCD server vcloud.isp.com
            - gathers the Rights assigned to role "Organization Administrator" of the org "GB-DJH"
            - checks for an entry matching the reference, in this case it's "Catalog: Add vApp from My Cloud"
            - if the right is missing, it is added to the supplied org

        .EXAMPLE
           Add-vCDorgAdminRoleRight -org GB-DJH -roleName "Organization Administrator" -rightName "Catalog: Add vApp from My Cloud" -server "vcloud.isp.com"
            - connects to the vCD server vcloud.isp.com
            - gathers the Rights assigned to role "Organization Administrator" of the org "GB-DJH"
            - checks for an entry matching the reference, in this case it's "Catalog: Add vApp from My Cloud"
            - if the right is missing, it is added to the supplied org
    #>

    # Allow advanced parameter functions
    [CmdletBinding()]

    # Define the Parameters to pass the to function
    Param(

    [Parameter(Mandatory=$true)]
    [ValidateScript({get-org $_})]
    [String]$orgName,

    [Parameter(Mandatory=$true)]
    [String]$roleName,

    [Parameter(Mandatory=$false)]
    [String]$rightRef,
    
    [Parameter(Mandatory=$false)]
    [String]$rightName,

    [Parameter(Mandatory=$true)]
    [string]$server,

    # Hidden options for debugging
    [Parameter(DontShow)]
    [switch]$showXML,

    [Parameter(DontShow)]
    [switch]$quiet,

    [Parameter(DontShow)]
    [switch]$showHeader
    )

     # Check for the supply of both a rightRef or rightName var
     if ($rightRef -and $rightName)
     {
         Write-Warning "Supply only one of [rightRef | rightName], cannot continue with both supplied"
         break
     }
     
     # Check for the supply of either a rightRef or rightName var
     if (-not $rightRef -and -not $rightName)
     {
         Write-Warning "No right reference or right name supplied, cannot continue with none supplied"
         break
     }

    # Hidden option for Header output
    if ($PSBoundParameters.ContainsKey('showHeader'))
    {
        # Display the headers (from connect-ciREST)
        write-Host "`nDisplaying headers..."
        $ciHeaders
    }

    # Make a ciRest connection if no existing headers detected
    if (!$ciHeaders)
    {
        connect-ciREST -server $server
    }
    else
    {   
        if (!$PSBoundParameters.ContainsKey('quiet'))
        {
            Write-Host "`nUsing existing vCloud Rest connection..."
        }
    }

    if (!$PSBoundParameters.ContainsKey('quiet'))
    {
        Write-Host "Collecting data from vCloud Director..."
    }

    # If the user has supplied a rightName, convert this into a rightReference
    if ($rightName)
    {
        $rightRef = Get-vCDRightReference -rightName $rightName -server $server -quiet
        if($rightRef -eq $false)
        {
            write-warning "Couldn't find a right with the supplied Name."
            write-host "Make a vCloud API call to the following URL for a full list of Rights and Names..."
            write-host "https://$($server)/api/admin?fields=RightReferences"
            break
        }
        else
        {
            if (!$PSBoundParameters.ContainsKey('quiet'))
            {
                Write-Host "Converted" -NoNewline
                Write-Host " $($rightName) " -ForegroundColor Yellow -NoNewline
                Write-Host "into" -NoNewline
                Write-Host " $($rightRef) " -ForegroundColor Yellow
            }
        }
    }

    else
    {
        $rightName = Get-vCDRightName -rightRef $rightRef -server $server -quiet
        if($rightName -eq $false)
        {
            write-warning "Couldn't find a right with the supplied Reference."
            write-host "Make a vCloud API call to the following URL for a full list of Rights and Names..."
            write-host "https://$($server)/api/admin?fields=RightReferences"
            break
        }
        else
        {
            if (!$PSBoundParameters.ContainsKey('quiet'))
            {
                Write-Host "Converted" -NoNewline
                Write-Host " $($rightRef) " -ForegroundColor Yellow -NoNewline
                Write-Host "into" -NoNewline
                Write-Host " $($rightName) " -ForegroundColor Yellow
            }
        }
    }
    
    # Check if the right has already been assigned to the org
    $rightExists = Test-vCDorgAdminRoleRight -org $orgName -roleName $roleName -rightRef $rightRef -server $server -quiet
    if (!$rightExists)
    {
        # Don't do any work as the requested right already exists
        Write-Host "$($RightName)" -NoNewline -ForegroundColor Yellow
        Write-Host " is already un-assigned to the $($roleName) Role"
    }
    else
    {
        # Process the Org's XML and add the rightRef to the list of assigned rights
        if (!$PSBoundParameters.ContainsKey('quiet'))
        {
            Write-Host "Removing right " -NoNewline
            Write-Host "$($rightRef)" -NoNewline -ForegroundColor Yellow
            Write-Host " to " -NoNewline
            Write-Host "$($roleName)"
            Write-Host " for "
            Write-Host "$($orgName)" -ForegroundColor Yellow
        }
        
        # Get the existing set of rights
            #https://{{vcdServer}}/api/admin/org/0712aa06-d241-4423-a6e7-80daeee8f71e/rights
        if (!$PSBoundParameters.ContainsKey('quiet'))
        {
            write-host "Getting the list of assigned rights for the Org Admin Role..."
        }

        # The admin URL for the supplied object needs changing, bit dirty to use string work here, but hey. Who's looking?
        $orgUrl = (get-org $orgName).href

        $response = Invoke-RestMethod -Method Get -Uri $orgUrl -Headers $ciHeaders
        $orgAdminRoleRightsUrl = $($([xml]$response).AdminOrg.RoleReferences.rolereference | where{$_.name -eq $roleName}).href
        
        Start-Sleep -milliseconds 500
        
        $response = Invoke-RestMethod -Method Get -Uri $orgAdminRoleRightsUrl -Headers $ciHeaders
        $rightsXML = $response.InnerXml

        # Build out the string you need to add to the XML
        $newRightsString = "<RightReference href=`"https://$($server)/api/admin/right/$($rightRef)`" name=`"$($rightName)`" type=`"application/vnd.vmware.admin.right+xml`" />"
        if (!$PSBoundParameters.ContainsKey('quiet'))
        {
            write-host "Proposed XML : " -NoNewline
            write-host "$($newRightsString)" -ForegroundColor Yellow
        }

        # Do a string replacement on the </RightReferences> to include the new role you want to add
        if (!$PSBoundParameters.ContainsKey('quiet'))
        {
            Write-Host "Removing XML from the existing set"
        }
        $newRightsXML = $rightsXML.Replace($newRightsString,'')
        
        # Hidden option for XML output
        if ($PSBoundParameters.ContainsKey('showXML'))
        {
            # Display the collected config
            write-Host "`nDisplaying Proposed XML config..."
            $newRightsXML
        }

        # Create a temporary copy of the headers, including the content-type of "application/vnd.vmware.admin.org.rights+xml"
        if (!$PSBoundParameters.ContainsKey('quiet'))
        {
            Write-Host "Setting Content-Type for the API call"
        }
        $tmpheader = $ciHeaders
        $tmpheader += @{"Content-Type" = "application/vnd.vmware.admin.role+xml"}
        
        # Use the new header to submit the request to the vCD server as a PUT
        if (!$PSBoundParameters.ContainsKey('quiet'))
        {
            Write-Host "Submitting changes to vCD..."
        }
        $response = Invoke-RestMethod -Method Put -Uri $orgAdminRoleRightsUrl -Headers $tmpheader -Body $newRightsXML
            
        # Trash the old temp copy of the headers
        $tmpheader = $null
        
        Start-Sleep -milliseconds 500
        # Test for task success and handle the outcome
        if (!$PSBoundParameters.ContainsKey('quiet'))
        {
            Write-Host "Testing for presence of the new right..."
        }

        if (!$(Test-vCDorgAdminRoleRight -org $orgName -roleName $roleName -rightRef $rightRef -server $server -quiet))
        {
            if (!$PSBoundParameters.ContainsKey('quiet'))
            {
                done
            }
        }
        
        else
        {
            Write-Warning "The right is still assigned, something has broken in the process :("
            break    
        }      
    }
}

function Test-vCDorgAdminRoleRight()
{
    <#
        .SYNOPSIS
            version 0.1 - Written by Dave Hocking 2017.
            This function will test for the presence of a specified vCD right against a candidate Org, using the vCloud API.
            It can optionally export a list of assigned rights.

            Requires PowerCLI Support and an existing connection to the vCloud Server (connect-ciserver)
            The script makes a connection to vCD via the RestAPI via the function "connect-ciRest" from this same module file, using the supplied $server
            Optionally, you can specify -exportPath to save a CSV for further use - The Parent Directory must exist.

        .DESCRIPTION
            Gathers details for the rights assigned to an Org (not necessarily assigned to any roles)
            Takes a supplied value for the right-reference and attempts to locate it in the Org's list of assigned rights
            If the right is present, the function returns $true, if the right is missing, it returns $false

            Written and tested against vCD 8.20

            -------------------------------------------------------------------------------------------------------------------

            Collects the following information
            - Org Name (e.g. "GB-DJH") [ string ] MANDATORY
            - Role Name (e.g. "Organization Administrator") [ string ] MANDATORY
            - Right Name (e.g. "Catalog: Add vApp from My Cloud") [ string ]
            - Right Ref (e.g. "4886663f-ae31-37fc-9a70-3dbe2f24a8c5") [ string ]
            - vCloud Server Name (e.g. "vcloud.isp.com") [ string ] MANDATORY

            ...checks for the presence of the right-ref in the Org's XML and returns $true or $false
            ...if desired, the -exportpath switch can be used to export the XML defining the rights that are assigned.

            Hidden parameters (switches) exist for -showXML and -showHeader to assist with any debugging that may be needed.
        
            -------------------------------------------------------------------------------------------------------------------

            RELATED COMMANDS
            Add-vCDorgCustomRole

        .EXAMPLE
           Test-vCDorgRights -org GB-DJH -roleName "Organization Administrator" -rightRef "4886663f-ae31-37fc-9a70-3dbe2f24a8c5" -server "vcloud.isp.com"
            - connects to the vCD server vcloud.isp.com
            - gathers the Rights assigned to role "Organization Administrator" of the org "GB-DJH"
            - checks for an entry matching the reference, in this case it's "Catalog: Add vApp from My Cloud"
            - returns $true or $false as appropriate

        .EXAMPLE
           Test-vCDorgRights -org GB-DJH -roleName "Organization Administrator" -rightName "Catalog: Add vApp from My Cloud" -server "vcloud.isp.com"
            - connects to the vCD server vcloud.isp.com
            - gathers the Rights assigned to role "Organization Administrator" of the org "GB-DJH"
            - checks for an entry matching the reference, in this case it's "Catalog: Add vApp from My Cloud"
            - returns $true or $false as appropriate
    
        .EXAMPLE
           Test-vCDorgRights -org GB-DJH -roleName "Organization Administrator" -rightRef "4886663f-ae31-37fc-9a70-3dbe2f24a8c5" -server "vcloud.isp.com" -exportPath c:\temp\export.xml
            - connects to the vCD server vcloud.isp.com
            - gathers the Rights assigned to role "Organization Administrator" of the org "GB-DJH"
            - checks for an entry matching the reference, in this case it's "Catalog: Add vApp from My Cloud"
            - returns $true or $false as appropriate
            - saves the collected XML file as specified by the -exportPath switch (The C:\temp folder must exist!)
    #>

    # Allow advanced parameter functions
    [CmdletBinding()]

    # Define the Parameters to pass the to function
    Param(

    [Parameter(Mandatory=$true)]
    [ValidateScript({get-org $_})]
    [String]$orgName,

    [Parameter(Mandatory=$true)]
    [String]$roleName,

    [Parameter(Mandatory=$false)]
    [String]$rightRef,
    
    [Parameter(Mandatory=$false)]
    [String]$rightName,

    [Parameter(Mandatory=$true)]
    [string]$server,

    [Parameter(Mandatory=$false)]
    #[ValidateScript({test-path (Split-Path $exportPath)})]
    [string]$exportPath,

    # Hidden options for debugging
    [Parameter(DontShow)]
    [switch]$showXML,

    [Parameter(DontShow)]
    [switch]$quiet,

    [Parameter(DontShow)]
    [switch]$showHeader
    )

    # Check for the supply of both a rightRef or rightName var
    if ($rightRef -and $rightName)
    {
        Write-Warning "Supply only one of [rightRef | rightName], cannot continue with both supplied"
        break
    }
    
    # Check for the supply of either a rightRef or rightName var
    if (-not $rightRef -and -not $rightName)
    {
        Write-Warning "No right reference or right name supplied, cannot continue with none supplied"
        break
    }

    # Hidden option for Header output
    if ($PSBoundParameters.ContainsKey('showHeader'))
    {
        # Display the headers (from connect-ciREST)
        write-Host "`nDisplaying headers..."
        $ciHeaders
    }


    # Make a ciRest connection if no existing headers detected
    if (!$ciHeaders)
    {
        connect-ciREST -server $server
    }
    else
    {   
        if (!$PSBoundParameters.ContainsKey('quiet'))
        {
            Write-Host "`nUsing existing vCloud Rest connection..."
        }
    }

    if (!$PSBoundParameters.ContainsKey('quiet'))
    {
        Write-Host "Collecting data from vCloud Director..."
    }

    # The org URL
    $orgUrl = (get-org $orgName).href

    # Collect the Org properties so we can get the roles
    if (!$PSBoundParameters.ContainsKey('quiet'))
    {
        write-host "Collecting Roles for Org : " -NoNewline
        Write-Host $orgName -ForegroundColor Yellow
    }
    $response = Invoke-RestMethod -Method Get -Uri $orgUrl -Headers $ciHeaders
    $orgAdminRoleRightsUrl = $($([xml]$response).AdminOrg.RoleReferences.rolereference | where{$_.name -eq $roleName}).href
    $response2 = Invoke-RestMethod -Method Get -Uri $orgAdminRoleRightsUrl -Headers $ciHeaders
    
    # Swap in the supplied reference for the right (or the name) and search for it
    if ($rightRef){$right = $rightRef}
    if ($rightName){$right = $rightName}

    if (!$PSBoundParameters.ContainsKey('quiet'))
    {
        Write-Host "Checking for presence of Right : " -NoNewline
        write-host $right -ForegroundColor Yellow
    }
    
    # Reset vars
    $containsRight = $null
    
    # Check the returned XML for the supplied right-reference or right-name
    $containsRight = $response2.InnerXml.Contains("$($right)")
    

    # Return true/false
    if ($containsRight -eq $true)
    {
        if (!$PSBoundParameters.ContainsKey('quiet'))
        {
            Write-Host "Reference/Right Found" -ForegroundColor Green
        }
        return $true
    }
    
    if ($containsRight -eq $false)
    {
        if (!$PSBoundParameters.ContainsKey('quiet'))
        {
            Write-Host "Reference/Right NOT Found" -ForegroundColor Red
        }
        return $false
    }

    # Check for exportPath option for XML export
    if ($PSBoundParameters.ContainsKey('exportPath'))
    {
        # Output the data to file
        write-Host "Exporting Organisation's Assigned Rights to `"" -NoNewline
        write-host "$($exportPath)" -NoNewline -ForegroundColor Yellow
        write-host "`""
        write-host "Existing file will be overwritten, make sure path exists also."
        confirm-continue
        if ($break)
        {
            break
        }

        write-Host "Exporting..."
        $response.InnerXml | out-file $($exportPath)
        done
    }
}

function Get-vCDRightName()
{
    <#
        .SYNOPSIS
            version 0.1 - Written by Dave Hocking 2017.
            This function will get the vCloud Director Right Name for a given Right Reference, using the vCloud API.
           
            Requires PowerCLI Support and an existing connection to the vCloud Server (connect-ciserver)
            The script makes a connection to vCD via the RestAPI via the function "connect-ciRest" from this same module file, using the supplied $server
            
        .DESCRIPTION
            Returns the right name from a given right reference, out of all available rights in vCloud Director
    
            Written and tested against vCD 8.20

            -------------------------------------------------------------------------------------------------------------------

            Collects the following information
            - Right Reference (e.g. "4886663f-ae31-37fc-9a70-3dbe2f24a8c5") [ string ] MANDATORY
            - vCloud Server Name (e.g. "vcloud.isp.com") [ string ] MANDATORY

            Hidden parameters (switches) exist for -showXML and -showHeader to assist with any debugging that may be needed.
        
            -------------------------------------------------------------------------------------------------------------------

            RELATED COMMANDS
            Add-vCDorgCustomRole
            Test-vCDorgRights

        .EXAMPLE
           Get-vCDRightName -rightRef "4886663f-ae31-37fc-9a70-3dbe2f24a8c5" -server "vcloud.isp.com"
            - connects to the vCD server vcloud.isp.com and gathers the Rights available
            - returns the name for the right reference, in this case "Catalog: Add vApp from My Cloud"
    #>

    # Allow advanced parameter functions
    [CmdletBinding()]

    # Define the Parameters to pass the to function
    Param(
  
    [Parameter(Mandatory=$true)]
    [String]$rightRef,

    [Parameter(Mandatory=$true)]
    [string]$server,

    # Hidden options for debugging
    [Parameter(DontShow)]
    [switch]$showXML,

    [Parameter(DontShow)]
    [switch]$quiet,

    [Parameter(DontShow)]
    [switch]$showHeader
    )

    # Hidden option for Header output
    if ($PSBoundParameters.ContainsKey('showHeader'))
    {
        # Display the headers (from connect-ciREST)
        write-Host "`nDisplaying headers..."
        $ciHeaders
    }

    # Make a ciRest connection if no existing headers detected
    if (!$ciHeaders)
    {
        connect-ciREST -server $server
    }
    else
    {   
        if (!$PSBoundParameters.ContainsKey('quiet'))
        {
            Write-Host "`nUsing existing vCloud Rest connection..."
        }
    }

    if (!$PSBoundParameters.ContainsKey('quiet'))
    {
        Write-Host "Collecting data from vCloud Director..."
    }

    # Reset the rightName variable
    $rightName = $null

    # Set the URL for the list of available rights from the vCD Server supplied
    $rightsUrl = "https://$($server)/api/admin?fields=RightReferences"

    # Collect the list of assigned rights for the org
    if (!$PSBoundParameters.ContainsKey('quiet'))
    {
        write-host "Collecting available Rights from vCD Server : " -NoNewline
        Write-Host $server -ForegroundColor Yellow
    }
    $response = Invoke-RestMethod -Method Get -Uri $rightsUrl -Headers $ciHeaders
    
    # Test for the presence of the reference you're looking for
    if ((($response.VCloud.RightReferences.RightReference.href)) | where {$_ -match $rightRef})
    {
        # Get the rightName for the given rightRef
        $rightName = ((($response.VCloud.RightReferences.RightReference)) | where {$_.href -match $rightRef}).name
        if (!$PSBoundParameters.ContainsKey('quiet'))
        {
            Write-Host "Reference/Right Found"
        }
        return $rightName
    }
    else
    {
        if (!$PSBoundParameters.ContainsKey('quiet'))
        {
            Write-Host "Right NOT Found"
        }
        return $false
    }
}
function Get-vCDRightReference()
{
    <#
        .SYNOPSIS
            version 0.1 - Written by Dave Hocking 2017.
            This function will get the vCloud Director Right Reference for a given Right Name, using the vCloud API.
           
            Requires PowerCLI Support and an existing connection to the vCloud Server (connect-ciserver)
            The script makes a connection to vCD via the RestAPI via the function "connect-ciRest" from this same module file, using the supplied $server
            
        .DESCRIPTION
            Returns the right reference from a given right name, out of all available rights in vCloud Director
    
            Written and tested against vCD 8.20

            -------------------------------------------------------------------------------------------------------------------

            Collects the following information
            - Right Name (e.g. "Catalog: Add vApp from My Cloud") [ string ] MANDATORY
            - vCloud Server Name (e.g. "vcloud.isp.com") [ string ] MANDATORY

            Hidden parameters (switches) exist for -showXML and -showHeader to assist with any debugging that may be needed.
        
            -------------------------------------------------------------------------------------------------------------------

            RELATED COMMANDS
            Add-vCDorgCustomRole
            Test-vCDorgRights

        .EXAMPLE
           Get-vCDRightReference -rightRef -rightName "Catalog: Add vApp from My Cloud" -server "vcloud.isp.com"
            - connects to the vCD server vcloud.isp.com and gathers the Rights available
            - returns the reference for the right name, in this case "4886663f-ae31-37fc-9a70-3dbe2f24a8c5"
    #>

    # Allow advanced parameter functions
    [CmdletBinding()]

    # Define the Parameters to pass the to function
    Param(
  
    [Parameter(Mandatory=$true)]
    [String]$rightName,

    [Parameter(Mandatory=$true)]
    [string]$server,

    # Hidden options for debugging
    [Parameter(DontShow)]
    [switch]$showXML,

    [Parameter(DontShow)]
    [switch]$quiet,

    [Parameter(DontShow)]
    [switch]$showHeader
    )

    # Hidden option for Header output
    if ($PSBoundParameters.ContainsKey('showHeader'))
    {
        # Display the headers (from connect-ciREST)
        write-Host "`nDisplaying headers..."
        $ciHeaders
    }

    # Make a ciRest connection if no existing headers detected
    if (!$ciHeaders)
    {
        connect-ciREST -server $server
    }
    else
    {   
        if (!$PSBoundParameters.ContainsKey('quiet'))
        {
            Write-Host "`nUsing existing vCloud Rest connection..."
        }
    }

    if (!$PSBoundParameters.ContainsKey('quiet'))
    {
        Write-Host "Collecting data from vCloud Director..."
    }

    # Reset the rightRef variable
    $rightRef = $null

    # Set the URL for the list of available rights from the vCD Server supplied
    $rightsUrl = "https://$($server)/api/admin?fields=RightReferences"

    # Collect the list of assigned rights for the org
    if (!$PSBoundParameters.ContainsKey('quiet'))
    {
        write-host "Collecting available Rights from vCD Server : " -NoNewline
        Write-Host $server -ForegroundColor Yellow
    }
    $response = Invoke-RestMethod -Method Get -Uri $rightsUrl -Headers $ciHeaders
    
    # Test for the presence of the name you're looking for
    if (($response.VCloud.RightReferences.RightReference.name).Contains($($rightName)))
    {
        # Get the rightRef for the given rightName
        $rightRef = ((($response.VCloud.RightReferences.RightReference | Where-Object {$_.Name -eq $($rightName)}).href).split('/'))[-1]
        if (!$PSBoundParameters.ContainsKey('quiet'))
        {
            Write-Host "Reference/Right Found"
        }
        return $rightRef
    }

    else
    {
        if (!$PSBoundParameters.ContainsKey('quiet'))
        {
            Write-Host "Right NOT Found"
        }
        return $false
    }
}

function Set-vCDEdgeName()
{
    <#
        .SYNOPSIS
            version 0.1 - Written by Dave Hocking 2017.
            This function will rename a vCloud Director Edge, using the vCloud API
            Requires PowerCLI Support and an existing connection to the vCloud Server (connect-ciserver)
            The script makes a connection to vCD via the RestAPI via the function "connect-ciRest" from this same module file, using the supplied $server

        .DESCRIPTION
            The script takes the OrgVDC name that holds the Edge Gateway, the name of the Edge and a new desired name
            The script renames the Object in vCD ONLY
            Written and tested against vCD 8.10 - will need testing for validity against 8.20's new API set.

        .EXAMPLE
           Set-vCDEdgeName -orgVDC GB-DJH-SKJ-A1 -edge SKJ-A1-ESG1 -name GB-DJH-SKJ-A1-ESG1 -server "vcloud.isp.com"
            - connects to the Edge called "SKJ-A1-ESG1" in the OrgVDC "GB-DJH-SKJ-A1", on the vCD server vcloud.isp.com
            - renames the Edge to "GB-DJH-SKJ-A1-ESG1"

    #>

    # Allow advanced parameter functions
    [CmdletBinding()]

    # Define the Parameters to pass the to function
    Param(

    [Parameter(Mandatory=$true)]
    [ValidateScript({get-orgvdc $_})]
    [String]$orgVDC,

    [Parameter(Mandatory=$true)]
    [String]$edge,

    [Parameter(Mandatory=$true)]
    [String]$name,

    [Parameter(Mandatory=$true)]
    [string]$server,

    # Hidden options for debugging etc
    [Parameter(DontShow)]
    [switch]$showXML,

    [Parameter(DontShow)]
    [switch]$quiet,

    [Parameter(DontShow)]
    [switch]$showHeader
    )

    # Hidden option for Header output
    if ($PSBoundParameters.ContainsKey('showHeader'))
    {
        # Display the headers (from connect-ciREST)
        write-Host "`nDisplaying headers..."
        $ciHeaders
    }

    # Make a ciRest connection if no existing headers detected
    if (!$ciHeaders)
    {
        connect-ciREST -server $server
    }
    else
    {
        if (!$PSBoundParameters.ContainsKey('quiet'))
        {
            Write-Host "`nUsing existing vCloud Rest connection..."
        }
    }

    # Check if the edge you want to convert is an advanced edge
    if (!$PSBoundParameters.ContainsKey('quiet'))
    {
        write-host "Checking for Advanced Services status on ESG : " -NoNewline
        Write-Host $edge -ForegroundColor Yellow
    }
    $advancedEdge = Test-vCDedgeAdvanced -orgVDC $orgVDC -edge $edge -server $server -quiet

    # If it is, then decline to do any work then break
    if ($advancedEdge -eq $true)
    {
        write-host "Edge Services Gateway is running Advanced Networking, aborting."
        break
    }

    # Grab the OrgVDC supplied and pull down the configuration for the Gateway contained within
    if (!$PSBoundParameters.ContainsKey('quiet'))
    {
        Write-Host "Collecting data from vCloud Director..."
    }

    # The admin URL for the supplied object needs changing, bit dirty to use string work here, but hey. Who's looking?
    $orgVDCadminURL = (get-orgvdc $orgVDC).href
    $orgVDCid = ($orgVDCadminURL.split('/'))[-1]
    $orgVDCurl = "https://$($server)/api/vdc/$($orgVDCid)"

    # Grab the list of Edges in the OrgVDC
    $response = Invoke-RestMethod -Method Get -Uri $orgVDCurl -Headers $ciHeaders
    $edgeGatewayURL = ($response.Vdc.Link | Where-Object {$_.rel -eq "edgeGateways"}).href
    $response = Invoke-RestMethod -Method Get -Uri $edgeGatewayURL -Headers $ciHeaders

    # Check for the Edge with a matching name to the supplied string
    $edgeRecord = $response.QueryResultRecords.EdgeGatewayRecord | Where-Object {$_.name -eq "$edge"}

    # If no Edge found matching the supplied name, or if more than one supplied (vCD should prevent this) break out
    if (@($edgeRecord).count -ne 1){write-output "No Edge detected using supplied name `"$($edge)`", or more than one returned (should be impossible!)";break}
    if (!$PSBoundParameters.ContainsKey('quiet'))
    {
        done
        write-host "Collecting Edge Gateway configuration..."
    }

    # Grab the configuration from the edge
    $response = Invoke-RestMethod -Method Get -Uri $edgeRecord.href -Headers $ciHeaders
    if (!$PSBoundParameters.ContainsKey('quiet'))
    {
        done
    }

    # Store the configuration XML as a variable
    $edgeConfiguration = $response.InnerXml

    # Clone the original XML for use later
    $originalEdgeConfig = $edgeConfiguration

    # Hidden option for XML output
    if ($PSBoundParameters.ContainsKey('showXML'))
    {
        # Display the collected config
        write-Host "`nDisplaying current Edge XML config..."
        $edgeConfiguration
    }

    # Replace the ' name="SLO-Proact-A-ESG2" ' string with the new name
    $edgeConfiguration = $originalEdgeConfig.Replace(" name=`"$($edge)`" "," name=`"$($name)`" ")

    # Hidden option for XML output
    if ($PSBoundParameters.ContainsKey('showXML'))
    {
        # Display the new candidate config
        write-Host "`nDisplaying candidate XML config..."
        $edgeConfiguration
    }

    # Output the status
    if (!$PSBoundParameters.ContainsKey('quiet'))
    {
        Write-Host "Renaming " -NoNewline
        Write-Host "$($edge)" -NoNewline -ForegroundColor Yellow
        Write-Host " as " -NoNewline
        Write-Host "$($name)" -ForegroundColor Yellow
    }

    # Submit the new configuration as a PUT request, remembering to set the correct content type for submission
    $ciHeaders.Add("Content-Type","application/vnd.vmware.admin.edgeGateway+xml")
    $response = Invoke-RestMethod -Method Put -Uri $edgeRecord.href -Headers $ciHeaders -Body $edgeConfiguration
    $ciHeaders.Remove('Content-Type')

    # Check for task status, wait for completion
    $url = $response.Task.href
    Test-TaskStatus -wait -for success -url $url -mins 1 -quiet
    #>
    done
}

function Test-vCDedgeAdvanced()
{
        <#
        .SYNOPSIS
            version 0.1 - Written by Dave Hocking 2017.
            This function will test if a vCloud Director Edge has been converted to Advanced Services mode, using the vCloud API
            Requires PowerCLI Support and an existing connection to the vCloud Server (connect-ciserver)
            The script makes a connection to vCD via the RestAPI via the function "connect-ciRest" from this same module file, using the supplied $server

        .DESCRIPTION
            The script takes the OrgVDC name that holds the Edge Gateway and the name of the Edge Gateway to test
            It calls the Get-vCDedgeXML function to get the configuration and then it tests for the value of  Configuration.AdvancedNetworkingEnabled
            If Advanced Services are enabled, it's set to "true", otherwise it will be "false".
            Once the result has been obtained, this script will return a value of true/false

        .EXAMPLE
           Test-vCDedgeAdvanced -orgVDC GB-DJH-SKJ-A1 -edge SKJ-A1-ESG1 -server "vcloud.isp.com"
            - connects to the Edge called "SKJ-A1-ESG1" in the OrgVDC "GB-DJH-SKJ-A1", on the vCD server vcloud.isp.com
            - tests for the presence/value of the advanced services flag
            - Returns true/false

    #>

    # Allow advanced parameter functions
    [CmdletBinding()]

    # Define the Parameters to pass the to function
    Param(

    [Parameter(Mandatory=$true)]
    [ValidateScript({get-orgvdc $_})]
    [String]$orgVDC,

    [Parameter(Mandatory=$true)]
    [String]$edge,

    [Parameter(Mandatory=$true)]
    [string]$server,

    # Hidden options for debugging etc
    [Parameter(DontShow)]
    [switch]$showXML,

    [Parameter(DontShow)]
    [switch]$quiet,

    [Parameter(DontShow)]
    [switch]$showHeader
    )

    # Hidden option for Header output
    if ($PSBoundParameters.ContainsKey('showHeader'))
    {
        # Display the headers (from connect-ciREST)
        write-Host "`nDisplaying headers..."
        $ciHeaders
    }

    # Make a ciRest connection if no existing headers detected
    if (!$ciHeaders)
    {
        connect-ciREST -server $server
    }
    else
    {
        if (!$PSBoundParameters.ContainsKey('quiet'))
        {
            Write-Host "`nUsing existing vCloud Rest connection..."
        }
    }

    # Reset the $script:advancedESG variable to $null
    $script:advancedESG = $null

    # Hidden option for Header output
    if ($PSBoundParameters.ContainsKey('showHeader'))
    {
        # Display the headers (from connect-ciREST)
        write-Host "`nDisplaying headers..."
        $ciHeaders
    }

    # Make a ciRest connection if no existing headers detected
    if (!$ciHeaders)
    {
        connect-ciREST -server $server
    }
    else
    {
        if (!$PSBoundParameters.ContainsKey('quiet'))
        {
            Write-Host "`nUsing existing vCloud Rest connection..."
        }
    }

    # Wipe out any existing EdgeXML
    $edgeConfigurationXML = $null

    # Get the EdgeXML
    $edgeConfigurationXML = Get-vCDEdgeXML -orgVDC $orgVDC -edge $edge -server $server -quiet

    # The above script should have set the $edgeConfigurationXML variable
    if (!($edgeConfigurationXML))
    {
        Write-Warning "Couldn't get the Edge Gateway XML for this function"
        break
    }

    # If the configuration of the ESG contains the 'Configuration.AdvancedNetworkingEnabled -eq "true"'
    # then return $advancedESG = $true
    if ($edgeConfigurationXML.EdgeGateway.Configuration.AdvancedNetworkingEnabled -eq "true")
    {
        if (!$PSBoundParameters.ContainsKey('quiet'))
        {
            Write-Host "Edge Gateway " -NoNewline
            Write-Host " $($edge) " -NoNewline -ForegroundColor Yellow
            Write-Host "is an Advanced Services Edge Gateway"
        }
        return $true
    }

    # else if 'Configuration.AdvancedNetworkingEnabled -eq "false"', $advancedESG = $false
    # ..anything other than true/false, return $advancedESG = $null and display diagnostic message
    if ($edgeConfigurationXML.EdgeGateway.Configuration.AdvancedNetworkingEnabled -eq "false")
    {
        if (!$PSBoundParameters.ContainsKey('quiet'))
        {
            Write-Host "Edge Gateway " -NoNewline
            Write-Host " $($edge) " -NoNewline -ForegroundColor Yellow
            Write-Host "is not an Advanced Services Edge Gateway"
        }
        return $false
    }
    
}

function Convert-vCDedge()
{
    <#
        .SYNOPSIS
            version 0.1 - Written by Dave Hocking 2017.
            This function will convert a vCloud Director Edge to Advanced Services mode, using the vCloud API
            Requires PowerCLI Support and an existing connection to the vCloud Server (connect-ciserver)
            The script makes a connection to vCD via the RestAPI via the function "connect-ciRest" from this same module file, using the supplied $server

        .DESCRIPTION
            The script takes the OrgVDC name that holds the Edge Gateway and the name of the Edge Gateway to convert
            It calls the Test-vCDEdgeAdvanced function to get the advanced services status
            If Advanced Services are enabled, the script stops processing. If disabled, the script attempts a conversion to Advanced Networking
            
        .EXAMPLE
           Convert-vCDedge -orgVDC GB-DJH-SKJ-A1 -edge SKJ-A1-ESG1 -server "vcloud.isp.com"
            - connects to the Edge called "SKJ-A1-ESG1" in the OrgVDC "GB-DJH-SKJ-A1", on the vCD server vcloud.isp.com
            - if suitable, attempts to convert to Advanced Networking Services

    #>

    # Allow advanced parameter functions
    [CmdletBinding()]

    # Define the Parameters to pass the to function
    Param(

    [Parameter(Mandatory=$true)]
    [ValidateScript({get-orgvdc $_})]
    [String]$orgVDC,

    [Parameter(Mandatory=$true)]
    [String]$edge,

    [Parameter(Mandatory=$true)]
    [string]$server,

    # Hidden options for debugging etc
    [Parameter(DontShow)]
    [switch]$showXML,

    [Parameter(DontShow)]
    [switch]$quiet,

    [Parameter(DontShow)]
    [switch]$showHeader
    )

    # Hidden option for Header output
    if ($PSBoundParameters.ContainsKey('showHeader'))
    {
        # Display the headers (from connect-ciREST)
        write-Host "`nDisplaying headers..."
        $ciHeaders
    }

    # Make a ciRest connection if no existing headers detected
    if (!$ciHeaders)
    {
        connect-ciREST -server $server
    }
    else
    {
        if (!$PSBoundParameters.ContainsKey('quiet'))
        {
            Write-Host "`nUsing existing vCloud Rest connection..."
        }
    }

    # Function is used to convert a vCloud Edge into an Advanced Edge - at which point a new API is needed to control it
    # see VMware KB2147625 for more info, also http://pubs.vmware.com/vcd-820/topic/com.vmware.ICbase/PDF/vcloud_nsx_api_guide_27_0.pdf
    # clear out the variable
    $advancedEdge = $null

    # Check if the edge you want to convert is already an advanced edge
    if (!$PSBoundParameters.ContainsKey('quiet'))
    {
        write-host "Checking for Advanced Services status on ESG : " -NoNewline
        Write-Host $edge -ForegroundColor Yellow
    }
    $advancedEdge = Test-vCDedgeAdvanced -orgVDC $orgVDC -edge $edge -server $server -quiet

    # If it is, then decline to do any work then break
    if ($advancedEdge -eq $true)
    {
        if (!$PSBoundParameters.ContainsKey('quiet'))
        {
            write-host "Edge Services Gateway is already running Advanced Networking"
            done
        }
        break
    }

    # If it isn't, then proceed
    if ($advancedEdge -eq $false)
    {

        write-host "Edge Services Gateway ready to be upgraded to Advanced Networking"
   
        # Wipe out any existing EdgeXML
        $edgeConfigurationXML = $null

        # Get the EdgeXML
        $edgeConfigurationXML = Get-vCDEdgeXML -orgVDC $orgVDC -edge $edge -server $server -quiet
        
        # Get the EdgeID (not edge-123)
        $edgeID = ($edgeConfigurationXML.edgegateway.id).split(':')[-1]
        if (!$PSBoundParameters.ContainsKey('quiet'))
        {
            write-host "Edge Services Gateway ID : " -NoNewline
            Write-Host $edgeID -ForegroundColor Yellow
        }

        # POST to https://$($server)/api/admin/edgeGateway/$($edgeID)/action/convertToAdvancedGateway
        $url = "https://$($server)/api/admin/edgeGateway/$($edgeID)/action/convertToAdvancedGateway"

        # Convert to Advanced Services
        $response = Invoke-RestMethod -Method Post -Uri $url -Headers $ciHeaders

        # Check for task status, wait for completion
        $url = $response.Task.href
        Test-TaskStatus -wait -for success -url $url -mins 1 -quiet

        # Return successfully
        if (!$PSBoundParameters.ContainsKey('quiet'))
        {
            done
        }
        break
    }
}

function Get-vCDEdgeXML()
{
    <#
        .SYNOPSIS
            version 0.1 - Written by Dave Hocking 2017.
            This function will collect a vCloud Director Edge's configuration as XML, using the vCloud API
            Requires PowerCLI Support and an existing connection to the vCloud Server (connect-ciserver)
            The script makes a connection to vCD via the RestAPI via the function "connect-ciRest" from this same module file, using the supplied $server

        .DESCRIPTION
            The script takes the OrgVDC name that holds the Edge Gateway and the name of the Edge as inputs
            The script only currently outputs the XML to screen if the user sets the hidden -showXML switch
            This function should be updated so it's called from other functions that operate on the Edge's in vCD.
            An XML export option should also be added, to allow the function to store the XML as a local file

            Written and tested against vCD 8.10 - will need testing for validity against 8.20's new API set.

        .EXAMPLE
           Get-vCDEdgeXML -orgVDC GB-DJH-SKJ-A1 -edge SKJ-A1-ESG1 -server "vcloud.isp.com"
            - connects to the Edge called "SKJ-A1-ESG1" in the OrgVDC "GB-DJH-SKJ-A1", on the vCD server vcloud.isp.com
            - Outputs the XML to the screen

    #>

    # Allow advanced parameter functions
    [CmdletBinding()]

    # Define the Parameters to pass the to function
    Param(

    [Parameter(Mandatory=$true)]
    [ValidateScript({get-orgvdc $_})]
    [String]$orgVDC,

    [Parameter(Mandatory=$true)]
    [String]$edge,

    [Parameter(Mandatory=$true)]
    [string]$server,

    # Hidden options for debugging etc
    [Parameter(DontShow)]
    [switch]$showXML,

    [Parameter(DontShow)]
    [switch]$quiet,

    [Parameter(DontShow)]
    [switch]$showHeader
    )

    # Hidden option for Header output
    if ($PSBoundParameters.ContainsKey('showHeader'))
    {
        # Display the headers (from connect-ciREST)
        write-Host "`nDisplaying headers..."
        $ciHeaders
    }

    # Make a ciRest connection if no existing headers detected
    if (!$ciHeaders)
    {
        connect-ciREST -server $server
    }
    else
    {
        if (!$PSBoundParameters.ContainsKey('quiet'))
        {
            Write-Host "`nUsing existing vCloud Rest connection..."
        }
    }


    # Grab the OrgVDC supplied and pull down the configuration for the Gateway contained within
    if (!$PSBoundParameters.ContainsKey('quiet'))
    {
        Write-Host "Collecting data from vCloud Director..."
    }

    # The admin URL for the supplied object needs changing, bit dirty to use string work here, but hey. Who's looking?
    $orgVDCadminURL = (get-orgvdc $orgVDC).href
    $orgVDCid = ($orgVDCadminURL.split('/'))[-1]
    $orgVDCurl = "https://$($server)/api/vdc/$($orgVDCid)"

    # Grab the list of Edges in the OrgVDC
    $response = Invoke-RestMethod -Method Get -Uri $orgVDCurl -Headers $ciHeaders
    $edgeGatewayURL = ($response.Vdc.Link | Where-Object {$_.rel -eq "edgeGateways"}).href
    $response = Invoke-RestMethod -Method Get -Uri $edgeGatewayURL -Headers $ciHeaders

    # Check for the Edge with a matching name to the supplied string
    $edgeRecord = $response.QueryResultRecords.EdgeGatewayRecord | Where-Object {$_.name -eq "$edge"}

    # If no Edge found matching the supplied name, or if more than one supplied (vCD should prevent this) break out
    if (@($edgeRecord).count -ne 1){write-output "No Edge detected using supplied name `"$($edge)`", or more than one returned (should be impossible!)";break}
    if (!$PSBoundParameters.ContainsKey('quiet'))
    {
        done
        write-host "Collecting Edge Gateway configuration..."
    }

    # Grab the configuration from the edge
    $response = Invoke-RestMethod -Method Get -Uri $edgeRecord.href -Headers $ciHeaders
    if (!$PSBoundParameters.ContainsKey('quiet'))
    {
        done
    }

    # Store the configuration XML as a variable
    $edgeConfiguration = $response
    $edgeConfigurationXML = $response.InnerXml

    # Hidden option for XML output
    if ($PSBoundParameters.ContainsKey('showXML'))
    {
        # Display the collected config
        write-Host "`nDisplaying current Edge XML config..."
        $edgeConfigurationXML
    }

    # Send the variables back to any calling script
    return $edgeConfiguration
    return $edgeConfigurationXML
}

function Set-vCDEdgeSyslog()
{
    <#
        .SYNOPSIS
            version 0.1 - Written by Dave Hocking 2017.
            This function will configure a vCloud Director Edge to use a specified Syslog Server, using the vCloud API
            Requires PowerCLI Support and an existing connection to the vCloud Server (connect-ciserver)
            The script makes a connection to vCD via the RestAPI via the function "connect-ciRest" from this same module file, using the supplied $server

        .DESCRIPTION
            The script takes the OrgVDC name that holds the Edge Gateway, the name of the Edge and a desired syslog server
            The script replaces any existing Syslog Server config and doesn't support adding more than one, as yet
            It seems that the vCD API wants the Syslog Server adding via IP, FQDN hasn't been tested as yet
            Written and tested against vCD 8.10 - will need testing for validity against 8.20's new API set.

        .EXAMPLE
           Set-vCDEdgeSyslog -orgVDC GB-DJH-SKJ-A1 -edge SKJ-A1-ESG1 -syslog 185.39.247.104 -server "vcloud.isp.com"
            - connects to the Edge called "SKJ-A1-ESG1" in the OrgVDC "GB-DJH-SKJ-A1", on the vCD server vcloud.isp.com
            - Sets the Syslog server for the Edge to "185.39.247.104"

    #>

    # Allow advanced parameter functions
    [CmdletBinding()]

    # Define the Parameters to pass the to function
    Param(

    [Parameter(Mandatory=$true)]
    [ValidateScript({get-orgvdc $_})]
    [String]$orgVDC,

    [Parameter(Mandatory=$true)]
    [String]$edge,

    [Parameter(Mandatory=$true)]
    [String]$syslog,

    [Parameter(Mandatory=$true)]
    [string]$server,

    # Hidden options for debugging etc
    [Parameter(DontShow)]
    [switch]$showXML,

    [Parameter(DontShow)]
    [switch]$quiet,

    [Parameter(DontShow)]
    [switch]$showHeader
    )

    # Hidden option for Header output
    if ($PSBoundParameters.ContainsKey('showHeader'))
    {
        # Display the headers (from connect-ciREST)
        write-Host "`nDisplaying headers..."
        $ciHeaders
    }

    # Make a ciRest connection if no existing headers detected
    if (!$ciHeaders)
    {
        connect-ciREST -server $server
    }
    else
    {
        if (!$PSBoundParameters.ContainsKey('quiet'))
        {
            Write-Host "`nUsing existing vCloud Rest connection..."
        }
    }

    # Check if the edge you want to convert is an advanced edge
    if (!$PSBoundParameters.ContainsKey('quiet'))
    {
        write-host "Checking for Advanced Services status on ESG : " -NoNewline
        Write-Host $edge -ForegroundColor Yellow
    }
    $advancedEdge = Test-vCDedgeAdvanced -orgVDC $orgVDC -edge $edge -server $server -quiet

    # If it is, then decline to do any work then break
    if ($advancedEdge -eq $true)
    {
        write-host "Edge Services Gateway is running Advanced Networking, aborting."
        break
    }

    # Grab the OrgVDC supplied and pull down the configuration for the Gateway contained within
    if (!$PSBoundParameters.ContainsKey('quiet'))
    {
        Write-Host "Collecting data from vCloud Director..."
    }

    # The admin URL for the supplied object needs changing, bit dirty to use string work here, but hey. Who's looking?
    $orgVDCadminURL = (get-orgvdc $orgVDC).href
    $orgVDCid = ($orgVDCadminURL.split('/'))[-1]
    $orgVDCurl = "https://$($server)/api/vdc/$($orgVDCid)"

    # Grab the list of Edges in the OrgVDC
    $response = Invoke-RestMethod -Method Get -Uri $orgVDCurl -Headers $ciHeaders
    $edgeGatewayURL = ($response.Vdc.Link | Where-Object {$_.rel -eq "edgeGateways"}).href
    $response = Invoke-RestMethod -Method Get -Uri $edgeGatewayURL -Headers $ciHeaders

    # Check for the Edge with a matching name to the supplied string
    $edgeRecord = $response.QueryResultRecords.EdgeGatewayRecord | Where-Object {$_.name -eq "$edge"}

    # If no Edge found matching the supplied name, or if more than one supplied (vCD should prevent this) break out
    if (@($edgeRecord).count -ne 1){write-output "No Edge detected using supplied name `"$($edge)`", or more than one returned (should be impossible!)";break}
    if (!$PSBoundParameters.ContainsKey('quiet'))
    {
        done
        write-host "Collecting Edge Gateway configuration..."
    }

    # Grab the configuration from the edge
    $response = Invoke-RestMethod -Method Get -Uri $edgeRecord.href -Headers $ciHeaders
    if (!$PSBoundParameters.ContainsKey('quiet'))
    {
        done
    }

    # Store the configuration XML as a variable
    $edgeConfiguration = $response.InnerXml

    # Store the Syslog configuration as an Object
    $edgeTenantSyslogConfiguration = $response.EdgeGateway.Configuration.SyslogServerSettings.TenantSyslogServerSettings

    # Clone the original XML for use later
    $originalEdgeConfig = $edgeConfiguration

    # Hidden option for XML output
    if ($PSBoundParameters.ContainsKey('showXML'))
    {
        # Display the collected config
        write-Host "`nDisplaying current Edge XML config..."
        $edgeConfiguration
    }

    # Check for existing syslog server IP, if there isn't one, skip, as the XML replacement currently relies upon one being there.
    if (!($edgeTenantSyslogConfiguration.SyslogServerIp))
    {
        Write-Host "No current Syslog Server IP exists (one hasn't been set on the NSX Manager)..."
        Write-Host "This function (beta) requires an existing server to use as string replacement in the XML Skipping..."
        $edgeTenantSyslogConfiguration
        done
    }
    else
    {
        # Grab the IP address of the first syslog server specified in the XML - again, this could easily be improved in behaviour
        $oldSyslog = @($edgeTenantSyslogConfiguration.SyslogServerIp)[0]

        # Build out the new configuration from the supplied information
        $oldXML = "<SyslogServerIp>$($oldSyslog)</SyslogServerIp>"
        $newXML = "<SyslogServerIp>$($syslog)</SyslogServerIp>"

        # Replace the Tenant Syslog Server section with the code above
        $edgeConfiguration = $originalEdgeConfig.Replace($oldXML,$newXML)

        # Hidden option for XML output
        if ($PSBoundParameters.ContainsKey('showXML'))
        {
            # Display the new candidate config
            write-Host "`nDisplaying candidate XML config..."
            $edgeConfiguration
        }

        # Output the status
        if (!$PSBoundParameters.ContainsKey('quiet'))
        {
            Write-Host "Setting " -NoNewline
            Write-Host "$($syslog)" -NoNewline -ForegroundColor Yellow
            Write-Host " as a syslog server..."
        }

        # Submit the new configuration as a PUT request, remembering to set the correct content type for submission
        $ciHeaders.Add("Content-Type","application/vnd.vmware.admin.edgeGateway+xml")
        $response = Invoke-RestMethod -Method Put -Uri $edgeRecord.href -Headers $ciHeaders -Body $edgeConfiguration
        $ciHeaders.Remove('Content-Type')

        # Check for task status, wait for completion
        $url = $response.Task.href
        Test-TaskStatus -wait -for success -url $url -mins 1 -quiet
        #>
        #>
        done
    }
}

function add-vCDEdgeSubAllocation()
{
    <#
        .SYNOPSIS
            version 0.1 - Written by Dave Hocking 2017.
            This function will allocate the Edge's External IP as a new SubAllocation, using the vCloud API
            Requires PowerCLI Support and an existing connection to the vCloud Server (connect-ciserver)
            The script makes a connection to vCD via the RestAPI via the function "connect-ciRest" from this same module file, using the supplied $server

        .DESCRIPTION
            Gathers external IP address of Edge Gateway and adds it as a new suballocation
            It's a quick and dirty function that can be easily improved to support a choice or range of IPs - One for another day
            Written and tested against vCD 8.10 - will need testing for validity against 8.20's new API set.

        .EXAMPLE
           add-vCDEdgeSubAllocation -orgVDC GB-DJH-SKJ-A1 -edge SKJ-A-ESG1 -server "vcloud.isp.com"
            - connects to the Edge called "SKJ-A-ESG1" in the OrgVDC "GB-DJH-SKJ-A1", on the vCD server vcloud.isp.com
            - gathers the allocated External IP (uplink interface)
            - adds the gathered IP as a new SubAllocation to the Edge

    #>

    # Allow advanced parameter functions
    [CmdletBinding()]

    # Define the Parameters to pass the to function
    Param(

    [Parameter(Mandatory=$true)]
    [ValidateScript({get-orgvdc $_})]
    [String]$orgVDC,

    [Parameter(Mandatory=$true)]
    [String]$edge,

    [Parameter(Mandatory=$true)]
    [string]$server,

    # Hidden options for debugging
    [Parameter(DontShow)]
    [switch]$showXML,

    [Parameter(DontShow)]
    [switch]$quiet,

    [Parameter(DontShow)]
    [switch]$showHeader
    )

    # Hidden option for Header output
    if ($PSBoundParameters.ContainsKey('showHeader'))
    {
        # Display the headers (from connect-ciREST)
        write-Host "`nDisplaying headers..."
        $ciHeaders
    }

    # Make a ciRest connection if no existing headers detected
    if (!$ciHeaders)
    {
        connect-ciREST -server $server
    }
    else
    {
        if (!$PSBoundParameters.ContainsKey('quiet'))
        {
            Write-Host "`nUsing existing vCloud Rest connection..."
        }
    }

    # Check if the edge you want to convert is an advanced edge
    if (!$PSBoundParameters.ContainsKey('quiet'))
    {
        write-host "Checking for Advanced Services status on ESG : " -NoNewline
        Write-Host $edge -ForegroundColor Yellow
    }
    $advancedEdge = Test-vCDedgeAdvanced -orgVDC $orgVDC -edge $edge -server $server -quiet

    # If it is, then decline to do any work then break
    if ($advancedEdge -eq $true)
    {
        write-host "Edge Services Gateway is running Advanced Networking, aborting."
        break
    }

    # Grab the OrgVDC supplied and pull down the configuration for the Gateway contained within
    if (!$PSBoundParameters.ContainsKey('quiet'))
    {
        Write-Host "Collecting data from vCloud Director..."
    }

    # The admin URL for the supplied object needs changing, bit dirty to use string work here, but hey. Who's looking?
    $orgVDCadminURL = (get-orgvdc $orgVDC).href
    $orgVDCid = ($orgVDCadminURL.split('/'))[-1]
    $orgVDCurl = "https://$($server)/api/vdc/$($orgVDCid)"

    # Grab the list of Edges in the OrgVDC
    $response = Invoke-RestMethod -Method Get -Uri $orgVDCurl -Headers $ciHeaders
    $edgeGatewayURL = ($response.Vdc.Link | Where-Object {$_.rel -eq "edgeGateways"}).href
    $response = Invoke-RestMethod -Method Get -Uri $edgeGatewayURL -Headers $ciHeaders

    # Check for the Edge with a matching name to the supplied string
    $edgeRecord = $response.QueryResultRecords.EdgeGatewayRecord | Where-Object {$_.name -eq "$edge"}

    # If no Edge found matching the supplied name, or if more than one supplied (vCD should prevent this) break out
    if (@($edgeRecord).count -ne 1){write-output "No Edge detected using supplied name `"$($edge)`", or more than one returned (should be impossible!)";break}
    if (!$PSBoundParameters.ContainsKey('quiet'))
    {
        done
        write-host "Collecting Edge Gateway configuration..."
    }

    # Grab the configuration from the edge
    $response = Invoke-RestMethod -Method Get -Uri $edgeRecord.href -Headers $ciHeaders
    if (!$PSBoundParameters.ContainsKey('quiet'))
    {
        done
    }

    # Store the configuration XML as a variable
    $edgeConfiguration = $response.InnerXml

    # Store the GW configuration XML as a variable and filter to just show the uplink interface
    $edgeGWConfiguration = $response.EdgeGateway.Configuration.GatewayInterfaces.GatewayInterface
    $edgeGWConfiguration = $edgeGWConfiguration | Where-Object {$_.InterfaceType -eq "uplink"}

    # Clone the original XML for use later
    $originalEdgeConfig = $edgeConfiguration

    # Hidden option for XML output
    if ($PSBoundParameters.ContainsKey('showXML'))
    {
        # Display the collected config
        write-Host "`nDisplaying current Edge GW XML config..."
        $edgeGWConfiguration.innerXML
    }

    # Check for existing <IpRanges> section (denoting that suballocation has already happened)
    if ($edgeGWConfiguration.SubnetParticipation.IpRanges)
    {
        Write-Host "Sub-Allocation already exists..."
        Write-Host "$($edgeGWConfiguration.SubnetParticipation.IpRanges.innerXML)" -ForegroundColor Green
        Write-Host "Skipping..."
    }
    else
    {
        # Grab the IP address of the External Interface
        $edgeGatewayExtIP = $edgeGWConfiguration.SubnetParticipation.IpAddress

        # Build out the new configuration from the supplied information, spacing only important if you're going to dump out the generated XML [-showXML]
        $newXML =
@"
    <IpAddress>$($edgeGatewayExtIP)</IpAddress>
                            <IpRanges>
                                <IpRange>
                                    <StartAddress>$($edgeGatewayExtIP)</StartAddress>
                                    <EndAddress>$($edgeGatewayExtIP)</EndAddress>
                                </IpRange>
                            </IpRanges>`n
"@

        # Replace the <IpAddress>$($edgeGatewayExtIP)</IpAddress> section with the code above
        $edgeConfiguration = $originalEdgeConfig.Replace("<IpAddress>$($edgeGatewayExtIP)</IpAddress>",$newXML)

        # Hidden option for XML output
        if ($PSBoundParameters.ContainsKey('showXML'))
        {
            # Display the new candidate config
            write-Host "`nDisplaying candidate XML config..."
            $edgeConfiguration
        }

        # Output the status
        if (!$PSBoundParameters.ContainsKey('quiet'))
        {
            Write-Host "Adding " -NoNewline
            Write-Host "$($edgeGatewayExtIP)" -NoNewline -ForegroundColor Yellow
            Write-Host " as a sub-allocation..."
        }

        # Submit the new configuration as a PUT request, remembering to set the correct content type for submission
        $ciHeaders.Add("Content-Type","application/vnd.vmware.admin.edgeGateway+xml")
        $response = Invoke-RestMethod -Method Put -Uri $edgeRecord.href -Headers $ciHeaders -Body $edgeConfiguration
        $ciHeaders.Remove('Content-Type')

        # Check for task status, wait for completion
        $url = $response.Task.href
        Test-TaskStatus -wait -for success -url $url -mins 1 -quiet
        #>
    }
    done
}

function Get-vCDedgeFWrule()
{
    <#
        .SYNOPSIS
            version 0.1 - Written by Dave Hocking 2017.
            This function will display NSX Edge based firewall rules, using the vCloud API
            Requires PowerCLI Support and an existing connection to the vCloud Server (connect-ciserver)
            The script makes a connection to vCD via the RestAPI via the function "connect-ciRest" from this same module file, using the supplied $server
            Optionally, you can specify -exportPath to save a CSV for further use - The Parent Directory must exist.

        .DESCRIPTION
            Gathers details for deployed firewall rules
            Written and tested against vCD 8.10 - will need testing for validity against 8.20's new API set.

            -------------------------------------------------------------------------------------------------------------------

            Collects the following information
            - OrgVDC Name (e.g. "GB-DJH-SKJ-A1") [ string ] MANDATORY
            - Edge Gateway Name (e.g. "SKJ-A-ESG1") [ string ] MANDATORY
            - vCloud Server Name (e.g. "vcloud.isp.com") [ string ] MANDATORY

            ...then displays the data in a table, with the following columns:
              enabled = [true|false]
              name = [string]
              action = [allow|drop]
              proto = [tcp|udp|tcp+udp|icmp|any]
              destprt = [any|1-65535]
              dest = [any|internal|external|10.20.30.40|10.20.30.40-10.20.30.50|10.20.30.0/24]
              srcprt = [any|1-65535]
              src = [any|internal|external|10.20.30.40|10.20.30.40-10.20.30.50|10.20.30.0/24]
              log = [true|false]

            Hidden parameters (switches) exist for -showXML and -showHeader to assist with any debugging that may be needed.

            -------------------------------------------------------------------------------------------------------------------

            RELATED COMMANDS
            Add-vCDEdgeFWRule

        .EXAMPLE
           Get-vCDedgeFWrule -orgVDC GB-DJH-SKJ-A1 -edge SKJ-A-ESG1 -server "vcloud.isp.com"
            - connects to the Edge called "SKJ-A-ESG1" in the OrgVDC "GB-DJH-SKJ-A1", on the vCD server vcloud.isp.com
            - displays a table of collected Firewall rules, using the following headings
            enabled,name,action,proto,destprt,dest,srcprt,src,log

        .EXAMPLE
           Get-vCDedgeFWrule -orgVDC GB-DJH-SKJ-A1 -edge SKJ-A-ESG1 -server vcloud.isp.com -exportPath c:\temp\export.csv
            - connects to the Edge called "SKJ-A-ESG1" in the OrgVDC "GB-DJH-SKJ-A1", on the vCD server "vcloud.isp.com"
            - displays a table of collected Firewall rules, using the following headings
            enabled,name,action,proto,destprt,dest,srcprt,src,log
            - saves the tabular output to a CSV file specified by the -exportPath switch (The C:\temp folder must exist!)

    #>

    # Allow advanced parameter functions
    [CmdletBinding()]

    # Define the Parameters to pass the to function
    Param(

    [Parameter(Mandatory=$true)]
    [ValidateScript({get-orgvdc $_})]
    [String]$orgVDC,

    [Parameter(Mandatory=$true)]
    [String]$edge,

    [Parameter(Mandatory=$true)]
    [string]$server,

    [Parameter(Mandatory=$false)]
    #[ValidateScript({test-path (Split-Path $exportPath)})]
    [string]$exportPath,

    # Hidden options for debugging
    [Parameter(DontShow)]
    [switch]$showXML,

    [Parameter(DontShow)]
    [switch]$showHeader
    )

    # Hidden option for Header output
    if ($PSBoundParameters.ContainsKey('showHeader'))
    {
        # Display the headers (from connect-ciREST)
        write-Host "`nDisplaying headers..."
        $ciHeaders
    }

    # Make a ciRest connection if no existing headers detected
    if (!$ciHeaders)
    {
        connect-ciREST -server $server
    }
    else
    {
        Write-Host "`nUsing existing vCloud Rest connection..."
    }

    # Check if the edge you want to convert is an advanced edge
    if (!$PSBoundParameters.ContainsKey('quiet'))
    {
        write-host "Checking for Advanced Services status on ESG : " -NoNewline
        Write-Host $edge -ForegroundColor Yellow
    }
    $advancedEdge = Test-vCDedgeAdvanced -orgVDC $orgVDC -edge $edge -server $server -quiet

    # If it is, then decline to do any work then break
    if ($advancedEdge -eq $true)
    {
        write-host "Edge Services Gateway is running Advanced Networking, aborting."
        break
    }

    # Grab the OrgVDC supplied and pull down the configuration for the Gateway contained within
    Write-Host "Collecting data from vCloud Director..."

    # The admin URL for the supplied object needs changing, bit dirty to use string work here, but hey. Who's looking?
    $orgVDCadminURL = (get-orgvdc $orgVDC).href
    $orgVDCid = ($orgVDCadminURL.split('/'))[-1]
    $orgVDCurl = "https://$($server)/api/vdc/$($orgVDCid)"

    # Grab the list of Edges in the OrgVDC
    $response = Invoke-RestMethod -Method Get -Uri $orgVDCurl -Headers $ciHeaders
    $edgeGatewayURL = ($response.Vdc.Link | Where-Object {$_.rel -eq "edgeGateways"}).href
    $response = Invoke-RestMethod -Method Get -Uri $edgeGatewayURL -Headers $ciHeaders

    # Check for the Edge with a matching name to the supplied string
    $edgeRecord = $response.QueryResultRecords.EdgeGatewayRecord | Where-Object {$_.name -eq "$edge"}

    # If no Edge found matching the supplied name, or if more than one supplied (vCD should prevent this) break out
    if (@($edgeRecord).count -ne 1){write-output "No Edge detected using supplied name `"$($edge)`", or more than one returned (should be impossible!)";break}
    done

    # Grab the configuration from the edge
    write-host "Collecting Edge Gateway configuration..."
    $response = Invoke-RestMethod -Method Get -Uri $edgeRecord.href -Headers $ciHeaders
    done

    # Store the configuration XML as variables
    $edgeFWConfiguration = $response.EdgeGateway.Configuration.EdgeGatewayServiceConfiguration.FirewallService

    # Hidden option for XML output
    if ($PSBoundParameters.ContainsKey('showXML'))
    {
        # Display the collected config
        write-Host "`nDisplaying current Firewall Service XML config..."
        $edgeFWConfiguration.innerXML
    }

    # Create Table Object for candidate rule(s)
    $ruleTable = New-Object system.Data.DataTable "ruleTable"

    # Define Columns for ruleTable
    $cols = @()
    $cols += New-Object system.Data.DataColumn id,([int])
    $cols += New-Object system.Data.DataColumn Name,([string])
    $cols += New-Object system.Data.DataColumn Enabled,([string])
    $cols += New-Object system.Data.DataColumn Action,([string])
    $cols += New-Object system.Data.DataColumn Log,([string])
    $cols += New-Object system.Data.DataColumn Proto,([string])
    $cols += New-Object system.Data.DataColumn Dest,([string])
    $cols += New-Object system.Data.DataColumn DestPrt,([string])
    $cols += New-Object system.Data.DataColumn Src,([string])
    $cols += New-Object system.Data.DataColumn SrcPrt,([string])

    # Add the Columns
    foreach ($col in $cols) {$ruleTable.columns.add($col)}

    # Parse the $EdgeConfiguration file and add each entry to the table
    Write-Host "Populating table from configuration export..."
    foreach ($rule in $edgeFWConfiguration.FirewallRule)
    {
        # Populate a row in the Rules Table
        $row = $ruleTable.NewRow()

        # Enter data in the row
        $row.ID = $rule.id
        $row.Name = $rule.Description
        $row.Enabled = $rule.IsEnabled
        $row.Action = $rule.Policy
        $row.Log = $rule.EnableLogging

        # Protocol needs handling differently
        if ($rule.protocols.any){$protocol = "any"}
        if ($rule.protocols.icmp){$protocol = "icmp"}
        if ($rule.protocols.tcp -and $rule.protocols.udp){$protocol = "tcp+udp"}
        else
        {
            if ($rule.protocols.udp){$protocol = "udp"}
            if ($rule.protocols.tcp){$protocol = "tcp"}
        }

        $row.Proto = $protocol
        $row.Dest = $rule.DestinationIp
        $row.DestPrt = $rule.DestinationPortRange
        $row.Src = $rule.SourceIp
        $row.SrcPrt = $rule.SourcePortRange

        # Add the row to the table
        $ruleTable.Rows.Add($row)
    }
    done

    # Output the collected rules
    Write-Host "Current rulebase as follows..."
    $ruleTable | Sort-Object {$_.id} | Format-Table
    done

    # Check for exportPath option for CSV export
    if ($PSBoundParameters.ContainsKey('exportPath'))
    {
        # Output the data to file
        write-Host "Exporting Firewall Rulebase to CSV `"" -NoNewline
        write-host "$($exportPath)" -NoNewline -ForegroundColor Yellow
        write-host "`""
        write-host "Existing file will be overwritten, make sure path exists also."
        confirm-continue
        if ($break)
        {
            break
        }

        write-Host "Exporting..."
        $ruleTable | Sort-Object {$_.id} | Export-Csv -Path $($exportPath) -NoTypeInformation
        done
    }
}

function Add-vCDedgeFWrule()
{
    <#
        .SYNOPSIS
            version 0.1 - Written by Dave Hocking 2017.
            This function will create NSX Edge based firewall rules, using the vCloud API, for vCD based rule entry
            Any existing rules will be left alone, this only adds new rules
            Requires PowerCLI Support and an existing connection to the vCloud Server (connect-ciserver)
            The script makes a connection to vCD via the RestAPI via the function "connect-ciRest" from this same module file, using the supplied $server
            Optionally, you can pass a CSV file to this function to allow for more simplified input of multiple records

        .DESCRIPTION
            Gathers details for firewall rule creation, then supplies the XML to vCD for submission.
            Written and tested against vCD 8.10 - will need testing for validity against 8.20's new API set.

            -------------------------------------------------------------------------------------------------------------------

            Collects the following information
            - OrgVDC Name (e.g. "GB-DJH-SKJ-A1") [ string ] MANDATORY
            - Edge Gateway Name (e.g. "SKJ-A-ESG1") [ string ] MANDATORY
            - vCloud Server Name (e.g. "vcloud.isp.com") [ string ] MANDATORY
            - Rule Name (Doesn't have to be unique, but rules cannot be duplicates) [ string ] MANDATORY

            ... then some of the below
            - Rule enabled? [ true | false ] OPTIONAL SWITCH, DEFAULT to "false"
            - Rule Action [ drop ] OPTIONAL SWITCH, DEFAULT TO "allow" otherwise
            - Protocol/s [ tcp | udp | tcp+udp | icmp | any ] OPTIONAL, DEFAULT TO "tcp"
            - Destination Port/s [ single-integer,1-65535 | contiguous-integer-range,1-65535 | any ] OPTIONAL, DEFAULT TO "any"
            - Destination IP/s [ single-ip | ip-range | cidr-notation | internal | external | any ] OPTIONAL, DEFAULT TO "any"
            - Source Port/s [ single-integer,1-65535 | contiguous-integer-range,1-65535 | any ] OPTIONAL, DEFAULT TO "any"
            - Source IP/s [ single-ip | ip-range | cidr-notation  | internal | external | any ] OPTIONAL, DEFAULT TO "any"
            - Logging Enabled? [ true | false ] OPTIONAL SWITCH, DEFAULT to "false"

            ...or the same fields, as specified in a CSV file that can be specified with:
            -import rulebase.csv [ Local File, must exist ] MANDATORY

            When using the CSV import option, the CSV file should contain the following columns:
              enabled,name,action,proto,destprt,dest,srcprt,src,log

            The options available under each column are:
              enabled = [true|false]
              name = *
              action = [allow|drop]
              proto = [tcp|udp|tcp+udp|icmp|any]
              destprt = [any|1-65535]
              dest = [any|internal|external|10.20.30.40|10.20.30.40-10.20.30.50|10.20.30.0/24]
              srcprt = [any|1-65535]
              src = [any|internal|external|10.20.30.40|10.20.30.40-10.20.30.50|10.20.30.0/24]
              log = [true|false]

            -------------------------------------------------------------------------------------------------------------------

            NB: Source and Destination Ports DO NOT support CSV-style port entry.
            Only single ports or contiguous port numbers are supported by vCD API

            The request will get the latest copy of the ESG firewall rules and place a new rule at the end
            The new rule will contain the information submitted above, and the ESG will be immediately reconfigured
            The task to reconfigure the firewall MUST be allowed to complete (success or failure) before this function returns.
            Should a collection of changes be needed, the import option should be used.

            Hidden parameters (switches) exist for -showXML and -showHeader to assist with any debugging that may be needed.

            -------------------------------------------------------------------------------------------------------------------

            RELATED COMMANDS
            Get-vCDEdgeFWRule

        .EXAMPLE
           Add-vCDedgeFWrule -orgVDC GB-DJH-SKJ-A1 -edge SKJ-A-ESG1 -import "myRules.csv" -server "vcloud.isp.com"
            - connects to the Edge called "SKJ-A-ESG1" in the OrgVDC "GB-DJH-SKJ-A1", on the vCD server vcloud.isp.com
            - imports the CSV file called "myRules.csv"
            - processes all rules in the CSV as one submission to the vCD server
            - the CSV file must take the following form
            enabled,name,action,proto,destprt,dest,srcprt,src,log
            true,Web Access,allow,tcp,443,external,any,internal,true
            true,Web Access,allow,tcp,80,external,any,internal,true
            true,DNS,allow,tcp+udp,53,external,any,internal,true
            true,WebServer,allow,tcp,80,123.45.67.89,any,any,false


        .EXAMPLE
           Add-vCDedgeFWrule -orgVDC GB-DJH-SKJ-A1 -edge SKJ-A-ESG1 -name "Allow all outbound" -proto "any" -src "internal" -dest "external" -server vcloud.isp.com
            - connects to the Edge called "SKJ-A-ESG1" in the OrgVDC "GB-DJH-SKJ-A1", on the vCD server "vcloud.isp.com"
            - creates a rule (enabled) called "Allow All Outbound"
            - which allows any protocol from an "internal" interface, going to an "external" interface
            - on any source or destination port
            - and does not log the matches

        .EXAMPLE
           Add-vCDedgeFWrule -orgVDC GB-DJH-SKJ-A1 -edge SKJ-A-ESG1 -name "Allow SSH to MGMT from HQ" -src "88.96.93.160/28" -dest "185.39.247.103" -destprt "22" -log "true" -server vcloud.isp.com
            - connects to the Edge called "SKJ-A-ESG1" in the OrgVDC "GB-DJH-SKJ-A1", on the vCD server "vcloud.isp.com"
            - creates a rule (enabled) called "Allow SSH to MGMT from HQ"
            - which allows SSH traffic from a /28 network, going to a specific IP
            - from any source port
            - and logs the matches

        .EXAMPLE
           Add-vCDedgeFWrule -orgVDC GB-DJH-SKJ-A1 -edge SKJ-A-ESG1 -name "drop 10 ports from A to B" -action drop -src "10.0.0.1" -dest "192.168.0.1" -destprt "1024-1033" -server vcloud.isp.com
            - connects to the Edge called "SKJ-A-ESG1" in the OrgVDC "GB-DJH-SKJ-A1", on the vCD server "vcloud.isp.com"
            - creates a rule (enabled) called "drop 10 ports from A to B"
            - which denies traffic from one IP to another
            - from any source port, to a range of destination ports
            - and does not log the matches

        .EXAMPLE
           Add-vCDedgeFWrule -orgVDC GB-DJH-SKJ-A1 -edge SKJ-A-ESG1 -name "Temp Rule" -enabled false -action drop -proto "tcp+udp" -srcprt "80" -dest "192.168.0.1" -server vcloud.isp.com
            - connects to the Edge called "SKJ-A-ESG1" in the OrgVDC "GB-DJH-SKJ-A1", on the vCD server "vcloud.isp.com"
            - creates a rule (disabled) called "Temp Rule"
            - which if enabled, would drop "tcp & udp" traffic from anywhere to 192.168.0.1
            - from the source port "80", to any destination ports
            - and would not not log the matches

        .EXAMPLE
           Add-vCDedgeFWrule -orgVDC GB-DJH-SKJ-A1 -edge SKJ-A-ESG1 -name "We Like to Ping" -proto "icmp" -dest "external" -server vcloud.isp.com
            - connects to the Edge called "SKJ-A-ESG1" in the OrgVDC "GB-DJH-SKJ-A1", on the vCD server "vcloud.isp.com"
            - creates a rule (enabled) called "We Like to Ping"
            - which allows all icmp traffic to an external destination
            - and would not not log the matches

        .EXAMPLE
           Add-vCDedgeFWrule -orgVDC GB-DJH-SKJ-A1 -edge SKJ-A-ESG1 -name "ICMP FTW" -proto "icmp" -server you.losethe.game
            - connects to the Edge called "SKJ-A-ESG1" in the OrgVDC "GB-DJH-SKJ-A1", on the vCD server "you.losethe.game"
            - creates a rule (enabled) called "ICMP FTW"
            - which allows all icmp traffic to anywhere
            - and would not not log the matches

    #>

    # Allow advanced parameter functions
    [CmdletBinding()]

    # Define the Parameters to pass the to function
    Param(

    [Parameter(Mandatory=$true)]
    [ValidateScript({get-orgvdc $_})]
    [String]$orgVDC,

    [Parameter(Mandatory=$true)]
    [String]$edge,

    [Parameter(Mandatory=$false,
               ParameterSetName='SingleEntry')]
    [ValidateSet("true","false")]
    [string]$enabled="true",

    [Parameter(Mandatory=$true,
               ParameterSetName='SingleEntry')]
    [String]$name,

    [Parameter(Mandatory=$false,
               ParameterSetName='SingleEntry')]
    [ValidateSet("allow","drop")]
    [string]$action="allow",

    [Parameter(Mandatory=$false,
               ParameterSetName='SingleEntry')]
    [ValidateSet("tcp","udp","tcp+udp","icmp","any")]
    [String]$proto="tcp",

    [Parameter(Mandatory=$false,
               ParameterSetName='SingleEntry')]
    [String]$destprt="any",

    [Parameter(Mandatory=$false,
               ParameterSetName='SingleEntry')]
    [String]$dest="any",

    [Parameter(Mandatory=$false,
               ParameterSetName='SingleEntry')]
    [String]$srcprt="any",

    [Parameter(Mandatory=$false,
               ParameterSetName='SingleEntry')]
    [String]$src="any",

    [Parameter(Mandatory=$false,
               ParameterSetName='SingleEntry')]
    [ValidateSet("true","false")]
    [string]$log="false",

    [Parameter(Mandatory=$true,
               ParameterSetName='CSVEntry')]
    [ValidateScript({test-path $_})]
    [string]$import,

    [Parameter(Mandatory=$true)]
    [string]$server,

    # Hidden options for debugging
    [Parameter(DontShow)]
    [switch]$showXML,

    [Parameter(DontShow)]
    [switch]$quiet,

    [Parameter(DontShow)]
    [switch]$showHeader
    )

    # Hidden option for Header output
    if ($PSBoundParameters.ContainsKey('showHeader'))
    {
        # Display the headers (from connect-ciREST)
        write-Host "`nDisplaying headers..."
        $ciHeaders
    }

    # Make a ciRest connection if no existing headers detected
    if (!$ciHeaders)
    {
        connect-ciREST -server $server
    }
    else
    {
        if (!$PSBoundParameters.ContainsKey('quiet'))
        {
            Write-Host "`nUsing existing vCloud Rest connection..."
        }
    }

    # Check if the edge you want to convert is an advanced edge
    if (!$PSBoundParameters.ContainsKey('quiet'))
    {
        write-host "Checking for Advanced Services status on ESG : " -NoNewline
        Write-Host $edge -ForegroundColor Yellow
    }
    $advancedEdge = Test-vCDedgeAdvanced -orgVDC $orgVDC -edge $edge -server $server -quiet

    # If it is, then decline to do any work then break
    if ($advancedEdge -eq $true)
    {
        write-host "Edge Services Gateway is running Advanced Networking, aborting."
        break
    }

    # Grab the OrgVDC supplied and pull down the configuration for the Gateway contained within
    if (!$PSBoundParameters.ContainsKey('quiet'))
    {
        Write-Host "Collecting data from vCloud Director..."
    }

    # The admin URL for the supplied object needs changing, bit dirty to use string work here, but hey. Who's looking?
    $orgVDCadminURL = (get-orgvdc $orgVDC).href
    $orgVDCid = ($orgVDCadminURL.split('/'))[-1]
    $orgVDCurl = "https://$($server)/api/vdc/$($orgVDCid)"

    # Grab the list of Edges in the OrgVDC
    $response = Invoke-RestMethod -Method Get -Uri $orgVDCurl -Headers $ciHeaders
    $edgeGatewayURL = ($response.Vdc.Link | Where-Object {$_.rel -eq "edgeGateways"}).href
    $response = Invoke-RestMethod -Method Get -Uri $edgeGatewayURL -Headers $ciHeaders

    # Check for the Edge with a matching name to the supplied string
    $edgeRecord = $response.QueryResultRecords.EdgeGatewayRecord | Where-Object {$_.name -eq "$edge"}

    # If no Edge found matching the supplied name, or if more than one supplied (vCD should prevent this) break out
    if (@($edgeRecord).count -ne 1){write-output "No Edge detected using supplied name `"$($edge)`", or more than one returned (should be impossible!)";break}
    if (!$PSBoundParameters.ContainsKey('quiet'))
    {
        write-host "Collecting Edge Gateway configuration..."
        done
    }

    # Grab the configuration from the edge
    $response = Invoke-RestMethod -Method Get -Uri $edgeRecord.href -Headers $ciHeaders
    if (!$PSBoundParameters.ContainsKey('quiet'))
    {
        done
    }

    # Store the configuration XML as a variable
    $edgeConfiguration = $response.InnerXml

    # Create Table Object for candidate rule(s)
    $ruleTable = New-Object system.Data.DataTable "ruleTable"

    # Define Columns for ruleTable
    $cols = @()
    $cols += New-Object system.Data.DataColumn Name,([string])
    $cols += New-Object system.Data.DataColumn Enabled,([string])
    $cols += New-Object system.Data.DataColumn Action,([string])
    $cols += New-Object system.Data.DataColumn Log,([string])
    $cols += New-Object system.Data.DataColumn Proto,([string])
    $cols += New-Object system.Data.DataColumn Dest,([string])
    $cols += New-Object system.Data.DataColumn DestPrt,([string])
    $cols += New-Object system.Data.DataColumn Src,([string])
    $cols += New-Object system.Data.DataColumn SrcPrt,([string])

    # Add the Columns
    foreach ($col in $cols) {$ruleTable.columns.add($col)}

    # If the -import option was specified, grab the CSV file now
    if ($PSBoundParameters.ContainsKey('import'))
    {
        # Import the specified CSV file
        if (!$PSBoundParameters.ContainsKey('quiet'))
        {
            write-Host "Importing CSV configuration file..."
        }
        $importedCSV = Import-Csv -Path $import

        # Check the import got at least one rule
        if (@($importedCSV).count -lt 1)
        {
            Write-Warning "No data imported from the CSV file"
            break
        }
        if (!$PSBoundParameters.ContainsKey('quiet'))
        {
            done

            # Output progress
            Write-Host "$($importedCSV.count)" -ForegroundColor Yellow -NoNewline
            Write-Host " items found in CSV, building tables..."
        }

        # Populate the table with the CSV items, could have used the existing table, but this way will strip out anything extra and enforce the same schema
        foreach ($rule in $importedCSV)
        {
            $row = $ruleTable.NewRow()

            # Enter data in the row
            $row.Name = $rule.name
            $row.Enabled = $rule.enabled
            $row.Action = $rule.action
            $row.Log = $rule.log
            $row.Proto = $rule.proto
            $row.Dest = $rule.dest
            $row.DestPrt = $rule.destprt
            $row.Src = $rule.src
            $row.SrcPrt = $rule.srcprt

            # Add the row to the table
            $ruleTable.Rows.Add($row)
        }
    }
    else
    # Populate the Table with the rule items specified as parameters
    {
        if (!$PSBoundParameters.ContainsKey('quiet'))
        {
            Write-Host "Populating data from parameter input..."
        }

        # Populate a row in the Rules Table
        $row = $ruleTable.NewRow()

        # Enter data in the row
        $row.Name = $name
        $row.Enabled = $enabled
        $row.Action = $action
        $row.Log = $log
        $row.Proto = $proto
        $row.Dest = $dest
        $row.DestPrt = $destprt
        $row.Src = $src
        $row.SrcPrt = $srcprt

        # Add the row to the table
        $ruleTable.Rows.Add($row)
        if (!$PSBoundParameters.ContainsKey('quiet'))
        {
            done
        }
    }

    # Start the loop to process all rule items
    # -----------------------------------------------------------------------------------------
    # Check that the input is in the correct format and parse for errors/omissions - param validation won't work for imported CSV
    if (!$PSBoundParameters.ContainsKey('quiet'))
    {
        Write-Host "Checking for invalid entries..."
    }
    $errorsDetected = $false
    foreach ($rule in $ruleTable)
    {
        # Check for correct wording of "enabled" and "log" column values
        $boolVerify = "(^true$|^false$)"
        if ($rule.enabled -notmatch $boolVerify)
        {
            Write-Warning "Rule: `"$($rule.name)`" Issue: Bad `"Enabled`", review 'get-help Add-vcdEdgeFWrule'"
            $errorsDetected = $true
        }

        if ($rule.log -notmatch $boolVerify)
        {
            Write-Warning "Rule: `"$($rule.name)`" Issue: Bad `"Log`", review 'get-help Add-vcdEdgeFWrule'"
            $errorsDetected = $true
        }

        # Change the case of the booleans to "true" or "false" to prevent any API errors
        if ($rule.enabled -match "TRUE"){$rule.enabled = "true"}
        if ($rule.enabled -match "FALSE"){$rule.enabled = "false"}
        if ($rule.log -match "TRUE"){$rule.log = "true"}
        if ($rule.log -match "FALSE"){$rule.log = "false"}

        # Check for correct wording of "action" column value
        $actionVerify = "(^allow$|^drop$)"
        if ($rule.action -notmatch $actionVerify)
        {
            Write-Warning "Rule: `"$($rule.name)`" Issue: Bad `"Action`", review 'get-help Add-vcdEdgeFWrule'"
            $errorsDetected = $true
        }

        # Check for correct wording of "proto" column value
        $protoVerify = "(^tcp$|^udp$|^icmp$|^any$|^tcp.+udp$)"
        if ($rule.proto -notmatch $protoVerify)
        {
            Write-Warning "Rule: `"$($rule.name)`" Issue: Bad `"Proto`", review 'get-help Add-vcdEdgeFWrule'"
            $errorsDetected = $true
        }

        # Check for correct wording of "destprt / srcprt" column values
        $prtVerify = "(^any$|^([0-9]{1,4}|[1-5][0-9]{4}|6[0-4][0-9]{3}|65[0-4][0-9]{2}|655[0-2][0-9]|6553[0-5])$|^([0-9]{1,4}|[1-5][0-9]{4}|6[0-4][0-9]{3}|65[0-4][0-9]{2}|655[0-2][0-9]|6553[0-5])`-([0-9]{1,4}|[1-5][0-9]{4}|6[0-4][0-9]{3}|65[0-4][0-9]{2}|655[0-2][0-9]|6553[0-5])$)"
        if ($rule.destprt -notmatch $prtVerify)
        {
            Write-Warning "Rule: `"$($rule.name)`" Issue: Bad `"DestPrt`", review 'get-help Add-vcdEdgeFWrule'"
            $errorsDetected = $true
        }

        if ($rule.srcprt -notmatch $prtVerify)
        {
            Write-Warning "Rule: `"$($rule.name)`" Issue: Bad `"SrcPrt`", review 'get-help Add-vcdEdgeFWrule'"
            $errorsDetected = $true
        }

        # Check for correct wording of "dest / src" column values
        $srcdstVerify = "(^internal$|^external$|^any$|^(([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])`.){3}([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])$|^(([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])`.){3}([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])`-(([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])`.){3}([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])$|^(([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])`.){3}([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])`/(([8-9]|[12][0-9]|3[01]))$)"
        if ($rule.dest -notmatch $srcdstVerify)
        {
            Write-Warning "Rule: `"$($rule.name)`" Issue: `"Bad Dest`", review 'get-help Add-vcdEdgeFWrule'"
            $errorsDetected = $true
        }

        if ($rule.src -notmatch $srcdstVerify)
        {
            Write-Warning "Rule: `"$($rule.name)`" Issue: `"Bad Src`", review 'get-help Add-vcdEdgeFWrule'"
            $errorsDetected = $true
        }

        # Detect if BOTH "any" OR "icmp" proto, AND source/destination ports are used - this is invalid
        if($rule.proto -eq "any" -or $rule.proto -eq "icmp")
        {
            if ($rule.srcprt -ne "any" -or $rule.destprt -ne "any")
            {
                Write-Warning "Rule: `"$($rule.name)`" Issue: Cannot specify both `"$($rule.proto)`" and src/dest prts"
                $errorsDetected = $true
            }
        }
    }

    # Check for errors and prompt to break
    if ($errorsDetected -eq $true)
    {
        write-host "Errors detected in input, I recommend you stop here, fix them and try again." -ForegroundColor Red -BackgroundColor Black
        confirm-continue
        if ($break)
        {
            break
        }
    }

    if (!$PSBoundParameters.ContainsKey('quiet'))
    {
        done
        # Output the proposed rules

        Write-Host "Proposed rulebase as follows..."
        $ruleTable | Format-Table
        confirm-continue
        if ($break)
        {
            write-warning "Exiting at request"
            break
        }
    }

    # Clone the original XML
    $originalEdgeConfig = $edgeConfiguration

    foreach ($rule in $ruleTable)
    {
        # Clear the AnyPort and IcmpSubType Vars
        $anyport = $null
        $icmpSubType = $null

        # Handle the Protocol choice properly
        $protocol = switch ($rule.proto)
        {
            tcp {"<Tcp>true</Tcp>"}
            udp {"<Udp>true</Udp>"}
            tcp+udp {"<Tcp>true</Tcp>`n                        <Udp>true</Udp>"}
            icmp {"<Icmp>true</Icmp>"}
            any {"<Any>true</Any>"}
        }

        # Conditional check for ICMP, it requires an extra <IcmpSubType> field adding
        if($rule.proto -eq "icmp")
        {
            $icmpSubType = "`n                    <IcmpSubType>any</IcmpSubType>"
        }

        # Conditional check for ANY protocol, requires an extra <port> field
        if($rule.proto -eq "any")
        {
            $anyport = "`n                    <Port>-1</Port>"
        }

        # Build out the new configuration from the supplied information, spacing only important if you're going to dump out the generated XML [-showXML]
        $newXML =
@"
                <FirewallRule>
                    <IsEnabled>$($rule.enabled)</IsEnabled>
                    <MatchOnTranslate>false</MatchOnTranslate>
                    <Description>$($rule.name)</Description>
                    <Policy>$($rule.action)</Policy>
                    <Protocols>
                        $($protocol)
                    </Protocols>$($anyPort)$($icmpSubType)
                    <DestinationPortRange>$($rule.destprt)</DestinationPortRange>
                    <DestinationIp>$($rule.dest)</DestinationIp>
                    <SourcePortRange>$($rule.srcprt)</SourcePortRange>
                    <SourceIp>$($rule.src)</SourceIp>
                    <EnableLogging>$($rule.log)</EnableLogging>
                </FirewallRule>`n
"@
        # Add this config to the previous loops
        $ruleXML += $newXML

        # -----------------------------------------------------------------------------------------
    }

    # Close the new candidate config off
    $ruleXML += '            </FirewallService>'

    # Replace </FirewallService> in the EdgeConfiguration with the code above
    $edgeConfiguration = $originalEdgeConfig.Replace('            </FirewallService>',$ruleXML)

    # Hidden option for XML output
    if ($PSBoundParameters.ContainsKey('showXML'))
    {
        # Display the new candidate config
        write-Host "`nDisplaying candidate XML config..."
        $edgeConfiguration
    }

    # Submit the new configuration as a PUT request, remembering to set the correct content type for submission
    $ciHeaders.Add("Content-Type","application/vnd.vmware.admin.edgeGateway+xml")
    $response = Invoke-RestMethod -Method Put -Uri $edgeRecord.href -Headers $ciHeaders -Body $edgeConfiguration
    $ciHeaders.Remove('Content-Type')

    # Check for task status, wait for completion
    $url = $response.Task.href
    Test-TaskStatus -wait -for success -url $url -mins 1 -quiet

    # Check for task success
    # Return success/fail message too before closing up this function

}

function Get-vCDedgeNATrule()
{
    <#
        .SYNOPSIS
            version 0.1 - Written by Dave Hocking 2017.
            This function will display NSX Edge based NAT rules, using the vCloud API
            Requires PowerCLI Support and an existing connection to the vCloud Server (connect-ciserver)
            The script makes a connection to vCD via the RestAPI via the function "connect-ciRest" from this same module file, using the supplied $server
            Optionally, you can specify -exportPath to save a CSV for further use - The Parent Directory must exist.

        .DESCRIPTION
            Gathers details for deployed NAT rules
            Written and tested against vCD 8.10 - will need testing for validity against 8.20's new API set.

            -------------------------------------------------------------------------------------------------------------------

            Collects the following information
            - OrgVDC Name (e.g. "GB-DJH-SKJ-A1") [ string ] MANDATORY
            - Edge Gateway Name (e.g. "SKJ-A-ESG1") [ string ] MANDATORY
            - vCloud Server Name (e.g. "vcloud.isp.com") [ string ] MANDATORY

            ...then displays the data in a table, with the following columns:
              Description = [string]
              enabled = [true|false]
              type = [snat|dnat]
              iface = [string]
              originalIp = [10.20.30.40|10.20.30.40-10.20.30.50|10.20.30.0/24]
              translatedIp = [10.20.30.40|10.20.30.40-10.20.30.50|10.20.30.0/24]
              originalPort = [1-65535]
              translatedPort = [1-65535]
              protocol = [tcp|udp|tcp+udp|icmp|any]
              icmpType = address-mask-request
                         address-mask-reply
                         destination-unreachable
                         echo-request
                         echo-reply
                         parameter-problem
                         redirect
                         router-advertisement
                         router-solicitation
                         source-quench
                         time-exceeded
                         timestamp-request
                         timestamp-reply

            Hidden parameters (switches) exist for -showXML and -showHeader to assist with any debugging that may be needed.

            -------------------------------------------------------------------------------------------------------------------

            RELATED COMMANDS
            Add-vCDEdgeNATrule

        .EXAMPLE
           Get-vCDedgeNATrule -orgVDC GB-DJH-SKJ-A1 -edge SKJ-A-ESG1 -server "vcloud.isp.com"
            - connects to the Edge called "SKJ-A-ESG1" in the OrgVDC "GB-DJH-SKJ-A1", on the vCD server vcloud.isp.com
            - displays a table of collected NAT rules, using the following headings
            Description,enabled,type,iface,originalIp,originalPort,translatedIp,translatedPort,protocol,icmpType

        .EXAMPLE
           Get-vCDedgeNATrule -orgVDC GB-DJH-SKJ-A1 -edge SKJ-A-ESG1 -server vcloud.isp.com -exportPath c:\temp\export.csv
            - connects to the Edge called "SKJ-A-ESG1" in the OrgVDC "GB-DJH-SKJ-A1", on the vCD server "vcloud.isp.com"
            - displays a table of collected NAT rules, using the following headings
            Description,enabled,type,iface,originalIp,originalPort,translatedIp,translatedPort,protocol,icmpType
            - saves the tabular output to a CSV file specified by the -exportPath switch (The C:\temp folder must exist!)

    #>


    # Allow advanced parameter functions
    [CmdletBinding()]

    # Define the Parameters to pass the to function
    Param(

    [Parameter(Mandatory=$true)]
    [ValidateScript({get-orgvdc $_})]
    [String]$orgVDC,

    [Parameter(Mandatory=$true)]
    [String]$edge,

    [Parameter(Mandatory=$true)]
    [string]$server,

    [Parameter(Mandatory=$false)]
    #[ValidateScript({test-path (Split-Path $exportPath)})]
    [string]$exportPath,

    # Hidden options for debugging
    [Parameter(DontShow)]
    [switch]$showXML,

    [Parameter(DontShow)]
    [switch]$showHeader
    )

    # Hidden option for Header output
    if ($PSBoundParameters.ContainsKey('showHeader'))
    {
        # Display the headers (from connect-ciREST)
        write-Host "`nDisplaying headers..."
        $ciHeaders
    }

    # Make a ciRest connection if no existing headers detected
    if (!$ciHeaders)
    {
        connect-ciREST -server $server
    }
    else
    {
        Write-Host "`nUsing existing vCloud Rest connection..."
    }

    # Check if the edge you want to convert is an advanced edge
    if (!$PSBoundParameters.ContainsKey('quiet'))
    {
        write-host "Checking for Advanced Services status on ESG : " -NoNewline
        Write-Host $edge -ForegroundColor Yellow
    }
    $advancedEdge = Test-vCDedgeAdvanced -orgVDC $orgVDC -edge $edge -server $server -quiet

    # If it is, then decline to do any work then break
    if ($advancedEdge -eq $true)
    {
        write-host "Edge Services Gateway is running Advanced Networking, aborting."
        break
    }

    # Grab the OrgVDC supplied and pull down the configuration for the Gateway contained within
    Write-Host "Collecting data from vCloud Director..."

    # The admin URL for the supplied object needs changing, bit dirty to use string work here, but hey. Who's looking?
    $orgVDCadminURL = (get-orgvdc $orgVDC).href
    $orgVDCid = ($orgVDCadminURL.split('/'))[-1]
    $orgVDCurl = "https://$($server)/api/vdc/$($orgVDCid)"

    # Grab the list of Edges in the OrgVDC
    $response = Invoke-RestMethod -Method Get -Uri $orgVDCurl -Headers $ciHeaders
    $edgeGatewayURL = ($response.Vdc.Link | Where-Object {$_.rel -eq "edgeGateways"}).href
    $response = Invoke-RestMethod -Method Get -Uri $edgeGatewayURL -Headers $ciHeaders

    # Check for the Edge with a matching name to the supplied string
    $edgeRecord = $response.QueryResultRecords.EdgeGatewayRecord | Where-Object {$_.name -eq "$edge"}

    # If no Edge found matching the supplied name, or if more than one supplied (vCD should prevent this) break out
    if (@($edgeRecord).count -ne 1){write-output "No Edge detected using supplied name `"$($edge)`", or more than one returned (should be impossible!)";break}
    done

    # Grab the configuration from the edge
    write-host "Collecting Edge Gateway configuration..."
    $response = Invoke-RestMethod -Method Get -Uri $edgeRecord.href -Headers $ciHeaders
    done

    # Store the configuration XML as variables
    $edgeNATConfiguration = $response.EdgeGateway.Configuration.EdgeGatewayServiceConfiguration.NatService

    # Hidden option for XML output
    if ($PSBoundParameters.ContainsKey('showXML'))
    {
        # Display the collected config
        write-Host "`nDisplaying current NAT Service XML config..."
        $edgeNATConfiguration.innerXML
    }

    # Create Table Object for candidate rule(s)
    $ruleTable = New-Object system.Data.DataTable "ruleTable"

    # Define Columns for ruleTable
    $cols = @()
    $cols += New-Object system.Data.DataColumn Description,([string])
    $cols += New-Object system.Data.DataColumn Enabled,([string])
    $cols += New-Object system.Data.DataColumn Type,([string])
    $cols += New-Object system.Data.DataColumn iFace,([string])
    $cols += New-Object system.Data.DataColumn OriginalIp,([string])
    $cols += New-Object system.Data.DataColumn OriginalPort,([string])
    $cols += New-Object system.Data.DataColumn TranslatedIp,([string])
    $cols += New-Object system.Data.DataColumn TranslatedPort,([string])
    $cols += New-Object system.Data.DataColumn Protocol,([string])
    $cols += New-Object system.Data.DataColumn ICMPtype,([string])

    # Add the Columns
    foreach ($col in $cols) {$ruleTable.columns.add($col)}

    # Parse the $EdgeConfiguration file and add each entry to the table
    Write-Host "Populating table from configuration export..."
    foreach ($rule in $edgeNATConfiguration.NATRule)
    {
        # Populate a row in the Rules Table
        $row = $ruleTable.NewRow()

        # Enter data in the row
        $row.Description = $rule.Description
        $row.Enabled = $rule.IsEnabled
        $row.Type = $rule.RuleType
        $row.iFace = $rule.GatewayNatRule.Interface.name
        $row.originalIp = $rule.GatewayNatRule.OriginalIp
        $row.translatedIp = $rule.GatewayNatRule.TranslatedIp

        # Translated Ports only valid for DNAT
        if ($rule.RuleType -eq "SNAT")
        {
            $portDesc = "-na-"
            $row.originalPort = $portDesc
            $row.translatedPort = $portDesc
        }
        else
        {
            $row.originalPort = $rule.GatewayNatRule.OriginalPort
            $row.translatedPort = $rule.GatewayNatRule.TranslatedPort
        }

        # Protocol needs handling differently - trying to get consistent protocol names with the FW rule functions
        if ($rule.GatewayNatRule.Protocol -eq "tcpudp"){$protocol = "tcp+udp"}
        if ($rule.GatewayNatRule.Protocol -eq "udp"){$protocol = "udp"}
        if ($rule.GatewayNatRule.Protocol -eq "tcp"){$protocol = "tcp"}
        if ($rule.GatewayNatRule.Protocol -eq "any"){$protocol = "any"}
        if ($rule.GatewayNatRule.Protocol -eq "icmp"){$protocol = "icmp"}

        $row.Protocol = $protocol
        $row.ICMPtype = $rule.GatewayNatRule.IcmpSubType


        # Add the row to the table
        $ruleTable.Rows.Add($row)
    }
    done

    # Output the collected rules
    Write-Host "Current rulebase as follows..."
    $ruleTable | Sort-Object {$_.id} | Format-Table
    done

    # Check for exportPath option for CSV export
    if ($PSBoundParameters.ContainsKey('exportPath'))
    {
        # Output the data to file
        write-Host "Exporting NAT Rules to CSV `"" -NoNewline
        write-host "$($exportPath)" -NoNewline -ForegroundColor Yellow
        write-host "`""
        write-host "Existing file will be overwritten, make sure path exists also."
        confirm-continue
        if ($break)
        {
            break
        }

        write-Host "Exporting..."
        $ruleTable | Sort-Object {$_.id} | Export-Csv -Path $($exportPath) -NoTypeInformation
        done
    }


}

function Add-vCDedgeNATrule()
{
    <#
        .SYNOPSIS
            version 0.1 - Written by Dave Hocking 2017.
            This function will create NSX Edge based NAT rules, using the vCloud API, for vCD based rule entry
            Any existing rules will be left alone, this only adds new rules
            Requires PowerCLI Support and an existing connection to the vCloud Server (connect-ciserver)
            The script makes a connection to vCD via the RestAPI via the function "connect-ciRest" from this same module file, using the supplied $server
            Optionally, you can pass a CSV file to this function to allow for more simplified input of multiple records

            N.B. vCloud Director and this script will do nothing to prevent you from adding duplicate NAT rules - you should check in the UI after completion

        .DESCRIPTION
            Gathers details for NAT rule creation, then supplies the XML to vCD for submission.
            Written and tested against vCD 8.10 - will need testing for validity against 8.20's new API set.

            -------------------------------------------------------------------------------------------------------------------

            Collects the following information
            - OrgVDC Name (e.g. "GB-DJH-SKJ-A1") [ string ] MANDATORY
            - Edge Gateway Name (e.g. "SKJ-A-ESG1") [ string ] MANDATORY
            - vCloud Server Name (e.g. "vcloud.isp.com") [ string ] MANDATORY
            - NAT Rule Description [ string ] MANDATORY
            - Rule enabled? [ true | false ] OPTIONAL SWITCH, DEFAULT to "false"
            - NAT Rule Type (e.g. "DNAT") [ string ]  OPTIONAL, DEFAULT to "DNAT"
            - NAT Interface Name (Uplink/External Interface Name) [ string ] MANDATORY

            ... then the below for SNAT
            - Internal (Original) IP/Range [ single-ip | ip-range | cidr-notation ] MANDATORY
            - External (Translated) IP/Range [ single-ip | ip-range | cidr-notation ] MANDATORY

            ... then some of the below for DNAT
            - Protocol/s [ tcp | udp | tcp+udp | icmp | any ] OPTIONAL, DEFAULT TO "tcp"
            - External (Original) IP/Range [ single-ip | ip-range | cidr-notation ] MANDATORY
            - Internal (Translated) IP/Range [ single-ip | ip-range | cidr-notation ] MANDATORY
            - Original Port [ single-integer,1-65535 | any ] OPTIONAL, DEFAULT TO "any", also for ICMP protocol choice
            - Translated Port [ single-integer,1-65535 | any ] OPTIONAL, DEFAULT TO "any", also for ICMP protocol choice
            - ICMP Type [ String options Below ] OPTIONAL, only valid with ICMP protocol, defaults to "Any"
                         address-mask-request
                         address-mask-reply
                         destination-unreachable
                         echo-request
                         echo-reply
                         parameter-problem
                         redirect
                         router-advertisement
                         router-solicitation
                         source-quench
                         time-exceeded
                         timestamp-request
                         timestamp-reply
                         any
            ...or the same fields, as specified in a CSV file that can be specified with:
            -import rulebase.csv [ Local File, must exist ] MANDATORY

            When using the CSV import option, the CSV file should contain the following columns:
              type,enabled,description,iface,protocol,icmptype,originalIP,originalPort,translatedIP,translatedPort

            -------------------------------------------------------------------------------------------------------------------

            NB: Original and Translated Ports will only accept "Any" or a single port number 1-65535

            The request will get the latest copy of the ESG NAT rules and place a new rule at the end
            Duplicate records are allowed and will not be detected
            The new rule will contain the information submitted above, and the ESG will be immediately reconfigured
            The task to reconfigure the NAT service MUST be allowed to complete (success or failure) before this function returns.
            Should a collection of changes be needed, the import function should be used.

            Hidden parameters (switches) exist for -showXML and -showHeader to assist with any debugging that may be needed.

            -------------------------------------------------------------------------------------------------------------------

            RELATED COMMANDS
            Get-vCDEdgeNATRule

        .EXAMPLE
           Add-vCDedgeNATrule -orgVDC GB-DJH-SKJ-A1 -edge SKJ-A-ESG1 -import "myRules.csv" -server "vcloud.isp.com"
            - connects to the Edge called "SKJ-A-ESG1" in the OrgVDC "GB-DJH-SKJ-A1", on the vCD server vcloud.isp.com
            - imports the CSV file called "myRules.csv"
            - processes all rules in the CSV as one submission to the vCD server
            - the CSV file must take the following form
            Description,enabled,type,iface,originalIp,originalPort,translatedIp,translatedPort,protocol,icmpType
            Web Access,true,SNAT,External,192.168.0.0/24,,212.12.21.12,,any,
            Web Server,true,DNAT,External,212.12.21.12,443,192.168.0.10,443,tcp,
            Ping Host,true,DNAT,External,212.12.21.12,,192.168.0.1,,icmp,echo-request


        .EXAMPLE
           Add-vCDedgeNATrule -orgVDC GB-DJH-SKJ-A1 -edge SKJ-A-ESG1 -Description "Outbound" -type SNAT -iFace "External" -Protocol "any" -OriginalIP "192.168.0.0/24" -TranslatedIP "212.12.21.12" -server vcloud.isp.com
            - connects to the Edge called "SKJ-A-ESG1" in the OrgVDC "GB-DJH-SKJ-A1", on the vCD server "vcloud.isp.com"
            - creates a SNAT rule (enabled) with a description of "Outbound"
            - which masquerades any traffic, on any protocol from the 192.168.0.0/24 network
            - as the external IP "212.12.21.12" as the traffic passed over the "External" interface
    #>

        # Allow advanced parameter functions
    [CmdletBinding()]

    # Define the Parameters to pass the to function
    Param(

    [Parameter(Mandatory=$true)]
    [ValidateScript({get-orgvdc $_})]
    [String]$orgVDC
    ,
    [Parameter(Mandatory=$true)]
    [String]$edge
    ,
    [Parameter(Mandatory=$false,ParameterSetName='SingleEntry')]
    [ValidateSet("true","false")]
    [string]$enabled="false"
    ,
    [Parameter(Mandatory=$true,ParameterSetName='SingleEntry')]
    [String]$description
    ,
    [Parameter(Mandatory=$false,ParameterSetName='SingleEntry')]
    [string]$iface
    ,
    [Parameter(Mandatory=$false,ParameterSetName='SingleEntry')]
    [ValidateSet("DNAT","SNAT")]
    [string]$type="DNAT"
    ,
    [Parameter(Mandatory=$true,ParameterSetName='SingleEntry')]
    [String]$translatedIP
    ,
    [Parameter(Mandatory=$true,ParameterSetName='SingleEntry')]
    [String]$originalIP
    ,
    [Parameter(Mandatory=$false,ParameterSetName='SingleEntry')]
    [ValidateSet("tcp","udp","tcp+udp","icmp","any")]
    [String]$protocol="tcp"
    ,
    [Parameter(Mandatory=$false,ParameterSetName='SingleEntry')]
    [String]$originalPort="any"
    ,
    [Parameter(Mandatory=$false,ParameterSetName='SingleEntry')]
    [String]$translatedPort="any"
    ,
    [Parameter(Mandatory=$false,ParameterSetName='SingleEntry')]
    [ValidateSet("address-mask-request","address-mask-reply","destination-unreachable","echo-request","echo-reply","parameter-problem","redirect","router-advertisement","router-solicitation","source-quench","time-exceeded","timestamp-request","timestamp-reply","any")]
    [String]$icmpType="any"
    ,
    [Parameter(Mandatory=$true,ParameterSetName='CSVEntry')]
    [ValidateScript({test-path $_})]
    [string]$import
    ,
    [Parameter(Mandatory=$true)]
    [string]$server
    ,
    # Hidden options for debugging
    [Parameter(DontShow)]
    [switch]$showXML
    ,
    [Parameter(DontShow)]
    [switch]$quiet
    ,
    [Parameter(DontShow)]
    [switch]$showHeader
    )

    # Hidden option for Header output
    if ($PSBoundParameters.ContainsKey('showHeader'))
    {
        # Display the headers (from connect-ciREST)
        write-Host "`nDisplaying headers..."
        $ciHeaders
    }

    # Make a ciRest connection if no existing headers detected
    if (!$ciHeaders)
    {
        connect-ciREST -server $server
    }
    else
    {
        if (!$PSBoundParameters.ContainsKey('quiet'))
        {
            Write-Host "`nUsing existing vCloud Rest connection..."
        }
    }

    # Check if the edge you want to convert is an advanced edge
    if (!$PSBoundParameters.ContainsKey('quiet'))
    {
        write-host "Checking for Advanced Services status on ESG : " -NoNewline
        Write-Host $edge -ForegroundColor Yellow
    }
    $advancedEdge = Test-vCDedgeAdvanced -orgVDC $orgVDC -edge $edge -server $server -quiet

    # If it is, then decline to do any work then break
    if ($advancedEdge -eq $true)
    {
        write-host "Edge Services Gateway is running Advanced Networking, aborting."
        break
    }

    # Grab the OrgVDC supplied and pull down the configuration for the Gateway contained within
    if (!$PSBoundParameters.ContainsKey('quiet'))
    {
        Write-Host "Collecting data from vCloud Director..."
    }

    # The admin URL for the supplied object needs changing, bit dirty to use string work here, but hey. Who's looking?
    $orgVDCadminURL = (get-orgvdc $orgVDC).href
    $orgVDCid = ($orgVDCadminURL.split('/'))[-1]
    $orgVDCurl = "https://$($server)/api/vdc/$($orgVDCid)"

    # Grab the list of Edges in the OrgVDC
    $response = Invoke-RestMethod -Method Get -Uri $orgVDCurl -Headers $ciHeaders
    $edgeGatewayURL = ($response.Vdc.Link | Where-Object {$_.rel -eq "edgeGateways"}).href
    $response = Invoke-RestMethod -Method Get -Uri $edgeGatewayURL -Headers $ciHeaders

    # Check for the Edge with a matching name to the supplied string
    $edgeRecord = $response.QueryResultRecords.EdgeGatewayRecord | Where-Object {$_.name -eq "$edge"}

    # If no Edge found matching the supplied name, or if more than one supplied (vCD should prevent this) break out
    if (@($edgeRecord).count -ne 1){write-output "No Edge detected using supplied name `"$($edge)`", or more than one returned (should be impossible!)";break}
    if (!$PSBoundParameters.ContainsKey('quiet'))
    {
        done
        write-host "Collecting Edge Gateway configuration..."
    }

    # Grab the configuration from the edge
    $response = Invoke-RestMethod -Method Get -Uri $edgeRecord.href -Headers $ciHeaders
    if (!$PSBoundParameters.ContainsKey('quiet'))
    {
        done
    }

    # Store the configuration XML as a variable
    $edgeConfiguration = $response

    # Create Table Object for candidate rule(s)
    $ruleTable = New-Object system.Data.DataTable "ruleTable"

    # Define Columns for ruleTable
    $cols = @()
    $cols += New-Object system.Data.DataColumn Enabled,([string])
    $cols += New-Object system.Data.DataColumn Description,([string])
    $cols += New-Object system.Data.DataColumn iFace,([string])
    $cols += New-Object system.Data.DataColumn Type,([string])
    $cols += New-Object system.Data.DataColumn TranslatedIP,([string])
    $cols += New-Object system.Data.DataColumn OriginalIP,([string])
    $cols += New-Object system.Data.DataColumn Protocol,([string])
    $cols += New-Object system.Data.DataColumn OriginalPort,([string])
    $cols += New-Object system.Data.DataColumn TranslatedPort,([string])
    $cols += New-Object system.Data.DataColumn ICMPtype,([string])
    # Creating container for the href for the interface name - gathered later, during input validation phase
    $cols += New-Object system.Data.DataColumn iFaceHref,([string])

    # Add the Columns
    foreach ($col in $cols) {$ruleTable.columns.add($col)}

    # If the -import option was specified, grab the CSV file now
    if ($PSBoundParameters.ContainsKey('import'))
    {
        # Import the specified CSV file
        if (!$PSBoundParameters.ContainsKey('quiet'))
        {
            write-Host "Importing CSV configuration file..."
        }
        $importedCSV = Import-Csv -Path $import

        # Check the import got at least one rule
        if (@($importedCSV).count -lt 1)
        {
            Write-Warning "No data imported from the CSV file"
            break
        }

        if (!$PSBoundParameters.ContainsKey('quiet'))
        {
            done

            # Output progress
            Write-Host "$($importedCSV.count)" -ForegroundColor Yellow -NoNewline
            Write-Host " items found in CSV, building tables..."
        }

        # Populate the table with the CSV items, could have used the existing table, but this way will strip out anything extra and enforce the same schema
        foreach ($rule in $importedCSV)
        {
            $row = $ruleTable.NewRow()

            # Enter data in the row
            $row.enabled = $rule.enabled
            $row.description = $rule.description
            $row.iface = $rule.iface
            $row.type = $rule.type
            $row.translatedIP = $rule.translatedIP
            $row.originalIP = $rule.originalIP
            $row.protocol = $rule.protocol
            $row.originalPort = $rule.originalPort
            $row.translatedPort = $rule.translatedPort
            $row.icmpType = $rule.icmpType

            # Add the row to the table
            $ruleTable.Rows.Add($row)
        }
    }
    else
    # Populate the Table with the rule items specified as parameters
    {
        if (!$PSBoundParameters.ContainsKey('quiet'))
        {
            Write-Host "Populating data from parameter input..."
        }

        # Populate a row in the Rules Table
        $row = $ruleTable.NewRow()

        # Enter data in the row
        $row.enabled = $enabled
        $row.description = $description
        $row.iface = $iface
        $row.type = $type
        $row.translatedIP = $translatedIP
        $row.originalIP = $originalIP
        $row.protocol = $protocol
        $row.originalPort = $originalPort
        $row.translatedPort = $translatedPort
        $row.icmpType = $icmpType

        # Add the row to the table
        $ruleTable.Rows.Add($row)
        if (!$PSBoundParameters.ContainsKey('quiet'))
        {
            done
        }
    }

    # Start the loop to process all rule items
    # -----------------------------------------------------------------------------------------
    if (!$PSBoundParameters.ContainsKey('quiet'))
    {
        Write-Host "You should double-check that the IP Suballocation on the ESG includes the address you want to use on the NAT'ing interface" -ForegroundColor Yellow
        Write-Host "The code needed to check for this automatically hasn't yet been written - if you are wrong, the config will fail with a HTTP:400 'Bad Request' error" -ForegroundColor Yellow
        ""
        # Check that the input is in the correct format and parse for errors/omissions - param validation won't work for imported CSV
        Write-Host "Checking for invalid entries..."
    }

    $errorsDetected = $false
    foreach ($rule in $ruleTable)
    {
        # Check for correct wording of "enabled" column value
        $boolVerify = "(^$|^true$|^false$)"
        if ($rule.enabled -notmatch $boolVerify)
        {
            Write-Warning "Rule: `"$($rule.Description)`" Issue: Bad `"Enabled`", review 'get-help Add-vcdEdgeNATrule'"
            $errorsDetected = $true
        }
        # Handle Blank Enabled field
        if ($rule.enabled -match "^$")
        {
            $rule.enabled = "false"
        }

        # Change the case of the boolean to "true" or "false" to prevent any API errors
        if ($rule.enabled -eq "TRUE"){$rule.enabled = "true"}
        if ($rule.enabled -eq "FALSE"){$rule.enabled = "false"}

        # Check for correct wording of "type" column value
        $typeVerify = "(^$|^dnat$|^snat$)"
        if ($rule.type -notmatch $typeVerify)
        {
            Write-Warning "Rule: `"$($rule.Description)`" Issue: Bad `"Type`", review 'get-help Add-vcdEdgeNATrule'"
            $errorsDetected = $true
        }

        # Handle Blank type field
        if ($rule.type -eq $null)
        {
            $rule.type = "DNAT"
        }

        # Change the case of the type to "DNAT" or "SNAT" to prevent any API errors
        if ($rule.type -eq "dnat"){$rule.type = "DNAT"}
        if ($rule.type -eq "snat"){$rule.type = "SNAT"}

        # Check for correct wording of "OriginalIP and TranslatedIP" column values
        $ipVerify = "(^internal$|^external$|^any$|^(([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])`.){3}([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])$|^(([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])`.){3}([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])`-(([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])`.){3}([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])$|^(([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])`.){3}([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])`/(([8-9]|[12][0-9]|3[01]))$)"
        if ($rule.originalIp -notmatch $ipVerify)
        {
            Write-Warning "Rule: `"$($rule.Description)`" Issue: `"Bad OriginalIP`", review 'get-help Add-vcdEdgeNATrule'"
            $errorsDetected = $true
        }

        if ($rule.translatedIp -notmatch $ipVerify)
        {
            Write-Warning "Rule: `"$($rule.Description)`" Issue: `"Bad TranslatedIP`", review 'get-help Add-vcdEdgeNATrule'"
            $errorsDetected = $true
        }

        # Check for the interface specified in the rule (should be a valid interface, usually set on the uplink)
        if (!($edgeConfiguration.EdgeGateway.Configuration.GatewayInterfaces.GatewayInterface | Where-Object {$_.Name -match $rule.iFace}).network.name)
        {
            Write-Warning "Rule: `"$($rule.Description)`" Issue: `"Bad Interface Name`", check the name of the uplink interface in the vCD UI."
            $errorsDetected = $true
        }

        # Assuming the interface name is correct, populate the href value in the rule table
        else
        {
            $rule.iFaceHref = ($edgeConfiguration.EdgeGateway.Configuration.GatewayInterfaces.GatewayInterface | Where-Object {$_.Name -match $rule.iFace}).network.href
            # Replace the iFace name supplied by the user, with the name on the object, should the user have specified a "search term" rather than full name.
            $rule.iFace = ($edgeConfiguration.EdgeGateway.Configuration.GatewayInterfaces.GatewayInterface | Where-Object {$_.Name -match $rule.iFace}).network.name
        }

        <#


            #########################################################################################


            CHECK FOR INVALID IP SUB ALLOCATION IN HERE


            #########################################################################################



        #>

        # Start branching logic to only check protocol and port numbers for DNAT rule types
        if ($rule.type -eq "DNAT")
        {
            # Check for correct wording of "proto" column value
            $protoVerify = "(^$|^tcp$|^udp$|^icmp$|^any$|^tcp.+udp$)"
            if ($rule.protocol -notmatch $protoVerify)
            {
                Write-Warning "Rule: `"$($rule.Description)`" Issue: Bad `"Protocol`", review 'get-help Add-vcdEdgeNATrule'"
                $errorsDetected = $true
            }
            if ($rule.protocol -match "(^$)")
            {
                $rule.protocol = "tcp"
            }
            # Change the case of the protocol to uppercase to prevent any API errors
            if ($rule.protocol -eq "TCP"){$rule.protocol = "tcp"}
            if ($rule.protocol -eq "UDP"){$rule.protocol = "udp"}
            if ($rule.protocol -eq "ICMP"){$rule.protocol = "icmp"}
            if ($rule.protocol -eq "ANY"){$rule.protocol = "any"}
            if ($rule.protocol -eq "TCP+UDP"){$rule.protocol = "tcp+udp"}

            # Check for correct wording of "OriginalPort / TranslatedPort" column values
            $prtVerify = "(^$|^any$|^([0-9]{1,4}|[1-5][0-9]{4}|6[0-4][0-9]{3}|65[0-4][0-9]{2}|655[0-2][0-9]|6553[0-5])$)"
            if ($rule.originalPort -notmatch $prtVerify)
            {
                Write-Warning "Rule: `"$($rule.Description)`" Issue: Bad `"OriginalPort`", review 'get-help Add-vcdEdgeNATrule'"
                $errorsDetected = $true
            }

            # Handle blank original port
            if ($rule.originalPort -match "^$")
            {
                $rule.originalPort = "any"
            }

            if ($rule.translatedPort -notmatch $prtVerify)
            {
                Write-Warning "Rule: `"$($rule.Description)`" Issue: Bad `"TranslatedPort`", review 'get-help Add-vcdEdgeNATrule'"
                $errorsDetected = $true
            }

            # Handle blank translated port
            if ($rule.translatedPort -match "^$")
            {
                $rule.translatedPort = "any"
            }


            # Detect ICMP protocol choice and check for valid ICMP sub types
            if($rule.protocol -eq "icmp")
            {
                # Wipe out any port numbers for ICMP protocol
                $rule.originalPort = "any"
                $rule.translatedPort = "any"

                # Detect correct range of subtypes
                if ($rule.icmpType -notmatch "(^$|^any$|^address-mask-request$|^address-mask-reply$|^destination-unreachable$|^echo-request$|^echo-reply$|^parameter-problem$|^redirect$|^router-advertisement$|^router-solicitation$|^source-quench$|^time-exceeded$|^timestamp-request$|^timestamp-reply$)")
                {
                    Write-Warning "Rule: `"$($rule.Description)`" Issue: Incorrect ICMP Sub Type `"$($rule.icmpType)`""
                    $errorsDetected = $true
                }
            }

            # If ICMP type isn't specified, handle a default of "any"
            if ($rule.icmpType -match "(^$)")
            {
                $rule.icmpType = "any"
            }
        }

        # For SNAT rules, set the Original and Translated ports to Any, Protocol and ICMP Subtype to Any
        if ($rule.type -eq "SNAT")
        {
            $rule.originalPort = "any"
            $rule.translatedPort = "any"
            $rule.protocol = "any"
            $rule.icmpType = "any"
        }

    }

    # Check for errors and prompt to break
    if ($errorsDetected -eq $true)
    {
        write-host "Errors detected in input, I recommend you stop here, fix them and try again." -ForegroundColor Red -BackgroundColor Black
        confirm-continue
        if ($break)
        {
            break
        }
    }

    if (!$PSBoundParameters.ContainsKey('quiet'))
    {
        done

        # Output the proposed rules
        Write-Host "Proposed rulebase as follows..."
        $ruleTable | Select-Object Description,Enabled,Type,OriginalIp,OriginalPort,TranslatedIp,TranslatedPort,Protocol,IcmpType | Format-Table
        confirm-continue
        if ($break)
        {
            break
        }
    }

    # Clone the original XML as a string
    $originalEdgeConfig = $edgeConfiguration.innerXML

    # Loop through the rules and create the config
    foreach ($rule in $ruleTable)
    {
        # Conditional check for ICMP Protocol, must contain icmpsubtype field
         if($rule.protocol -match "icmp")
        {
            $icmpTypeOptions =
@"
`n                        <IcmpSubType>$($rule.icmptype)</IcmpSubType>
"@
        }

        # Build out the new configuration from the supplied information, spacing only important if you're going to dump out the generated XML [-showXML]
        $newXML =
@"
                <NatRule xmlns="http://www.vmware.com/vcloud/v1.5">
                    <Description>$($rule.description)</Description>
                    <RuleType>$($rule.type)</RuleType>
                    <IsEnabled>$($rule.enabled)</IsEnabled>
                    <GatewayNatRule>
                        <Interface href="$($rule.ifacehref)" name="$($rule.iface)"  type="application/vnd.vmware.admin.network+xml"/>
                        <OriginalIp>$($rule.OriginalIP)</OriginalIp>
                        <OriginalPort>$($rule.originalPort)</OriginalPort>
                        <TranslatedIp>$($rule.TranslatedIP)</TranslatedIp>
                        <TranslatedPort>$($rule.translatedPort)</TranslatedPort>
                        <Protocol>$($rule.protocol)</Protocol>$($icmpTypeOptions)
                    </GatewayNatRule>
                </NatRule>`n
"@

        # Add this config to the previous loops
        $ruleXML += $newXML

        # -----------------------------------------------------------------------------------------
    }

    #>

    # Close the new candidate config off
    $ruleXML += '            </NatService>'

    # Check for the presence of <NatService> in the XML, if it's not present, then create it with defaults, replacing </FirewallService> which will exist
    if (!($originalEdgeConfig.contains('            <NatService>')))
    {
        $blankNatXML =
@"
            </FirewallService>
            <NatService>
                <IsEnabled>true</IsEnabled>
            </NatService>
"@

        if (!$PSBoundParameters.ContainsKey('quiet'))
        {
            Write-Host "No previous NAT configuration detected on this Edge, creating one..."
        }
        $originalEdgeConfig = $originalEdgeConfig.Replace('            </FirewallService>',$blankNatXML)

        if (!$PSBoundParameters.ContainsKey('quiet'))
        {
            done
        }
    }

    # Replace </NatService> in the EdgeConfiguration with the code above
    $edgeConfiguration = $originalEdgeConfig.Replace('            </NatService>',$ruleXML)

    # Hidden option for XML output
    if ($PSBoundParameters.ContainsKey('showXML'))
    {
        # Display the new candidate config
        write-Host "`nDisplaying candidate XML config..."
        $edgeConfiguration
    }

    # Submit the new configuration as a PUT request, remembering to set the correct content type for submission
    $ciHeaders.Add("Content-Type","application/vnd.vmware.admin.edgeGateway+xml")
    $response = Invoke-RestMethod -Method Put -Uri $edgeRecord.href -Headers $ciHeaders -Body $edgeConfiguration
    $ciHeaders.Remove('Content-Type')

    # Check for task status, wait for completion
    $url = $response.Task.href
    Test-TaskStatus -wait -for success -url $url -mins 1 -quiet

    # Check for task success
    # Return success/fail message too before closing up this function

}

#function Remove-vCloudOrg()
{
    <#
        .SYNOPSIS
            version 0.1 - Written by Dave Hocking 2016.
            This function will remove the vCloud Director objects associated with a given Organisation
            Requires PowerCLI Support

        .DESCRIPTION
            Gathers objects for the given vCD Org and removes them in the correct order
            - Stops all CI vApps
            - Removes all media and catalogs
            - Removes all CI vApps
            - Removes all Org Networks
            - Prompts user to removes all vShield Edges (NSX Edge Gateways)
            - Removes any vDCs
            - Removes the Organisation

            THIS IS NOT A PROCESS THAN CAN BE REVERTED - BE SURE YOU ARE OPERATING ON THE RIGHT ORGANISATION

            RELATED COMMANDS
            - Add-vCloudOrg
            - Add-vCloudOrgVDC

        .EXAMPLE
           Remove-vCloudOrg OrganisationName
            - Removes the vCD Organisation "OrganisationName" and all objects
            - THIS CANNOT BE UNDONE!

        .EXAMPLE
           Remove-vCloudOrg OrganisationName -maverick
            - DISPLAYS ONLY ONE CONFIRMATION OPTION - IF YOU HAVE THE WRONG ORG, THIS IS BAD, VERY BAD
            - Removes the vCD Organisation "OrganisationName" and all objects
            - THIS CANNOT BE UNDONE!

    #>

    # Allow advanced parameter functions
    [CmdletBinding()]

    # Define the Parameters to pass the to function
    Param(

    [Parameter(Mandatory=$true)]
    [String]$orgName,
    [Parameter(Mandatory=$false)]
    [Switch]$labs,
    [Parameter(Mandatory=$false)]
    [Switch]$maverick
    )

    #===============#
    # INITIAL SETUP #
    #===============#

    # Import Modules / Functions / Variables
    Import-vCDcommonVars

    # Display company header
    show-header

    # Detect if user has called the -labs switch (or if it's been added from above)
    if ($PSBoundParameters.ContainsKey('labs')) {$ciserver = $ciLabsServer; $viserver = $viLabsServer}

    # Connect to the CI server
    connect-ci

    # Gather objects into variables
    Write-Host "Collecting Objects for vCloud Organisation $orgName..."

    # Get the vCD Organisation
    $org = get-org -Name $orgName -ErrorVariable noConnect
    if ($noConnect)
    {
        write-warning "Error collecting vCD Organisation Details for `"$orgName`""
        break
    }

    # Display prominent warning about the use of this tool
    Write-Warning "YOU ARE GOING TO BE ATTEMPTING TO REMOVE $($orgName)'s DATA ON $($ciserver)"
    Write-Host " _    _  ___  ______ _   _ _____ _   _ _____"  -ForegroundColor Red -BackgroundColor White
    Write-Host "| |  | |/ _ \ | ___ \ \ | |_   _| \ | |  __ \" -ForegroundColor Red -BackgroundColor White
    Write-Host "| |  | / /_\ \| |_/ /  \| | | | |  \| | |  \/" -ForegroundColor Red -BackgroundColor White
    Write-Host "| |/\| |  _  ||    /| .   | | | | .   | | __ " -ForegroundColor Red -BackgroundColor White
    Write-Host "\  /\  / | | || |\ \| |\  |_| |_| |\  | |_\ \" -ForegroundColor Red -BackgroundColor White
    Write-Host " \/  \/\_| |_/\_| \_\_| \_/\___/\_| \_/\____/" -ForegroundColor Red -BackgroundColor White
    Write-Host "MAKE SURE YOU WANT TO DO THIS, THERE IS NO WAY BACK." -ForegroundColor Red -BackgroundColor White
    ""

    # Display a warning before proceeding with maverick mode (if selected)
    if($PSBoundParameters.ContainsKey('maverick'))
    {
        Write-Host "YOU GET AN EXTRA WARNING BECAUSE YOU HAVE SELECTED MAVERICK MODE" -ForegroundColor Red -BackgroundColor White
        Write-Host "If you continue, from here onwards, NO CONFIRMATION WILL BE GIVEN" -ForegroundColor Red -BackgroundColor White
        Write-Host "All objects will be removed and cannot be undone. Do you feel lucky, Punk?" -ForegroundColor Red -BackgroundColor White
        confirm-continue
        if ($break)
        {
            break
        }
        [string]$operatingMode = "Maverick"
    }
    else
    {
        [string]$operatingMode = "Paranoid"
    }

    # Get the Catalogs - Empty Catalogs are permitted with no vDC
    $orgCat = $org | Get-Catalog

    # Get the Org VDCs, all other objects depend on these existing, branch into if statement
    $orgVdcs = $org | Get-OrgVdc
    if ($orgVdcs)
    {
        foreach ($orgVdc in $orgVdcs)
        {

            # Get the Media in the Catalogs
            $orgMedia = $orgCat | Get-Media

            # Get the vApps for the Org
            $ciVapp = Get-CIVApp -Org $org

            # Get the Org Networks for the Org VDC
            #$orgNet = $orgVdc | Get-OrgvdcNetwork
            $orgNet = Get-OrgNetwork -org $org

            # Get the vApp Templates in the Catalogs
            $orgVappTemplates = $org | Get-Catalog | Get-CIVAppTemplate
            done

            # Trash the media in the catalogs, if existing
            if ($orgMedia)
            {
                Write-Host "Organisation Media found:"
                $orgMedia

                # Check for maverick or paranoid operating mode
                if ($operatingMode -eq "Paranoid")
                {
                    write-Host "Delete all media?"
                    confirm-continue

                    if ($break)
                    {
                        write-warning "Exiting script - Disconnecting from VCD"
                        disconnect-ci
                        break
                    }
                }
                elseif ($operatingMode -eq "Maverick")
                {
                        Write-Host "Maverick mode enabled" -ForegroundColor Red -BackgroundColor White
                }

                ($orgMedia | get-ciview).Delete()
                done

            }

            # If Running, you need to stop the vApps First, if existing
            if ($ciVapp)
            {
                Write-Host "Organisation vApps found:"
                $ciVapp | select-object Name,Status

                # Check for maverick or paranoid operating mode
                if ($operatingMode -eq "Paranoid")
                {
                    Write-Host "Stop and delete all vApps?"
                    confirm-continue

                    if ($break)
                    {
                        write-warning "Exiting script - Disconnecting from VCD"
                        disconnect-ci
                        break
                    }
                }
                elseif ($operatingMode -eq "Maverick")
                {
                    Write-Host "Maverick mode enabled" -ForegroundColor Red -BackgroundColor White
                }
                write-host ""
                $runningVapps = $ciVapp | where-object {$_.status -ne 'PoweredOff'}
                if ($runningVapps)
                {
                    Write-Host "Stopping vApps..."
                    foreach ($vapp in $runningVapps)
                    {
                        stop-civapp -vapp $vapp -Confirm:$false
                    }
                    done
                }

                # Deleting the vApps
                Write-Host "Deleting vApps..."
                foreach ($obj in $ciVapp)
                {
                    Remove-CIVApp -VApp $obj -Confirm:$false
                }
                done

            # Sleep for 5 to ensure cleanup is complete
            start-sleep 5
            }

            # Deleting the Organisation Networks, if existing
            if ($orgNet)
            {
                Write-Host "Organisation Networks found:"
                $orgNet | format-table
                # Check for maverick or paranoid operating mode
                if ($operatingMode -eq "Paranoid")
                {
                    Write-Host "Delete all Organisation Networks?"
                    confirm-continue

                    if ($break)
                    {
                        write-warning "Exiting script - Disconnecting from VCD"
                        disconnect-ci
                        break
                    }
                }
                elseif ($operatingMode -eq "Maverick")
                {
                    Write-Host "Maverick mode enabled" -ForegroundColor Red -BackgroundColor White
                }

                Write-Host "Deleting Organisation Networks..."
                Remove-OrgNetwork $orgNet -Confirm:$false
                done
            }

            # Edge Deletion must be done through the API as no cmdlets exist for this
            # Use the connect-ciREST function to connect to vCD via the API
            write-host "Connecting to REST API, Authentication needed."
            connect-ciREST -server $ciserver
            ""

            # Get Orgs
            Write-Host "Getting Org details..."
            $resource = "/org"
            $url = $baseurl + $resource
            $response = Invoke-RestMethod -Uri $url -Headers $ciHeaders -Method GET
            $orgs = $response.OrgList.org

            # Get link for a given OrgName
            $orgURL = ($orgs | where-object {$_.name -match $orgName}).href
            done

            # Get the list of Org VDCs
            Write-Host "Getting OrgVDC List..."
            $response = Invoke-RestMethod -Uri $orgURL -Headers $ciHeaders -Method GET
            done

            $orgVDClist = ($response.Org.Link | where-object {$_.type -eq "application/vnd.vmware.vcloud.vdc+xml"}).href
            foreach ($orgVDCurl in $orgVDClist)
            {
                # Get the list of Edge Gateways
                Write-Host "Getting Edge Gateway List..."
                $response = Invoke-RestMethod -Uri $orgVDCurl -Headers $ciHeaders -Method GET
                $edgeGatewayListURL = ($response.Vdc.Link | where-object {$_.rel -match "edgeGateways"}).href
                $response = Invoke-RestMethod -Uri $edgeGatewayListURL -Headers $ciHeaders -Method GET
                $edgeGatewayList = $response.QueryResultRecords.EdgeGatewayRecord
                done

                # Need a for-each loop to trash all edges
                # Check for maverick or paranoid operating mode
                if ($operatingMode -eq "Paranoid")
                {
                    write-host "Delete all Edge(s)..."
                    confirm-continue

                    if ($break)
                    {
                        write-warning "Exiting script - Disconnecting from VCD"
                        disconnect-ci
                        disconnect-ciREST
                        break
                    }
                }
                elseif ($operatingMode -eq "Maverick")
                {
                    Write-Host "Maverick mode enabled" -ForegroundColor Red -BackgroundColor White
                }

                foreach ($edge in $edgeGatewayList)
                {
                    write-host "$($edge.name)" -ForegroundColor Yellow
                    $response = Invoke-RestMethod -Uri $edge.href -Headers $ciHeaders -Method Delete

                    # Check for task success
                    $url = $response.Task.href
                    Test-TaskStatus -wait -for success -url $url
                }
                done
            }


            # Deleting any vApp Templates, if existing
            if ($orgVappTemplates)
            {
                Write-Host "Organisation vApp Templates found:"
                $orgVappTemplates | format-table
                # Check for maverick or paranoid operating mode
                if ($operatingMode -eq "Paranoid")
                {
                    write-host "Delete all Organisation vApp Templates?"
                    confirm-continue

                    if ($break)
                    {
                        write-warning "Exiting script - Disconnecting from VCD"
                        disconnect-ci
                        disconnect-ciREST
                        break
                    }
                }
                elseif ($operatingMode -eq "Maverick")
                {
                    Write-Host "Maverick mode enabled" -ForegroundColor Red -BackgroundColor White
                }

                Write-Host "Deleting Templates..."
                $orgVappTemplates | Remove-CIVAppTemplate -Confirm:$false
                done
            }

            # Delete any catalogs
            if ($orgCat)
            {
                Write-Host "Organisation Catalogs found:"
                $orgCat.name
                                # Check for maverick or paranoid operating mode
                if ($operatingMode -eq "Paranoid")
                {
                    write-host "Delete all Catalogs?"
                    confirm-continue

                    if ($break)
                    {
                        write-warning "Exiting script - Disconnecting from VCD"
                        disconnect-ci
                        disconnect-ciREST
                        break
                    }
                }
                elseif ($operatingMode -eq "Maverick")
                {
                    Write-Host "Maverick mode enabled" -ForegroundColor Red -BackgroundColor White
                }

                Write-Host "Deleting Catalogs..."
                ($orgCat | Get-CIView).Delete()
                done
            }

            # Deleting the Organisation vDC
            Write-Host "Organisation vDCs: $($orgVdc.name)"
            # Check for maverick or paranoid operating mode
            if ($operatingMode -eq "Paranoid")
            {
                write-host "Delete this vDC?"
                confirm-continue

                if ($break)
                {
                    write-warning "Exiting script - Disconnecting from VCD"
                    disconnect-ci
                    disconnect-ciREST
                    break
                }
            }
            elseif ($operatingMode -eq "Maverick")
            {
                Write-Host "Maverick mode enabled" -ForegroundColor Red -BackgroundColor White
            }

            Write-Host "Deleting Organisation Virtual Data Centre..."
            $orgVdc | Set-OrgVdc -Enabled $false | Remove-OrgVdc -Confirm:$false
            done
        }
    }

    # Deleting the Organisation
    # Check for maverick or paranoid operating mode
    if ($operatingMode -eq "Paranoid")
    {
        write-host "Delete the Organisation?"
        confirm-continue

        if ($break)
        {
            write-warning "Exiting script - Disconnecting from VCD"
            disconnect-ci
            disconnect-ciREST
            break
        }
    }
    elseif ($operatingMode -eq "Maverick")
    {
        Write-Host "Maverick mode enabled" -ForegroundColor Red -BackgroundColor White
    }

    Write-Host "Deleting vCloud Director Organisation..."
    $org | Set-Org -Enabled $false | remove-org -Confirm:$false
    done
    Write-Host "All objects removed." -ForegroundColor green
    ""

    # Tidy up afterwards
    Disconnect-CI
    disconnect-ciREST
}

#function Remove-vCloudOrg()
{
    <#
        .SYNOPSIS
            version 0.2 - Written by Dave Hocking 2016.
            This function will remove the vCloud Director objects associated with a given Organisation
            Requires PowerCLI Support

        .DESCRIPTION
            Gathers objects for the given vCD Org and removes them in the correct order
            - Stops all CI vApps
            - Removes all media and catalogs
            - Removes all CI vApps
            - Removes all Org Networks
            - Removes all vShield Edges (NSX Edge Gateways)
            - Removes any vDCs
            - Removes the Organisation

            THIS IS NOT A PROCESS THAN CAN BE REVERTED - BE SURE YOU ARE OPERATING ON THE RIGHT ORGANISATION

            RELATED COMMANDS
            - Add-vCloudOrg
            - Add-vCloudOrgVDC

        .EXAMPLE
           Remove-vCloudOrg OrganisationName
            - Removes the vCD Organisation "OrganisationName" and all objects
            - THIS CANNOT BE UNDONE!

    #>

    # Allow advanced parameter functions
    [CmdletBinding()]

    # Define the Parameters to pass the to function
    Param(

    [Parameter(Mandatory=$true)]
    [String]$orgName,
    [Parameter(Mandatory=$false)]
    [Switch]$labs
    )

    #===============#
    # INITIAL SETUP #
    #===============#

    # Import Modules / Functions / Variables
    Import-vCDcommonVars

    # Display company header
    show-header

    # Detect if user has called the -labs switch (or if it's been added from above)
    if ($PSBoundParameters.ContainsKey('labs')) {$ciserver = $ciLabsServer; $viserver = $viLabsServer}

    # Connect to the CI server
    connect-ci

    # Create a Hash-table for populating the customers objects
    $assets = @{}

    # Gather objects into variables
    Write-Host "Collecting Objects for vCloud Organisation $orgName..."

    # Get the vCD Organisation
    $org = get-org -Name $orgName -ErrorVariable noConnect
    if ($noConnect)
    {
        write-warning "Error collecting vCD Organisation Details for `"$orgName`""
        break
    }

    # Add the Org to the assets list
    Write-Host "Getting assets... [" -NoNewline
    Write-Host " Org " -NoNewline -ForegroundColor Yellow
    $assets.Add("Organisation","$($org.name)")

    # Display prominent warning about the use of this tool
    Write-Warning "YOU ARE GOING TO BE ATTEMPTING TO REMOVE $($orgName)'s DATA ON $($ciserver)"
    Write-Host " _    _  ___  ______ _   _ _____ _   _ _____"  -ForegroundColor Red -BackgroundColor White
    Write-Host "| |  | |/ _ \ | ___ \ \ | |_   _| \ | |  __ \" -ForegroundColor Red -BackgroundColor White
    Write-Host "| |  | / /_\ \| |_/ /  \| | | | |  \| | |  \/" -ForegroundColor Red -BackgroundColor White
    Write-Host "| |/\| |  _  ||    /| .   | | | | .   | | __ " -ForegroundColor Red -BackgroundColor White
    Write-Host "\  /\  / | | || |\ \| |\  |_| |_| |\  | |_\ \" -ForegroundColor Red -BackgroundColor White
    Write-Host " \/  \/\_| |_/\_| \_\_| \_/\___/\_| \_/\____/" -ForegroundColor Red -BackgroundColor White
    Write-Host "MAKE SURE YOU WANT TO DO THIS, THERE IS NO WAY BACK." -ForegroundColor Red -BackgroundColor White
    ""

    # Get the Catalogs - Empty Catalogs are permitted with no vDC
    $orgCat = $org | Get-Catalog
    if ($orgCat -ne $null)
    {
        Write-Host " Catalogs " -NoNewline -ForegroundColor Yellow
        $assets.Add("Catalog","$($orgCat.name)")
    }

    # Get the Org VDCs, all other objects depend on these existing, branch into if statement
    $orgVdcs = $org | Get-OrgVdc

    if ($orgVdcs)
    {
            # If the Org has a VDC, Get the Media in the Catalogs
            $orgMedia = $orgCat | Get-Media

            # If media exists, add it to the assets list
            if ($orgMedia -ne $null)
            {
                Write-Host " Media " -NoNewline -ForegroundColor Yellow
                # Add the OrgVDC to the assets list
                $assets.Add("Media","$($orgMedia.name)")
            }

            # Get the vApp Templates
            $ciVappTemplates = $org | Get-Catalog | get-civapptemplate

            # If templates exists, add them to the assets list
            if ($ciVappTemplates -ne $null)
            {
                Write-Host " Templates " -NoNewline -ForegroundColor Yellow
                $assets.Add("Templates","$($ciVappTemplate.name)")
            }

            # Add the OrgVDCs to the assets list
            Write-Host " OrgVDCs " -NoNewline -ForegroundColor Yellow
            $assets.Add("OrgVDC","$($orgVdc.name)")

            # Get the vApps for the Org
            $ciVapps = Get-CIVApp -Org $org

            if ($ciVapps -ne $null)
            {
                Write-Host " vApps " -NoNewline -ForegroundColor Yellow
                # Loop through the vApps
                foreach ($ciVapp in $ciVapps)
                {
                    # Add the vApps to the assets list
                    $assets.Add("vApps","$($ciVapp.name)")
                }
            }

            # Get the Org Networks for the Org VDC
            $orgVDCNets = $orgVdc | Get-OrgvdcNetwork

            # Check for OrgVDC Networks and populate the assets list
            if ($orgVDCNets -ne $null)
            {
                Write-Host " Networks " -NoNewline -ForegroundColor Yellow
                # Loop through the Nets
                foreach ($orgVDCNet in $orgVDCNets)
                {
                    # Add the vApps to the assets list
                    $assets.Add("OrgVDC Networks","$($orgVDCNet.name)")
                }
            }

            # Get Edge Gateways from RestAPI
            #Write-Host "Getting Org details..."
            $resource = "/org"
            $url = $baseurl + $resource
            $response = Invoke-RestMethod -Uri $url -Headers $ciHeaders -Method GET
            $orgs = $response.OrgList.org

            # Get link for a given OrgName
            $orgURL = ($orgs | where-object {$_.name -match $orgName}).href
            $orgResponse = Invoke-RestMethod -Uri $orgURL -Headers $ciHeaders -Method GET
            #done

            #Write-Host "Getting OrgVDC details..."
            $orgVdcUrls = ($orgResponse.Org.Link | where-object {$_.type -match "\.vcloud\.vdc\+xml"}).href
            #done

            # Create array to hold hrefs for Edge gateways
            $edgeGatewayURLs = @()

            # Create array to hold IDs for Edge gateways
            $edgeGatewayIDs = @()

            # Create array to hold Names for Edge gateways
            $edgeGatewayNames = @()

            #Write-Host "Checking OrgVDCs for Edge Gateways..."
            # Loop through each vDC and get the links for Edges
            foreach ($orgVdcUrl in $orgVdcUrls)
            {
                # Get orgVDCs
                $orgvdcResponse = Invoke-RestMethod -Uri $orgVdcUrl -Headers $ciHeaders -Method GET
                # Cumulatively add the urls to the array
                $edgeGatewayURLs += ($orgvdcResponse.vdc.link | where-object {$_.rel -match "edgeGateways"}).href
            }
            #done

            # If edge Gateways exist, get their IDs for the delete operation
            if (@($edgeGatewayURLs).Count -gt 0)
            {
                Write-Host " EdgeGateways " -NoNewline -ForegroundColor Yellow
                #Write-Host "Getting Edge Gateway IDs..."
                foreach ($edgeGatewayURL in $edgeGatewayURLs)
                {
                    $edgeResponse = Invoke-RestMethod -Uri $edgeGatewayURL -Headers $ciHeaders -Method GET
                    $edgeGatewayIDs += $edgeResponse.QueryResultRecords.EdgeGatewayRecord.href
                    $edgeGatewayNames += $edgeResponse.QueryResultRecords.EdgeGatewayRecord.name
                }
                # Add the Edges to the assets list
                $assets.Add("Edge Gateways","$($edgeGatewayNames)")
                #done
            }








            # Trash the media in the catalogs, if existing
            if ($orgMedia)
            {
                Write-Host "Organisation Media found:"
                $orgMedia

                # Check for maverick or paranoid operating mode
                if ($operatingMode -eq "Paranoid")
                {
                    write-Host "Delete all media?"
                    confirm-continue

                    if ($break)
                    {
                        write-warning "Exiting script - Disconnecting from VCD"
                        disconnect-ci
                        break
                    }
                }
                elseif ($operatingMode -eq "Maverick")
                {
                        Write-Host "Maverick mode enabled" -ForegroundColor Red -BackgroundColor White
                }

                ($orgMedia | get-ciview).Delete()
                ($orgCat | get-ciview).Delete()
                done

            }

            # If Running, you need to stop the vApps First, if existing
            if ($ciVapp)
            {
                Write-Host "Organisation vApps found:"
                $ciVapp | select-object Name,Status

                # Check for maverick or paranoid operating mode
                if ($operatingMode -eq "Paranoid")
                {
                    Write-Host "Stop and delete all vApps?"
                    confirm-continue

                    if ($break)
                    {
                        write-warning "Exiting script - Disconnecting from VCD"
                        disconnect-ci
                        break
                    }
                }
                elseif ($operatingMode -eq "Maverick")
                {
                    Write-Host "Maverick mode enabled" -ForegroundColor Red -BackgroundColor White
                }
                write-host ""
                $runningVapps = $ciVapp | where-object {$_.status -ne 'PoweredOff'}
                if ($runningVapps)
                {
                    Write-Host "Stopping vApps..."
                    foreach ($vapp in $runningVapps)
                    {
                        stop-civapp -vapp $vapp -Confirm:$false
                    }
                    done
                }

                # Deleting the vApps
                Write-Host "Deleting vApps..."
                foreach ($obj in $ciVapp)
                {
                    Remove-CIVApp -VApp $obj -Confirm:$false
                }
                done

            # Sleep for 5 to ensure cleanup is complete
            start-sleep 5
            }

            # Deleting the Organisation Networks, if existing
            if ($orgNet)
            {
                Write-Host "Organisation Networks found:"
                $orgNet | format-table
                # Check for maverick or paranoid operating mode
                if ($operatingMode -eq "Paranoid")
                {
                    Write-Host "Delete all Organisation Networks?"
                    confirm-continue

                    if ($break)
                    {
                        write-warning "Exiting script - Disconnecting from VCD"
                        disconnect-ci
                        break
                    }
                }
                elseif ($operatingMode -eq "Maverick")
                {
                    Write-Host "Maverick mode enabled" -ForegroundColor Red -BackgroundColor White
                }

                Write-Host "Deleting Organisation Networks..."
                Remove-OrgNetwork $orgNet -Confirm:$false
                done
            }

            # Edge Deletion must be done through the API as no cmdlets exist for this
            # Use the connect-ciREST function to connect to vCD via the API
            write-host "Connecting to REST API, Authentication needed."
            connect-ciREST -server $ciserver
            ""

            # Get Orgs
            Write-Host "Getting Org details..."
            $resource = "/org"
            $url = $baseurl + $resource
            $response = Invoke-RestMethod -Uri $url -Headers $ciHeaders -Method GET
            $orgs = $response.OrgList.org

            # Get link for a given OrgName
            $orgURL = ($orgs | where-object {$_.name -match $orgName}).href
            done

            # Get the list of Org VDCs
            Write-Host "Getting OrgVDC List..."
            $response = Invoke-RestMethod -Uri $orgURL -Headers $ciHeaders -Method GET
            done

            $orgVDClist = ($response.Org.Link | where-object {$_.type -eq "application/vnd.vmware.vcloud.vdc+xml"}).href
            foreach ($orgVDCurl in $orgVDClist)
            {
                # Get the list of Edge Gateways
                Write-Host "Getting Edge Gateway List..."
                $response = Invoke-RestMethod -Uri $orgVDCurl -Headers $ciHeaders -Method GET
                $edgeGatewayListURL = ($response.Vdc.Link | where-object {$_.rel -match "edgeGateways"}).href
                $response = Invoke-RestMethod -Uri $edgeGatewayListURL -Headers $ciHeaders -Method GET
                $edgeGatewayList = $response.QueryResultRecords.EdgeGatewayRecord
                done

                # Need a for-each loop to trash all edges
                # Check for maverick or paranoid operating mode
                if ($operatingMode -eq "Paranoid")
                {
                    write-host "Delete all Edge(s)..."
                    confirm-continue

                    if ($break)
                    {
                        write-warning "Exiting script - Disconnecting from VCD"
                        disconnect-ci
                        disconnect-ciREST
                        break
                    }
                }
                elseif ($operatingMode -eq "Maverick")
                {
                    Write-Host "Maverick mode enabled" -ForegroundColor Red -BackgroundColor White
                }

                foreach ($edge in $edgeGatewayList)
                {
                    write-host "$($edge.name)" -ForegroundColor Yellow
                    $response = Invoke-RestMethod -Uri $edge.href -Headers $ciHeaders -Method Delete

                    # Check for task success
                    $url = $response.Task.href
                    Test-TaskStatus -wait -for success -url $url
                }
                done
            }


            # Deleting any vApp Templates, if existing
            if ($orgVappTemplates)
            {
                Write-Host "Organisation vApp Templates found:"
                $orgVappTemplates | format-table
                # Check for maverick or paranoid operating mode
                if ($operatingMode -eq "Paranoid")
                {
                    write-host "Delete all Organisation vApp Templates?"
                    confirm-continue

                    if ($break)
                    {
                        write-warning "Exiting script - Disconnecting from VCD"
                        disconnect-ci
                        disconnect-ciREST
                        break
                    }
                }
                elseif ($operatingMode -eq "Maverick")
                {
                    Write-Host "Maverick mode enabled" -ForegroundColor Red -BackgroundColor White
                }

                Write-Host "Deleting Templates..."
                $orgVappTemplates | Remove-CIVAppTemplate -Confirm:$false
                done
            }

            # Delete any catalogs
            if ($orgCat)
            {
                Write-Host "Organisation Catalogs found:"
                $orgCat.name
                                # Check for maverick or paranoid operating mode
                if ($operatingMode -eq "Paranoid")
                {
                    write-host "Delete all Catalogs?"
                    confirm-continue

                    if ($break)
                    {
                        write-warning "Exiting script - Disconnecting from VCD"
                        disconnect-ci
                        disconnect-ciREST
                        break
                    }
                }
                elseif ($operatingMode -eq "Maverick")
                {
                    Write-Host "Maverick mode enabled" -ForegroundColor Red -BackgroundColor White
                }

                Write-Host "Deleting Catalogs..."
                ($orgCat | Get-CIView).Delete()
                done
            }

            # Deleting the Organisation vDC
            Write-Host "Organisation vDCs: $($orgVdc.name)"
            # Check for maverick or paranoid operating mode
            if ($operatingMode -eq "Paranoid")
            {
                write-host "Delete this vDC?"
                confirm-continue

                if ($break)
                {
                    write-warning "Exiting script - Disconnecting from VCD"
                    disconnect-ci
                    disconnect-ciREST
                    break
                }
            }
            elseif ($operatingMode -eq "Maverick")
            {
                Write-Host "Maverick mode enabled" -ForegroundColor Red -BackgroundColor White
            }

            Write-Host "Deleting Organisation Virtual Data Centre..."
            $orgVdc | Set-OrgVdc -Enabled $false | Remove-OrgVdc -Confirm:$false
            done
        }



    # Deleting the Organisation

    write-host "Delete the Organisation?"
    confirm-continue

    if ($break)
    {
        write-warning "Exiting script - Disconnecting from VCD"
        disconnect-ci
        disconnect-ciREST
        break
    }


    Write-Host "Deleting vCloud Director Organisation..."
    $org | Set-Org -Enabled $false | remove-org -Confirm:$false
    done
    Write-Host "All objects removed." -ForegroundColor green
    ""

    # Tidy up afterwards
    Disconnect-CI
    disconnect-ciREST
}

function New-SWRandomPassword {
    <#
    .Synopsis
       Generates one or more complex passwords designed to fulfill the requirements for Active Directory
    .DESCRIPTION
       Generates one or more complex passwords designed to fulfill the requirements for Active Directory
    .EXAMPLE
       New-SWRandomPassword
       C&3SX6Kn

       Will generate one password with a length between 8  and 12 chars.
    .EXAMPLE
       New-SWRandomPassword -MinPasswordLength 8 -MaxPasswordLength 12 -Count 4
       7d&5cnaB
       !Bh776T"Fw
       9"C"RxKcY
       %mtM7#9LQ9h

       Will generate four passwords, each with a length of between 8 and 12 chars.
    .EXAMPLE
       New-SWRandomPassword -InputStrings abc, ABC, 123 -PasswordLength 4
       3ABa

       Generates a password with a length of 4 containing atleast one char from each InputString
    .EXAMPLE
       New-SWRandomPassword -InputStrings abc, ABC, 123 -PasswordLength 4 -FirstChar abcdefghijkmnpqrstuvwxyzABCEFGHJKLMNPQRSTUVWXYZ
       3ABa

       Generates a password with a length of 4 containing atleast one char from each InputString that will start with a letter from
       the string specified with the parameter FirstChar
    .OUTPUTS
       [String]
    .NOTES
       Written by Simon WÃ¥hlin, blog.simonw.se
       I take no responsibility for any issues caused by this script.
    .FUNCTIONALITY
       Generates random passwords
    .LINK
       http://blog.simonw.se/powershell-generating-random-password-for-active-directory/

    #>
    [CmdletBinding(DefaultParameterSetName='FixedLength',ConfirmImpact='None')]
    [OutputType([String])]
    Param
    (
        # Specifies minimum password length
        [Parameter(Mandatory=$false,
                   ParameterSetName='RandomLength')]
        [ValidateScript({$_ -gt 0})]
        [Alias('Min')]
        [int]$MinPasswordLength = 8,

        # Specifies maximum password length
        [Parameter(Mandatory=$false,
                   ParameterSetName='RandomLength')]
        [ValidateScript({
                if($_ -ge $MinPasswordLength){$true}
                else{Throw 'Max value cannot be lesser than min value.'}})]
        [Alias('Max')]
        [int]$MaxPasswordLength = 12,

        # Specifies a fixed password length
        [Parameter(Mandatory=$false,
                   ParameterSetName='FixedLength')]
        [ValidateRange(1,2147483647)]
        [int]$PasswordLength = 8,

        # Specifies an array of strings containing charactergroups from which the password will be generated.
        # At least one char from each group (string) will be used.
        [String[]]$InputStrings = @('abcdefghijkmnpqrstuvwxyz', 'ABCEFGHJKLMNPQRSTUVWXYZ', '23456789', '!"#%&'),

        # Specifies a string containing a character group from which the first character in the password will be generated.
        # Useful for systems which requires first char in password to be alphabetic.
        [String] $FirstChar,

        # Specifies number of passwords to generate.
        [ValidateRange(1,2147483647)]
        [int]$Count = 1
    )
    Begin {
        Function Get-Seed{
            # Generate a seed for randomization
            $RandomBytes = New-Object -TypeName 'System.Byte[]' 4
            $Random = New-Object -TypeName 'System.Security.Cryptography.RNGCryptoServiceProvider'
            $Random.GetBytes($RandomBytes)
            [BitConverter]::ToUInt32($RandomBytes, 0)
        }
    }
    Process {
        For($iteration = 1;$iteration -le $Count; $iteration++){
            $Password = @{}
            # Create char arrays containing groups of possible chars
            [char[][]]$CharGroups = $InputStrings

            # Create char array containing all chars
            $AllChars = $CharGroups | ForEach-Object {[Char[]]$_}

            # Set password length
            if($PSCmdlet.ParameterSetName -eq 'RandomLength')
            {
                if($MinPasswordLength -eq $MaxPasswordLength) {
                    # If password length is set, use set length
                    $PasswordLength = $MinPasswordLength
                }
                else {
                    # Otherwise randomize password length
                    $PasswordLength = ((Get-Seed) % ($MaxPasswordLength + 1 - $MinPasswordLength)) + $MinPasswordLength
                }
            }

            # If FirstChar is defined, randomize first char in password from that string.
            if($PSBoundParameters.ContainsKey('FirstChar')){
                $Password.Add(0,$FirstChar[((Get-Seed) % $FirstChar.Length)])
            }
            # Randomize one char from each group
            Foreach($Group in $CharGroups) {
                if($Password.Count -lt $PasswordLength) {
                    $Index = Get-Seed
                    While ($Password.ContainsKey($Index)){
                        $Index = Get-Seed
                    }
                    $Password.Add($Index,$Group[((Get-Seed) % $Group.Count)])
                }
            }

            # Fill out with chars from $AllChars
            for($i=$Password.Count;$i -lt $PasswordLength;$i++) {
                $Index = Get-Seed
                While ($Password.ContainsKey($Index)){
                    $Index = Get-Seed
                }
                $Password.Add($Index,$AllChars[((Get-Seed) % $AllChars.Count)])
            }
            Write-Output -InputObject $(-join ($Password.GetEnumerator() | Sort-Object -Property Name | Select-Object -ExpandProperty Value))
        }
    }
}