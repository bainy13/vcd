<#  .SYNOPSIS
    version 0.1 - Written by Dave Hocking 2016.

    This script is a dirty hack to download bitbucket projects that are powershell modules
    It doesn't do anything clever with bitbucket, doesn't use Git at all, doesn't check for the need to copy/replace files
    It's inefficient, but, has low requirements and is intended as a stop-gap

    .DESCRIPTION
    If you run the script without admin rights, you are presented with the option to install into the user's personal modules folder:
    <userprofile>\documents\windowspowershell\modules
    
    If you execute the script with admin rights, the install will automatically proceed into the system's modules folder:
    <windows>\system32\windowspowershell\v1.0\modules

    ...if installed into the system, all users can access the module's functions.

    If verbose output is required, use -v at execution time

    .EXAMPLE
    Installs into either the systemroot or userprofile Module stores, depending on access rights
    
    downloadModule.ps1 

    .EXAMPLE
    Installs into either the systemroot or userprofile Module stores, depending on access rights
    Displays verbose output during execution

    downloadModule.ps1 -v
#>

[string]$bitbucketAccount = "davehocking"

# Define URL to get the files from
[string]$bitbucket = "https://bitbucket.org"

# Define the read-only account                                                                  
[string]$user = "proact@thehockings.net"                                                                                               
[string]$pass = "pro817BUCKET+"                                                                                                        
[string]$pair = "${user}:${pass}"      

# Encode the credentials                                                                                                
$bytes = [System.Text.Encoding]::ASCII.GetBytes($pair)                                                                         
[string]$base64 = [System.Convert]::ToBase64String($bytes) 

# Build the header                                                                                                
[string]$basicAuthValue = "Basic $base64"
[System.Collections.Hashtable]$headers = @{ Authorization = $basicAuthValue }     

# Detect verbose mode, set variable
if ($args[0] -eq "-v"){[string]$v = $true}
$args[0]

# Function declaration
function done
{
    if ($v)
    {
        Write-Host "Done." -ForegroundColor Green
        ""
    }
}

function move-and-delete
{
        # The archive creates a .git folder, move the contents to the parent directory
        if ($v)
        {
            Write-Host "    Moving files..."
        }
        Move-Item "$($destination)\$($bitbucketAccount)-$($project)-*\*" -Destination "$($destination)\"
        
        # The archive creates a .git folder, this is now useless, trash it
        if ($v)
        {
            Write-Host "    Cleaning up extra .git directory..."
        }
        Remove-Item "$($destination)\$($bitbucketAccount)-$($project)-*"
        done
}

function unpack
{
        # Unzip the archive to the destination
        if ($v)
        {
            Write-Host "    Working on " -NoNewline
            Write-Host "$project" -NoNewline -ForegroundColor Yellow
            Write-Host ", unzipping..."
        }
        Unzip $outfile $destination
}

# Detect if admin rights have been used
if (-NOT ([Security.Principal.WindowsPrincipal] [Security.Principal.WindowsIdentity]::GetCurrent()).IsInRole(`
[Security.Principal.WindowsBuiltInRole] "Administrator"))
{
    # No rights detected - install into user profile
    if ($v)
    {
        Write-Host "No Admin rights detected" -ForegroundColor Magenta
    }
    [string]$outpath = "$($env:USERPROFILE)\Documents\WindowsPowerShell\modules"
    [string]$question = read-host "Do you want to install into `"$($outpath)`" [y] or Enter to confirm"
    switch ($question)
    {
        y       {}
        ""      {}
        default {Write-Host "Stopping."; break}
    }
}        
else
{
    if ($v)
    {
        Write-Host "Admin rights detected" -ForegroundColor Green
    }
    # Admin rights used - install into system profile
    [string]$outpath = "$($env:SystemRoot)\System32\WindowsPowerShell\v1.0\Modules"
}

# Set up script to be able to unzip compressed archives (PowerShell v4 lacked this feature)
if ($v)
{
    Write-Host "Setting up ZIP de/compression functionality..."
}

# Create unzip function
Add-Type -AssemblyName System.IO.Compression.FileSystem
function Unzip
{
    param([string]$zipfile, [string]$outpath)
    [System.IO.Compression.ZipFile]::ExtractToDirectory($zipfile, $outpath)
}
done

# Create the Projects Array, for holding the names of the projects on bitbucket
if ($v)
{
    Write-Host "Creating Projects Array..."
}
[array]$projects = @()
$projects += "vcd"
done

# Loop through the array and download the latest build from the bitbucket Server
if ($v)
{
    Write-Host "Looping through the projects, downloading and unpacking..."
}
foreach ($project in $projects)
{
    # Set the URLs/Paths to be used and download the Archive file
    $url = "$($bitbucket)/$bitbucketAccount/$($project)/get/master.zip"
    $outfile = "c:\windows\temp\$($project).zip"
    $destination = "$($outpath)\$($project)"
    Invoke-WebRequest -Uri $url -OutFile $outfile -ErrorAction Stop -ErrorVariable fatal -Headers $headers
    if ($fatal)
    {
        Write-Warning "Failed to download file - stopping process to prevent accidental removal of old versions"
        break
    }

    # Check if the folder already exists (if it does -> trash it, if not, then just extract)
    if (!(Test-Path $destination))
    {
        unpack
        move-and-delete
    }
    else
    {
        # As the destination already exists, trash it, you'll be dropping a fresh copy off
        if ($v)
        {
            Write-Host "    Project already exists, trashing local copy..."
        }
        Remove-Item $destination -Confirm:$false -Recurse
        unpack
        move-and-delete
    }
}
done